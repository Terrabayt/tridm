<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\CareerController;
use App\Http\Controllers\Admin\EventsController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\Main\DictionaryController;
use App\Http\Controllers\Main\EventController;
use App\Http\Controllers\Main\LoginController;
use App\Http\Controllers\Main\MainController;
use App\Http\Controllers\Main\ProjectsController;
use Illuminate\Support\Facades\Route;


Route::get('/', [MainController::class, 'main']);

Route::get('/lang/{lang}', [LocalizationController::class, 'index'])->name('lang-switch');

//Route::middleware(['auth:sanctum', 'verified', 'prefix' => 'admin'])->group(function (){
//
//});
Route::post('/ckeditor-upload-file', [\App\Http\Controllers\CKEditorController::class, 'upload'])->name('ckeditor.image-upload');
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth:sanctum'], function () {
    Route::get('/', [AdminController::class, 'dashboard'])->name('dashboard');
    #region Menu
    Route::get('/menu', [MenuController::class, 'index'])->name('menu-index');
    Route::get('/menu/create', [MenuController::class, 'create'])->name('menu-create');
    Route::post('/menu/create', [MenuController::class, 'store'])->name('menu-store');
    Route::get('/menu/{menu}/update', [MenuController::class, 'updateForm'])->name('menu-update-form');
    Route::post('/menu/{menu}/update', [MenuController::class, 'update'])->name('menu-update');
    Route::delete('/menu/{menu}/delete', [MenuController::class, 'delete'])->name('menu-delete');
    Route::post('/menu/rearrange', [MenuController::class, 'rearrange'])->name('menu-rearrange');
    #endregion

    #region News
    Route::get('/news', [NewsController::class, 'index'])->name('news-index');
    Route::get('/news/create', [NewsController::class, 'create'])->name('news-create');
    Route::post('/news/create', [NewsController::class, 'store'])->name('news-store');

    Route::post('/news/{news}/delete', [NewsController::class, 'delete']);

    Route::get('/news/{news}/update', [NewsController::class, 'updateForm'])->name('news-update-form');
    Route::post('/news/{news}/update', [NewsController::class, 'update'])->name('news-update');
    Route::get('/news/{news}', [NewsController::class, 'show'])->name('news-show');
    #endregion

    #region Events
    Route::get('/events', [EventsController::class, 'index'])->name('events-index');
    Route::get('/events/create', [EventsController::class, 'create'])->name('events-create');
    Route::post('/events/create', [EventsController::class, 'store'])->name('events-store');

    Route::post('/events/{events}/delete', [EventsController::class, 'delete'])->name('events-delete');

    Route::get('/events/{events}/update', [EventsController::class, 'updateForm'])->name('events-update-form');
    Route::post('/events/{events}/update', [EventsController::class, 'update'])->name('events-update');
    Route::get('/events/{events}', [EventsController::class, 'show'])->name('events-show');
    #endregion

    #region Dictionary
    Route::get('/dictionary', [\App\Http\Controllers\Admin\DictionaryController::class, 'index'])->name('dictionary-index');
    Route::get('/dictionary/create', [\App\Http\Controllers\Admin\DictionaryController::class, 'create'])->name('dictionary-create');
    Route::post('/dictionary/create', [\App\Http\Controllers\Admin\DictionaryController::class, 'store'])->name('dictionary-store');

    Route::post('/dictionary/{dictionary}/delete', [\App\Http\Controllers\Admin\DictionaryController::class, 'delete']);

    Route::get('/dictionary/{dictionary}/update', [\App\Http\Controllers\Admin\DictionaryController::class, 'updateForm'])->name('dictionary-update-form');
    Route::post('/dictionary/{dictionary}/update', [\App\Http\Controllers\Admin\DictionaryController::class, 'update'])->name('dictionary-update');
    Route::get('/dictionary/{dictionary}', [\App\Http\Controllers\Admin\DictionaryController::class, 'show'])->name('dictionary-show');
    #endregion

    #region Career
    Route::get('/career-group', [CareerController::class, 'groupIndex'])->name('career-group-index');
    Route::get('/career-group/create', [CareerController::class, 'groupCreate'])->name('career-group-create');
    Route::post('/career-group/create', [CareerController::class, 'groupStore'])->name('career-group-store');

    Route::post('/career-group/{model}/delete', [CareerController::class, 'groupDelete'])->name('career-group-delete');

    Route::get('/career-group/{model}/update', [CareerController::class, 'groupUpdateForm'])->name('career-group-update-form');
    Route::post('/career-group/{model}/update', [CareerController::class, 'groupUpdate'])->name('career-group-update');

    Route::get('/career/{group}', [CareerController::class, 'index'])->name('career-index');
    Route::get('/career/create/{group}', [CareerController::class, 'create'])->name('career-create');
    Route::post('/career/create', [CareerController::class, 'store'])->name('career-store');
    Route::get('/career/{career}/update', [CareerController::class, 'updateForm'])->name('career-update-form');
    Route::post('/career/{career}/update', [CareerController::class, 'update'])->name('career-update');
    Route::delete('/career/{career}/delete', [CareerController::class, 'delete'])->name('career-delete');
    Route::post('/career/rearrange', [CareerController::class, 'rearrange'])->name('career-rearrange');
    #endregion
});

Route::name('main-')->group(function (){
    Route::get('/career', [MainController::class, 'career'])->name('career');
    Route::get('/get-career/{career}', [MainController::class, 'getCareer'])->name('get-career');
    Route::get('/cert/{user}/{course}', [MainController::class, 'cert'])->name('cert');
    Route::get('/news', [\App\Http\Controllers\Main\NewsController::class, 'index'])->name('news-index');
    Route::get('/news/{news}', [\App\Http\Controllers\Main\NewsController::class, 'show'])->name('news-show');
    Route::get('/tourist_atlas', [ProjectsController::class, 'atlas'])->name('tourist_atlas');
    Route::get('/events', [EventController::class, 'index'])->name('events-index');
    Route::get('/events/{event}', [EventController::class, 'show'])->name('events-show');
    Route::get('/tour_dictionary', [DictionaryController::class, 'index'])->name('dictionary-index');
    Route::get('/tour_dictionary/{dictionary}', [DictionaryController::class, 'show'])->name('dictionary-show');
    Route::get('/login-register', function (){
        return view('login-register');
    })->name('login-register');
    Route::post('/auth-check', [LoginController::class, 'check'])->name('auth-check');
    Route::post('/auth-register', [LoginController::class, 'register'])->name('auth-register');
    Route::get('/wait-moderator', function (){
        return view('wait-moderator');
    })->name('wait-moderator');
});

include 'academy.php';

Route::get('/{slug}', [MainController::class, 'generate']);

