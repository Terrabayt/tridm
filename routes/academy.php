<?php

use App\Http\Controllers\Academy\Instructor\CoursesController;
use App\Http\Controllers\Academy\Instructor\LessonsController;
use App\Http\Controllers\Academy\Instructor\MainController;
use App\Http\Controllers\Academy\Instructor\ModulesController;
use App\Http\Controllers\Academy\Instructor\QuizzesController;
use App\Http\Controllers\Academy\Instructor\StudentsController;
use App\Http\Controllers\Academy\Student\TestController;
use Illuminate\Support\Facades\Route;

Route::group(['as' => 'student-', 'prefix' => '/academy', 'middleware' => 'role:student'], function () {
    Route::get('/', [\App\Http\Controllers\Academy\Student\MainController::class, 'dashboard'])->name('dashboard');
    Route::get('/courses', [\App\Http\Controllers\Academy\Student\CoursesController::class, 'index'])->name('courses');
    Route::get('/courses/{course}', [\App\Http\Controllers\Academy\Student\CoursesController::class, 'show'])->name('courses-show');
    Route::get('/courses/module/{module}', [\App\Http\Controllers\Academy\Student\CoursesController::class, 'module'])->name('module-show');
    Route::get('/courses/lesson/{lesson}', [\App\Http\Controllers\Academy\Student\CoursesController::class, 'lesson'])->name('lessons-show');
    Route::get('/courses/start/lesson', [\App\Http\Controllers\Academy\Student\CoursesController::class, 'startLesson'])->name('lessons-start');
    Route::get('/module/test/{module}', [TestController::class, 'moduleIndex'])->name('module-test');
    Route::get('/module/start-test/{module}', [TestController::class, 'startModuleTest'])->name('module-test-start');
    Route::get('/test', [TestController::class, 'start'])->name('start-test');
    Route::post('/check-test', [TestController::class, 'check'])->name('check-test');
    Route::get('/report/{report}', [TestController::class, 'report'])->name('report');
    Route::get('/certificates', [\App\Http\Controllers\Academy\Student\MainController::class, 'certs'])->name('certs');
    Route::get('/certificates/{courses}', [\App\Http\Controllers\Academy\Student\MainController::class, 'downloadCerts'])->name('download-certs');
});

Route::group(['as' => 'instructor-', 'prefix' => '/academy/instructor', 'middleware' => 'role:instructor'], function () {
    Route::get('/', [MainController::class, 'dashboard'])->name('dashboard');
    #region courses
    Route::get('/courses', [CoursesController::class, 'index'])->name('courses');
    Route::get('/courses/{course}', [CoursesController::class, 'show'])->name('courses-show');
    Route::get('/course/new', [CoursesController::class, 'create'])->name('courses-create');
    Route::post('/courses/create', [CoursesController::class, 'store'])->name('courses-store');
    Route::get('/courses/update-form/{courses}', [CoursesController::class, 'updateForm'])->name('courses-update-form');
    Route::post('/courses/update/{courses}', [CoursesController::class, 'update'])->name('courses-update');
//    Route::get('/courses/delete', [CoursesController::class, 'delete'])->name('courses-delete');
    #endregion

    #region courses
    Route::get('/modules/{module}', [ModulesController::class, 'show'])->name('modules-show');
    Route::get('/modules/new/{course}', [ModulesController::class, 'create'])->name('modules-create');
    Route::post('/modules/create', [ModulesController::class, 'store'])->name('modules-store');
    Route::get('/modules/update-form/{module}', [ModulesController::class, 'updateForm'])->name('modules-update-form');
    Route::post('/modules/update/{module}', [ModulesController::class, 'update'])->name('modules-update');
//    Route::get('/courses/delete', [CoursesController::class, 'delete'])->name('courses-delete');
    #endregion

    #region Lessons
    Route::get('/lessons/show/{lesson}', [LessonsController::class, 'show'])->name('lessons-show');
    Route::get('/lessons/new/{module}', [LessonsController::class, 'create'])->name('lessons-create');
    Route::post('/lessons/store', [LessonsController::class, 'store'])->name('lessons-store');
    Route::get('/lessons/update/{lesson}', [LessonsController::class, 'updateForm'])->name('lessons-update-form');
    Route::post('/lessons/update/{lesson}', [LessonsController::class, 'update'])->name('lessons-update');
    #endregion

    #region Lessons
    Route::get('/quizzes/new/{module}', [QuizzesController::class, 'create'])->name('quizzes-create');
    Route::post('/quizzes/store', [QuizzesController::class, 'store'])->name('quizzes-store');
    Route::get('/quizzes/update/{quizzes}', [QuizzesController::class, 'updateForm'])->name('quizzes-update-form');
    Route::post('/quizzes/update/{quizzes}', [QuizzesController::class, 'update'])->name('quizzes-update');
    #endregion

    #region Category
    Route::get('/category', [CoursesController::class, 'categoryIndex'])->name('courses-category');
    Route::get('/category/new', [CoursesController::class, 'categoryCreate'])->name('courses-category-create');
    Route::post('/category/store', [CoursesController::class, 'categoryStore'])->name('courses-category-store');
    Route::get('/category/update/{category}', [CoursesController::class, 'categoryUpdateForm'])->name('courses-category-update-form');
    Route::post('/category/update/{category}', [CoursesController::class, 'categoryUpdate'])->name('courses-category-update');
    #endregion

    #region Students
    Route::get('/students', [StudentsController::class, 'index'])->name('students');
    Route::get('/students/new', [StudentsController::class, 'create'])->name('students-create');
    Route::post('/students/store', [StudentsController::class, 'store'])->name('students-store');
    Route::get('/students/update/{student}', [StudentsController::class, 'updateForm'])->name('students-update-form');
    Route::post('/students/update/{student}', [StudentsController::class, 'update'])->name('students-update');
    Route::post('/students/test-access/{user}', [StudentsController::class, 'testAccess'])->name('students-test-access');
    #endregion
});
