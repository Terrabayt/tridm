<?php
return [
    "Tashkent",
    "Republic of Karakalpakstan",
    "Andijan region",
    "Bukhara region",
    "Jizzakh region",
    "Kashkadarya region",
    "Navoi region",
    "Namangan region",
    "Samarkand region",
    "Surkhandarya region",
    "Syrdarya region",
    "Tashkent region",
    "Fergana region",
    "Khorezm region",
];
