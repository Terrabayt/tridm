<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            ['name_ru' => 'Home','name_uz' => 'Home','name_en' => 'Home', 'parent_id' => 0, 'order' => 0, 'link' => '/'],
            ['name_ru' => 'Pages','name_uz' => 'Pages','name_en' => 'Pages', 'parent_id' => 0, 'order' => 1, 'link' => '/pages'],
            ['name_ru' => 'Our Services','name_uz' => 'Our Services','name_en' => 'Our Services', 'parent_id' => 2, 'order' => 2, 'link' => '/our-services'],
            ['name_ru' => 'About','name_uz' => 'About','name_en' => 'About', 'parent_id' => 2, 'order' => 3, 'link' => '/about'],
            ['name_ru' => 'About Team','name_uz' => 'About Team','name_en' => 'About Team', 'parent_id' => 4, 'order' => 3, 'link' => '/about-team'],
            ['name_ru' => 'About Clients','name_uz' => 'About Clients','name_en' => 'About Clients', 'parent_id' => 4, 'order' => 3, 'link' => '/about-clients'],
            ['name_ru' => 'Contact Team','name_uz' => 'Contact Team','name_en' => 'Contact Team', 'parent_id' => 5, 'order' => 3, 'link' => '/contact-team'],
            ['name_ru' => 'Contact Clients','name_uz' => 'Contact Clients','name_en' => 'Contact Clients', 'parent_id' => 6, 'order' => 3, 'link' => '/contact-clients'],
            ['name_ru' => 'Contact','name_uz' => 'Contact','name_en' => 'Contact', 'parent_id' => 2, 'order' => 4, 'link' => '/contact'],
            ['name_ru' => 'Portfolio','name_uz' => 'Portfolio','name_en' => 'Portfolio', 'parent_id' => 2, 'order' => 4, 'link' => '/portfolio'],
            ['name_ru' => 'Gallery','name_uz' => 'Gallery','name_en' => 'Gallery', 'parent_id' => 2, 'order' => 4, 'link' => '/gallery']
        ];
        foreach ($menus as $menu) {
            \App\Models\Menu::Create($menu);
        }
    }
}
