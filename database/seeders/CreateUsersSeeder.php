<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'instructor',
                'email' => 'instructor@mail.uz',
                'role' => 'instructor',
                'password' => bcrypt('instructor'),
            ],
            [
                'name' => 'student',
                'email' => 'student@mail.uz',
                'role' => 'student',
                'password' => bcrypt('student'),
                'active' => 1,
            ],
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
