<?php

namespace App\Providers;

use App\Models\Menu;
use App\Models\News;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['Main.main-layout', 'Main.layout'], function ($view) {
            $view->with([
                'dynamicMenus' => Menu::where('parent_id', '=', 0)->orderBy('order')->get(),
            ]);
        });
        View::composer(['Main.layout'], function ($view) {
            $view->with([
                'latestNews' => News::where('name_' . app()->getLocale(), '!=', null)->where('active', 1)->orderBy('date', 'desc')->limit(6)->get()
            ]);
        });

    }
}
