<?php
if (! function_exists('getColorFromGroup')) {
    function getColorFromGroup($column, $value)
    {
        return \App\Models\CareerGroup::query()->where($column, $value)->first()->color;
    }
}
