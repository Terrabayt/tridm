<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Courses extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'categories_id',
        'desc_uz',
        'desc_en',
        'desc_ru',
        'name_uz',
        'name_ru',
        'name_en',
    ];

    public function categories()
    {
        return $this->hasOne(Category::class, 'id','categories_id' );
    }

    public function modules()
    {
        return $this->hasMany(Modules::class, 'courses_id','id' );
    }

    public function getName()
    {
        return $this->{'name_' . App::getLocale()};
    }

    public function getDesc()
    {
        return $this->{'desc_' . App::getLocale()};
    }

    public function generatePathCert() {
        return asset('certificates/' . auth()->user()->id . '/' . $this->id . '/'. $this->getName() .'.jpg');
    }

    public function users() {
        return $this->belongsToMany(
            User::class,
            'user_courses',
            'courses_id',
            'user_id');
    }

    public function coursesCompleted() {
        return $this->belongsToMany(
            User::class,
            'courses_completed',
            'courses_id',
            'user_id');
    }
}
