<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Modules extends Model
{
    use HasFactory;
    protected $fillable = [
        'courses_id',
        'desc_uz',
        'desc_en',
        'desc_ru',
        'name_uz',
        'name_ru',
        'name_en',
    ];

    public function getName()
    {
        return $this->{'name_' . App::getLocale()};
    }

    public function getDesc()
    {
        return $this->{'desc_' . App::getLocale()};
    }

    public function course(){
        return $this->hasOne(Courses::class,'id','courses_id');
    }

    public function lessons()
    {
        return $this->hasMany(Lessons::class, 'modules_id','id' );
    }

    public function quizzes(){
        return $this->hasMany(Quizzes::class,'modules_id','id');
    }

    public function users() {
        return $this->belongsToMany(
            User::class,
            'user_modules',
            'modules_id',
            'user_id');
    }
}
