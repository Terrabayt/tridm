<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_uz',
        'name_ru',
        'name_en',
        'link',
        'content_uz',
        'content_ru',
        'content_en',
        'keywords_uz',
        'keywords_ru',
        'keywords_en',
        'description_uz',
        'description_ru',
        'description_en',
        'parent_id',
        'system',
        'order',
    ];

    public function getName(){
        return $this->{'name_' . App::getLocale()};
    }

    public function getContent(){
        return $this->{'content_' . App::getLocale()};
    }

    protected static function booted()
    {
        self::creating(function ($model) {
            if ($model->system === 'on') {
                $model->system = 1;
            } else {
                $model->system = 0;
            }
            $model->parent_id = $model->parent_id !== null ? $model->parent_id : 0;
        });

        self::updating(function ($model) {
            if ($model->system === 'on') {
                $model->system = 1;
            } else {
                $model->system = 0;
            }

            $model->parent_id = $model->parent_id !== null ? $model->parent_id : 0;
        });
    }

    public function childs()
    {
        return $this->hasMany(Menu::class, 'parent_id', 'id')->orderBy('order');
    }
}
