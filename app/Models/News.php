<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name_ru",
        "name_en",
        "name_uz",
        "desc_uz",
        "desc_ru",
        "desc_en",
        "content_uz",
        "content_ru",
        "content_en",
        "active",
        "date",
        "image",
        "type",
    ];

    public function getName()
    {
        return $this->{'name_' . App::getLocale()};
    }

    public function getDesc()
    {
        return $this->{'desc_' . App::getLocale()};
    }

    public function getContent()
    {
        return $this->{'content_' . App::getLocale()};
    }

    protected static function booted()
    {
        self::creating(function ($model) {
            if (!is_int($model->active)) {
                if ($model->active === 'on') {
                    $model->active = 1;
                } else {
                    $model->active = 0;
                }
            }
        });

        self::updating(function ($model) {
            if (!is_int($model->active)) {
                if ($model->active === 'on') {
                    $model->active = 1;
                } else {
                    $model->active = 0;
                }
            }
        });
    }
}
