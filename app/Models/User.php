<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use InteractsWithMedia;

    public const ROLE_STUDENT = 'student';
    public const ROLE_INSTRUCTOR = 'instructor';


    public const roles = [
        'student' => 'Student',
        'instructor' => 'Instructor'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'active',
        'role',
        'region',
        'test_access',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
//        'profile_photo_url',
    ];

    public function courses(){
        return $this->belongsToMany(
            Courses::class,
            'user_courses',
            'users_id',
            'courses_id');
    }

    public function modules(){
        return $this->belongsToMany(
            Modules::class,
            'user_modules',
            'users_id',
            'modules_id');
    }

    public function lessons(){
        return $this->belongsToMany(
            Lessons::class,
            'user_lessons',
            'users_id',
            'lessons_id');
    }

    public function coursesCompleted(){
        return $this->belongsToMany(
            Courses::class,
            'courses_completed',
            'users_id',
            'courses_id');
    }

    public function reports(){
        return $this->hasMany(TestsReport::class, 'user_id','id');
    }

}
