<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name_ru",
        "name_en",
        "name_uz",
        "content_uz",
        "content_ru",
        "content_en",
        "comment_uz",
        "comment_ru",
        "comment_en",
        "active",
        "start_date",
        "end_date",
        "image",
        "views",
        'location_uz',
        'location_ru',
        'location_en',
        "comment",
        "fee"
    ];

    protected static function booted()
    {
        self::creating(function ($model) {
            if (!is_int($model->active))
                if ($model->active === 'on') {
                    $model->active = 1;
                } else {
                    $model->active = 0;
                }
        });

        self::updating(function ($model) {
            if (!is_int($model->active)) {
                if ($model->active === 'on') {
                    $model->active = 1;
                } else {
                    $model->active = 0;
                }
            }
        });
    }

    public function getName()
    {
        return $this->{'name_' . App::getLocale()};
    }

    public function getContent()
    {
        return $this->{'content_' . App::getLocale()};
    }

    public function getComment()
    {
        return $this->{'comment_' . App::getLocale()};
    }

    public function getLocation()
    {
        return $this->{'location_' . App::getLocale()};
    }
}
