<?php

namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    use HasFactory;

    protected $fillable = [
        "name_ru",
        "name_en",
        "name_uz",
        "content_uz",
        "content_ru",
        "content_en",
        "views",
        "desc_uz",
        "desc_ru",
        "desc_en",
    ];

    public function getName()
    {
        return $this->{'name_' . App::getLocale()};
    }

    public function getContent()
    {
        return $this->{'content_' . App::getLocale()};
    }

    public function getDesc()
    {
        return $this->{'desc_' . App::getLocale()};
    }
}
