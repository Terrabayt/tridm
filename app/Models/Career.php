<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Career extends Model
{
    use HasFactory;

    protected $fillable = [
        'group_id',
        'order',
        'name_uz',
        'name_ru',
        'name_en',
        'content_uz',
        'content_ru',
        'content_en',
        'parent_id',
        'color',
    ];
    public function getName() {
        return $this->{'name_' . App::getLocale()};
    }
    protected static function booted()
    {
        self::creating(function ($model) {
            $model->parent_id = $model->parent_id !== null ? $model->parent_id : 0;
        });

        self::updating(function ($model) {
            $model->parent_id = $model->parent_id !== null ? $model->parent_id : 0;
        });
    }

    public function childs()
    {
        return $this->hasMany(Career::class, 'parent_id', 'id')->orderBy('order');
    }

    public function group(){
        return $this->belongsTo(CareerGroup::class);
    }
}
