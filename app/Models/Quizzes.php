<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Quizzes extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_uz',
        'name_ru',
        'name_en',
        'modules_id',
    ];

    public function getName() {
        return $this->{'name_' . App::getLocale()};
    }

    public function modules()
    {
        return $this->hasOne(Modules::class, 'id','modules_id' );
    }

    public function answers()
    {
        return $this->hasMany(Answers::class, 'quizzes_id','id' );
    }
}
