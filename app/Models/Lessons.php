<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Lessons extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_uz',
        'name_ru',
        'name_en',
        'content_uz',
        'content_ru',
        'content_en',
        'link_uz',
        'link_ru',
        'link_en',
        'modules_id',
    ];

    public function getName()
    {
        return $this->{'name_' . App::getLocale()};
    }

    public function getContent()
    {
        return $this->{'content_' . App::getLocale()};
    }

    public function module(){
        return $this->hasOne(Modules::class,'id','modules_id');
    }

    public function next() {
        return $this->where('id', '>', $this->id)->where('modules_id', $this->modules_id)->orderBy('id','asc')->first();
    }

    public function prev() {
        return $this->where('id', '<', $this->id)->where('modules_id', $this->modules_id)->orderBy('id','asc')->first();
    }
}
