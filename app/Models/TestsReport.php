<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestsReport extends Model
{
    use HasFactory;

    public const MODULE_COUNT = 10;
    public const COURSE_COUNT = 30;

    protected $fillable = [
        'course_id',
        'module_id',
        'user_id',
        'true_count',
        'false_count',
        'passed',
        'finish_time',
        'quizzes'
    ];

    protected $casts = [
      'quizzes' => 'json'
    ];

    public function module()
    {
        return $this->hasOne(Modules::class, 'id','module_id' );
    }

    public function course()
    {
        return $this->hasOne(Courses::class, 'id','course_id' );
    }

    public function getLastModule(){
        return Modules::query()->where('courses_id', $this->course_id)->orderBy('id','desc')->first()->id;
    }
}
