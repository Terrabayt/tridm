<?php


namespace App\Http\Controllers\Main;


use App;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class EventController extends Controller
{
    public function index(Request $request)
    {
        $events = App\Models\Events::whereYear('start_date', date('Y'))
            ->where([
                ['active', true],
                ['name_' . App::getLocale(), '!=', null],
            ])->orderBy('start_date')->get()->groupBy(function ($d) {
                return Carbon::parse($d->start_date)->format('m');
            });
        return view('Main.events.index', [
            'model' => $events
        ]);
    }

    public function show(Request $request, App\Models\Events $event)
    {
        $events = App\Models\Events::whereYear('created_at', date('Y'))
            ->where([
                ['active', true],
                ['name_' . App::getLocale(), '!=', null],
//                ['start_date', '>', now()],
                ['id', '!=', $event->id]
            ])->limit(4)->get();
        $event->views = $event->views + 1;
        $event->save();
        return view('Main.events.show', [
            'event' => $event,
            'otherEvents' => $events
        ]);
    }

}
