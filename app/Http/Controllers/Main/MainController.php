<?php


namespace App\Http\Controllers\Main;


use App;
use App\Http\Controllers\Controller;
use App\Models\Career;
use App\Models\Courses;
use App\Models\Events;
use App\Models\Menu;
use App\Models\News;
use App\Models\User;
use File;
use Illuminate\Http\Client\Response;
use Request;

class MainController extends Controller
{
    public function main()
    {
        $news = News::select("*")
            ->where('name_' . $this->lang, '!=', null)
            ->where('active', 1)
            ->orderBy('date', 'desc')
            ->limit(6)
            ->get();

        $events = Events::where('name_' . $this->lang, '!=', null)
            ->where('active', 1)
//            ->where('start_date', '>', now())
            ->orderBy('start_date', 'asc')
            ->limit(4)
            ->get();
        return view('Main.main', [
            'news' => $news,
            'events' => $events
        ]);
    }

    public function generate(Request $request, string $slug)
    {
        return view('Main.generate', [
            'content' => Menu::where('link', '=', $slug)->firstOrFail()
        ]);
    }

    public function cert(User $user, Courses $course) {
        return view('Main.certificate.index', [
            'user' => $user,
            'course' => $course
        ]);
    }

    public function career(){
        $model = Career::query()->with('group')->where('parent_id', '=', 0)->get()->sortBy(function ($model) {
            return $model->group->getName();
        })->groupBy(function ($model) {
            return $model->group->getName();
        })->sortBy('name_' . \app()->getLocale());


        return view('Main.others.career',[
            'model' => $model
        ]);
    }

    public function getCareer(Career $career){
        return $career->{'content_'. app()->getLocale()};
    }
}
