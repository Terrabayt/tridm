<?php


namespace App\Http\Controllers\Main;


use App;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class LoginController extends Controller
{
    use Authenticatable;

    public function register(Request $request){
        $input = $request->all();

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6',
            'region' => 'required',
        ]);

        User::create([
            'email' => $input['email'],
            'name' => $input['username'],
            'password' => bcrypt($input['password']),
            'region' => $input['region'],
            'active' => false,
            'role' => User::ROLE_STUDENT
        ]);

        return redirect()->route('main-wait-moderator');
    }

    public function check(Request $request)
    {
        $input = $request->all();
        $this->validate($request, [
            'login_email' => 'required|email',
            'login_password' => 'required',
        ]);

        if(auth()->attempt(array('email' => $input['login_email'], 'password' => $input['login_password']),true))
        {
            if (auth()->user()->role == 'admin') {
                return redirect()->route('dashboard');
            }elseif (auth()->user()->role == 'student') {
                if (!auth()->user()->active){
                    auth()->logout();
                    return redirect()->route('main-wait-moderator');
                }
                return redirect()->route('student-courses');
            }elseif (auth()->user()->role == 'instructor') {
                return redirect()->route('instructor-dashboard');
            }
        }else{
            return redirect()->route('main-login-register')->with('error_login', 'Password or email is incorrect');
        }

    }
}
