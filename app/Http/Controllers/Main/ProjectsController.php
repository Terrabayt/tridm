<?php


namespace App\Http\Controllers\Main;


use App;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\News;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ProjectsController extends Controller
{
    public function atlas(Request $request)
    {
        return view('Main.projects.atlas', [
            'model' => Menu::where('link', '=', 'tourist_atlas')->firstOrFail()
        ]);
    }

}
