<?php


namespace App\Http\Controllers\Main;


use App\Http\Controllers\Controller;
use App\Models\Dictionary;

class DictionaryController extends Controller
{
    public function index()
    {
        return view('Main.dictionary.index', [
            'count' => Dictionary::count()
        ]);
    }

    public function show(Dictionary $dictionary)
    {
        return view('Main.dictionary.show', [
            'model' => $dictionary
        ]);
    }

}
