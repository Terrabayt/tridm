<?php


namespace App\Http\Controllers\Main;


use App;
use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        return view('Main.news.index', [
            'model' => News::where([
                ['active', true],
                ['name_' . App::getLocale(), '!=', null],
            ])->orderBy('date','desc')->paginate(6)
        ]);
    }

    public function show(Request $request, News $news)
    {
        $news->views = $news->views + 1;
        $news->save();
        return view('Main.news.show', [
            'news' => $news
        ]);
    }

}
