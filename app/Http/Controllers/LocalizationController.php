<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class LocalizationController extends Controller
{
    public function index($locale)
    {
        if (array_key_exists($locale, Config::get('languages'))){
            App::setlocale($locale);
            session()->put('locale', $locale);
        }
        return redirect()->back();
    }
}
