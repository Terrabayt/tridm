<?php

namespace App\Http\Controllers;

use App\Models\Courses;
use App\Models\Lessons;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $lang;

    public function __construct()
    {
        $this->lang = App::currentLocale();
    }

    public function checkPermission(Courses $course)
    {
        if (auth()->user()->courses()->find($course->id)) {
            return true;
        }
        abort(401, '');
    }

    public function checkLesson(Lessons $lesson)
    {
        $last_lesson = Lessons::where('modules_id', $lesson->module->id)->where('id', '<', $lesson->id)->orderBy('id', 'desc')->first();
        if ($last_lesson) {
            if (auth()->user()->lessons()->find($last_lesson)) {
                return true;
            }
        }
        if (!$last_lesson) {
            return true;
        }
        abort(404, "Don’t cheating");
    }
}
