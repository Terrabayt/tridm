<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    public function index()
    {

        return view('Admin.news.index', [
            'model' => News::all()
        ]);
    }

    public function create()
    {
        return view('Admin.news.create', []);
    }

    public function store(Request $request)
    {
        $request->validate([
            'photo' => 'required',
        ]);

        $news = News::create($request->all());
        if ($request->hasFile('photo')) {
            $image = $request->file('photo');
            $originName = $request->file('photo')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = $fileName . '_' . time() . '.' . $extension;
            $image_resize = Image::make($image->getRealPath());
//            $image_resize->resize(370, 240);
            $image_resize->save(public_path('images/news/main/' . $fileName));
            $news->image = $fileName;
            $news->save();
        }
        return redirect()->route('news-index')
            ->with('success', 'News created successfully.');
    }

    public function updateForm(News $news)
    {
        return view('Admin.news.update', [
            'news' => $news
        ]);
    }

    public function update(Request $request, News $news)
    {
        $news->update($request->all());
        if ($request->hasFile('photo')) {
            if ($request->file('photo')->getClientOriginalName() !== $news->image){
                if (file_exists(public_path('images/news/main/' . $news->image))){
                    unlink(public_path('images/news/main/' . $news->image));
                }
                $image = $request->file('photo');
                $originName = $request->file('photo')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('photo')->getClientOriginalExtension();
                $fileName = $fileName . '_' . time() . '.' . $extension;
                $image_resize = Image::make($image->getRealPath());
//                $image_resize->resize(370, 240);
                $image_resize->save(public_path('images/news/main/' . $fileName));
                $news->image = $fileName;
                $news->save();
            }
        }
        $news->update($request->all());


        return redirect()->route('news-index')
            ->with('success', 'Product created successfully.');
    }

    public function show(News $news)
    {
        return view('Admin.news.show', [
            'news' => $news
        ]);
    }

    public function delete(News $news)
    {
        $news->delete();
        return true;
    }

}
