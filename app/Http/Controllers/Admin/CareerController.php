<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Services\RearrangeService;
use App\Models\Career;
use App\Models\CareerGroup;
use App\Models\News;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class CareerController extends Controller
{
    #region Group
    public function groupIndex()
    {
        return view('Admin.career.group.index', [
            'model' => CareerGroup::all()
        ]);
    }

    public function groupCreate()
    {
        return view('Admin.career.group.create', []);
    }

    public function groupStore(Request $request)
    {
        $request->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
            'name_en' => 'required',
            'color' => 'required',
        ]);

        CareerGroup::create($request->all());
        return redirect()->route('career-group-index')
            ->with('success', 'News created successfully.');
    }

    public function groupUpdateForm(CareerGroup $model)
    {
        return view('Admin.career.group.create', [
            'model' => $model
        ]);
    }

    public function groupUpdate(Request $request, CareerGroup $model)
    {
        $model->update($request->all());

        return redirect()->route('career-group-index')
            ->with('success', 'Product created successfully.');
    }

    public function groupDelete(CareerGroup $model)
    {
        $model->delete();
        return true;
    }
    #endregion

    #region Career
    public function index(CareerGroup $group)
    {
        $model = Career::where('parent_id', '=', 0)->where('group_id', $group->id)->get();
        return view('Admin.career.index', [
            'model' => $model,
            'group' => $group,
            'content' => RearrangeService::generate($model)
        ]);
    }

    public function create(CareerGroup $group)
    {
        return view('Admin.career.create', [
            'group' => $group,
            'careers' => Career::where('group_id', $group->id)->orderBy('name_' . $this->lang, 'asc')->get()
        ]);
    }

    public function store(Request $request)
    {
        Career::create($request->all());
        return redirect()->route('career-index', $request->get('group_id'));
    }

    public function updateForm(Career $career)
    {
        return view('Admin.career.create', [
            'model' => $career,
            'menus' => Career::where('id' , '!=', $career->id)->where('group_id', $career->group_id)->get()
        ]);
    }

    public function update(Request $request, Career $career)
    {
        $career->update($request->all());

        return redirect()->route('career-index', $request->get('group_id'));
    }

    public function delete(Career $career)
    {
        $career->delete();
        return true;
    }

    public function rearrange(Request $request)
    {
        $data = $request->get('data');
        RearrangeService::run($data);
        return new JsonResponse(true);
    }
    #endregion
}
