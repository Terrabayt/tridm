<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Events;
use App\Models\News;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class EventsController extends Controller
{
    public function index()
    {

        return view('Admin.events.index', [
            'model' => Events::orderBy('start_date', 'desc')->get()
        ]);
    }

    public function create()
    {
        return view('Admin.events.create', []);
    }

    public function store(Request $request)
    {
        $request->validate([
            'photo' => 'required',
        ]);

        $events = Events::create($request->all());
        if ($request->hasFile('photo')) {
            $image = $request->file('photo');
            $originName = $request->file('photo')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = $fileName . '_' . time() . '.' . $extension;
            $image_resize = Image::make($image->getRealPath());
//            $image_resize->resize(370, 240);
            $image_resize->save(public_path('images/events/main/' . $fileName));
            $events->image = $fileName;
            $events->save();
        }
        return redirect()->route('events-index')
            ->with('success', 'News created successfully.');
    }

    public function updateForm(Events $events)
    {
        return view('Admin.events.update', [
            'events' => $events
        ]);
    }

    public function update(Request $request, Events $events)
    {
        $events->update($request->all());
        if ($request->hasFile('photo')) {
            if ($request->file('photo')->getClientOriginalName() !== $events->image){
                if (file_exists(public_path('images/events/main/' . $events->image))){
                    unlink(public_path('images/events/main/' . $events->image));
                }
                $image = $request->file('photo');
                $originName = $request->file('photo')->getClientOriginalName();
                $fileName = pathinfo($originName, PATHINFO_FILENAME);
                $extension = $request->file('photo')->getClientOriginalExtension();
                $fileName = $fileName . '_' . time() . '.' . $extension;
                $image_resize = Image::make($image->getRealPath());
//                $image_resize->resize(370, 240);
                $image_resize->save(public_path('images/events/main/' . $fileName));
                $events->image = $fileName;
                $events->save();
            }
        }
        $events->update($request->all());


        return redirect()->route('events-index')
            ->with('success', 'Product created successfully.');
    }

    public function show(Events $events)
    {
        return view('Admin.events.show', [
            'events' => $events
        ]);
    }

    public function delete(Events $events)
    {
        $events->delete();
        return true;
    }

}
