<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Menu;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Response;

class MenuController extends Controller
{
    public function index()
    {
        $menus = Menu::where('parent_id', '=', 0)->orderBy('order', 'asc')->get();
        return view('Admin.menu.index', [
            'menus' => $menus
        ]);
    }

    public function create()
    {
        return view('Admin.menu.create', [
            'menus' => Menu::where('parent_id', '=', 0)->orderBy('name_' . $this->lang, 'asc')->get()
        ]);
    }

    public function store(Request $request)
    {
        Menu::create($request->all());

        return redirect()->route('menu-index')
            ->with('success', 'Product created successfully.');
    }

    public function updateForm(Menu $menu)
    {
        return view('Admin.menu.create', [
            'menu' => $menu,
            'menus' => Menu::where('id' , '!=', $menu->id)->where('parent_id', '=', 0)->get()
        ]);
    }

    public function update(Request $request, Menu $menu)
    {
        $menu->update($request->all());

        return redirect()->route('menu-index')
            ->with('success', 'Product created successfully.');
    }

    public function show(Menu $menu)
    {
        return view('Admin.news.show', [
            'news' => $menu
        ]);
    }

    public function delete(Menu $menu)
    {
        $menu->delete();
        return true;
    }

    public function rearrange(Request $request)
    {
        $data = $request->get('data');
        $order = 1;
        foreach ($data as $menu) {
            $model = Menu::find($menu['id']);
            $model->order = $order;
            $model->parent_id = 0;
            $model->save();
            $order += 1;
            if (isset($menu['children'])) {
                $childrenOrder = 1;
                foreach ($menu['children'] as $child) {
                    $childMenu = Menu::find($child['id']);
                    $childMenu->order = $childrenOrder;
                    $childMenu->parent_id = $menu['id'];
                    $childMenu->save();
                    $childrenOrder += 1;
                }
            }
        }
        return new JsonResponse(true);
    }
}
