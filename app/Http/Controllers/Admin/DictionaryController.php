<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Dictionary;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class DictionaryController extends Controller
{
    public function index()
    {

        return view('Admin.dictionary.index', [
            'model' => Dictionary::all()
        ]);
    }

    public function create()
    {
        return view('Admin.dictionary.create', []);
    }

    public function store(Request $request)
    {
        $request->validate([
        ]);

        Dictionary::create($request->all());
        return redirect()->route('dictionary-index')
            ->with('success', 'Dictionary created successfully.');
    }

    public function updateForm(Dictionary $dictionary)
    {
        return view('Admin.dictionary.create', [
            'model' => $dictionary
        ]);
    }

    public function update(Request $request, Dictionary $dictionary)
    {
        $dictionary->update($request->all());

        return redirect()->route('dictionary-index')
            ->with('success', 'Product created successfully.');
    }

    public function show(Dictionary $dictionary)
    {
        return view('Admin.dictionary.show', [
            'dictionary' => $dictionary
        ]);
    }

    public function delete(Dictionary $dictionary)
    {
        $dictionary->delete();
        return true;
    }

}
