<?php


namespace App\Http\Controllers\Academy\Instructor;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Courses;
use App\Models\User;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    public function index()
    {
        $model = User::where('role','student')->get();
        return view('academy.instructor.students.index', [
            'model' => $model
        ]);
    }

    public function show(Courses $course)
    {
        return view('academy.instructor.courses.show', [
            'model' => $course
        ]);
    }

    public function create()
    {
        return view('academy.instructor.students.create-update', [
            'courses' => Courses::all()
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if ($request->get('pass')) {
            $data['password'] = \Hash::make($request->get('pass'));
        }
        $users = User::create($data);
        if($request->hasFile('photo'))
        {
            $users->addMediaFromRequest('photo')->toMediaCollection('avatar');
        }
        if ($courses = $request->get('courses')) {
                $users->courses()->attach($courses);
        }

        return redirect()->route('instructor-students');
    }

    public function updateForm(User $student) {
        return view('academy.instructor.students.create-update', [
            'model' => $student,
            'courses' => Courses::all(),
            'current_courses' => $student->courses->pluck('id')->toArray()
        ]);
    }
    public function update(Request $request, User $student)
    {
        if ($request->hasFile('photo')) {
            if ($student->getFirstMedia('avatar')) {
                $student->getFirstMedia('avatar')->delete();
            }
            $student->addMediaFromRequest('photo')->toMediaCollection('avatar');
        }
        $data = $request->all();
        if ($request->get('pass')) {
            $data['password'] = \Hash::make($data['pass']);
        }
        $data['active'] = $request->has('active');
        $student->update($data);
        $student->courses()->sync($request->get('courses'));
        return redirect()->route('instructor-students');
    }

    public function testAccess(User $user) {
        if (!$user->test_access) {
            $user->test_access = !$user->test_access;
            $user->save();
            foreach ($user->reports()->where('old', false)->get() as $item) {
                $item->old = true;
                $item->save();
            }
        }
    }
}
