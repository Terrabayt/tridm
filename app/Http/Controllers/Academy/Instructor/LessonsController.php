<?php


namespace App\Http\Controllers\Academy\Instructor;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Courses;
use App\Models\Lessons;
use App\Models\Modules;
use Illuminate\Http\Request;

class LessonsController extends Controller
{

    public function show(Lessons $lesson)
    {
        return view('academy.instructor.lessons.show', [
            'model' => $lesson
        ]);
    }

    public function create(Modules $module)
    {
        return view('academy.instructor.lessons.create-update', [
            'module' => $module->id
        ]);
    }

    public function store(Request $request)
    {
        $lesson = Lessons::create($request->all());
        return redirect()->route('instructor-lessons-show', ['lesson' => $lesson]);
    }

    public function updateForm(Lessons $lesson) {
        return view('academy.instructor.lessons.create-update', [
            'model' => $lesson,
            'module' => $lesson->module->id
        ]);
    }
    public function update(Request $request, Lessons $lesson)
    {
        $lesson->update($request->all());
        return redirect()->route('instructor-lessons-show', ['lesson' => $lesson]);
    }
}
