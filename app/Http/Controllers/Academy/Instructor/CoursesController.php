<?php


namespace App\Http\Controllers\Academy\Instructor;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Courses;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function index()
    {
        $model = Courses::all()->groupBy('categories_id');
        return view('academy.instructor.courses.index', [
            'model' => $model
        ]);
    }

    public function show(Courses $course)
    {
        return view('academy.instructor.courses.show', [
            'model' => $course
        ]);
    }

    public function create()
    {

        return view('academy.instructor.courses.create', [
            'category' => Category::all()
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'photo' => 'required'
        ]);
        $courses = Courses::create($request->all());

        if($request->hasFile('photo'))
        {
            $courses->addMediaFromRequest('photo')->toMediaCollection('courses');
        }
        return redirect()->route('instructor-courses');
    }

    public function updateForm(Courses $courses) {
        return view('academy.instructor.courses.create', [
            'model' => $courses,
            'category' => Category::all()
        ]);
    }
    public function update(Request $request, Courses $courses)
    {
        if ($request->hasFile('photo')) {
            if ($courses->getFirstMedia('courses')) {
                $courses->getFirstMedia('courses')->delete();
            }
            $courses->addMediaFromRequest('photo')->toMediaCollection('courses');
        }
        $courses->update($request->all());
        return redirect()->route('instructor-courses');
    }

    #region Category
    public function categoryIndex() {
        return view('academy.instructor.courses.category-index', [
            'category' => Category::all()
        ]);
    }

    public function categoryCreate() {
        return view('academy.instructor.courses.category-create-update');
    }

    public function categoryStore(Request $request)
    {
        $data = $request->validate([
            'name_ru' => 'required',
            'name_uz' => 'required',
            'name_en' => 'required',
        ]);
        Category::create($data);
        return redirect()->route('instructor-courses-category');
    }
    public function categoryUpdateForm(Category $category) {
        return view('academy.instructor.courses.category-create-update', [
            'model' => $category
        ]);
    }
    public function categoryUpdate(Request $request, Category $category)
    {
        $data = $request->validate([
            'name_ru' => 'required',
            'name_uz' => 'required',
            'name_en' => 'required',
        ]);
        $category->update($data);
        return redirect()->route('instructor-courses-category');
    }
    #endregion
}
