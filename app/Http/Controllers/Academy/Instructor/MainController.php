<?php


namespace App\Http\Controllers\Academy\Instructor;


use App\Http\Controllers\Controller;
use Request;

class MainController extends Controller
{
    public function dashboard()
    {
        return view('academy.instructor.main', [] );
    }

}
