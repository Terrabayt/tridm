<?php


namespace App\Http\Controllers\Academy\Instructor;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Courses;
use App\Models\Module;
use App\Models\Modules;
use Illuminate\Http\Request;

class ModulesController extends Controller
{
    public function show(Modules $module)
    {
        return view('academy.instructor.modules.show', [
            'model' => $module
        ]);
    }

    public function create(Courses $course)
    {
        return view('academy.instructor.modules.create-update', [
            'course' => $course->id
        ]);
    }

    public function store(Request $request)
    {
        $modules = Modules::create($request->all());
        return redirect()->route('instructor-modules-show', ['module' => $modules]);
    }

    public function updateForm(Modules $module) {
        return view('academy.instructor.modules.create-update', [
            'model' => $module,
            'course' => $module->courses_id
        ]);
    }

    public function update(Request $request, Modules $module)
    {
        $module->update($request->all());
        return redirect()->route('instructor-modules-show', ['module' => $module]);

    }
}
