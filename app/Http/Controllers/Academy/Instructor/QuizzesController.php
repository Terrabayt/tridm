<?php


namespace App\Http\Controllers\Academy\Instructor;


use App\Http\Controllers\Controller;
use App\Models\Answers;
use App\Models\Category;
use App\Models\Courses;
use App\Models\Lessons;
use App\Models\Modules;
use App\Models\Quizzes;
use Illuminate\Http\Request;

class QuizzesController extends Controller
{
    public function create(Modules $module)
    {
        return view('academy.instructor.lessons.quizzes-create', [
            'module' => $module->id
        ]);
    }

    public function store(Request $request)
    {
        $quiz = Quizzes::create([
           'name_ru' => $request->get('name_ru'),
           'name_uz' => $request->get('name_uz'),
           'name_en' => $request->get('name_en'),
            'modules_id' => (int)$request->get('modules_id')
        ]);
        if ($answers = $request->get('Answers')) {
            foreach ($answers as $key => $answer){
                Answers::create([
                    'name_ru' => $answer['name_ru'],
                    'name_uz' => $answer['name_uz'],
                    'name_en' => $answer['name_en'],
                    'true_answer' => (int)$request->get('true_answer') === $key ,
                    'quizzes_id' => $quiz->id
                ]);
            }
        }
        return redirect()->route('instructor-modules-show', ['module' => (int)$request->get('modules_id')]);
    }

    public function updateForm(Quizzes $quizzes) {
        return view('academy.instructor.lessons.quizzes-update', [
            'model' => $quizzes,
        ]);
    }
    public function update(Request $request, Quizzes $quizzes)
    {
        if ($answers = $request->get('Answers')) {
            foreach ($answers as $key => $answer){
                foreach ($answer as $id => $value){
                    Answers::where('id', $id)
                        ->update([
                            'name_ru' => $value['name_ru'],
                            'name_uz' => $value['name_uz'],
                            'name_en' => $value['name_en'],
                            'true_answer' => (int)$request->get('true_answer') === $key
                        ]);
                }
            }
        }
        $quizzes->update($request->all());
        return redirect()->route('instructor-modules-show', ['module' => $quizzes->modules]);
    }
}
