<?php


namespace App\Http\Controllers\Academy\Student;


use App\Http\Controllers\Controller;
use App\Http\Services\GenerateCertificateService;
use App\Models\Courses;
use App\Models\Lessons;
use App\Models\Modules;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function index()
    {
        $model = \Auth::user()->courses()->get()->groupBy('categories_id');
        return view('academy.student.courses.index', [
            'model' => $model
        ]);
    }

    public function show(Courses $course)
    {
        $this->checkPermission($course);
        return view('academy.student.courses.show', [
            'model' => $course,
            'passed_modules' => auth()->user()->modules->where('courses_id',$course->id)->pluck('id')->toArray(),
        ]);
    }

    public function module(Modules $module) {
        $this->checkPermission($module->course);
        return view('academy.student.courses.module', [
            'model' => $module,
            'passed_lessons' => auth()->user()->lessons->where('modules_id', $module->id)->pluck('id')->toArray(),
        ] );
    }

    public function lesson(Lessons $lesson) {
        $this->checkPermission($lesson->module->course);
        $this->checkLesson($lesson);
        auth()->user()->lessons()->syncWithoutDetaching($lesson);
        return view('academy.student.courses.lessons', [
            'model' => $lesson,
            'passed_lessons' => auth()->user()->lessons->pluck('id')->toArray(),
            'next_lesson' => $lesson->next(),
            'prev_lesson' => $lesson->prev(),
            'passed_modules' => auth()->user()->modules->where('courses_id',$lesson->module->course->id)->pluck('id')->toArray()
        ] );
    }

    public function startLesson(Request $request) {
        if ($module = $request->get('module')) {
            $lesson = auth()->user()->lessons()->where('modules_id' , $module)->orderBy('id', 'desc')->first();
            if (!$lesson) {
                $lesson = (Modules::find($module))->lessons()->orderBy('id', 'asc')->first();
            }
        }else{
            if ($course = $request->get('course')) {
                $lesson = auth()->user()->lessons()->with(['module' => function ($q) use ($course) {
                    $q->where('modules.courses_id', '=', $course);
                }])->orderBy('lessons_id', 'desc')->first();
                if (!$lesson) {
                    $lesson = auth()->user()->courses()->where('courses_id', $course)->first()->modules()->orderBy('id', 'asc')->first()->lessons()->orderBy('id', 'asc')->first();
                }
            }else{
                abort(404, 'Уроков не найдено 😢');
            }
        }
        return redirect()->route('student-lessons-show', ['lesson' => $lesson]);
    }

}
