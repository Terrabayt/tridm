<?php


namespace App\Http\Controllers\Academy\Student;


use App\Http\Controllers\Controller;
use App\Http\Services\GenerateCertificateService;
use App\Models\Courses;
use Request;

class MainController extends Controller
{
    public function dashboard()
    {
        return redirect()->route('student-courses');
        return view('academy.student.main', []);
    }

    public function certs()
    {
        return view('academy.student.cert.cert', [
            'model' => auth()->user()->coursesCompleted
        ]);
    }

    public function downloadCerts(Courses $courses)
    {
        $headers = [
            'Content-Type' => 'image/jpeg',
        ];
        $file = public_path('certificates/' . auth()->user()->id . '/' . $courses->id . '/' . $courses->getName() . '.jpg');
        return response()->download($file, $courses->getName(). '.jpg', $headers);
    }
}
