<?php


namespace App\Http\Controllers\Academy\Student;


use App\Http\Controllers\Controller;
use App\Http\Services\GenerateCertificateService;
use App\Http\Services\TestReportService;
use App\Models\Answers;
use App\Models\Courses;
use App\Models\Lessons;
use App\Models\Modules;
use App\Models\Quizzes;
use App\Models\TestsReport;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function moduleIndex(Modules $module)
    {
        TestReportService::checkOldReports();
        if (TestReportService::checkForFinalTest($module)) {
            $reports = TestsReport::where('course_id', $module->courses_id)->whereNull('module_id')->orderBy('id', 'desc')->where('old', false)->get();
            $success = !empty($reports->where('passed', 1)->all());
            return view('academy.student.test.course', [
                'model' => $module,
                'reports' => TestsReport::where('course_id', $module->courses_id)->whereNull('module_id')->orderBy('id', 'asc')->get(),
                'test_count' => $reports->count(),
                'success' => $success,
            ]);
        } else {
            $reports = TestsReport::where('module_id', $module->id)->orderBy('id', 'desc')->where('old', false)->get();
            $success = !empty($reports->where('passed', 1)->all());
            $next_module = null;
            if ($success) {
                $modules = auth()->user()->modules()->pluck('modules_id')->toArray();
                $next_module = Modules::whereNotIn('id', $modules)->orderBy('id', 'asc')->first();
            }
            return view('academy.student.test.module', [
                'model' => $module,
                'reports' => TestsReport::where('module_id', $module->id)->orderBy('id', 'asc')->get(),
                'test_count' => $reports->count(),
                'success' => $success,
                'next_module' => $next_module
            ]);
        }
    }


    public function startModuleTest(Modules $module)
    {
        TestReportService::checkOldReports();
        if (TestReportService::checkForFinalTest($module)) {
            if (!TestsReport::where([
                [
                    'course_id', $module->courses_id
                ],
                [
                    'user_id', auth()->user()->id
                ],
                [
                    'finish_time', '>', Carbon::now()
                ],
                [
                    'passed', '=', null
                ],
                [
                    'module_id', '=', null
                ]
            ])->first()) {
                TestsReport::create([
                    'user_id' => auth()->user()->id,
                    'course_id' => $module->course->id,
                    'false_count' => 0,
                    'true_count' => 0,
                    'finish_time' => Carbon::now()->add(env('COURSE_TEST_MIN') . ' minute')
                ]);
            }
        } else {
            if (!TestsReport::where([
                [
                    'module_id', $module->id
                ],
                [
                    'user_id', auth()->user()->id
                ],
                [
                    'finish_time', '>', Carbon::now()
                ]
            ])->whereNull('passed')->first()) {
                TestsReport::create([
                    'user_id' => auth()->user()->id,
                    'course_id' => $module->course->id,
                    'module_id' => $module->id,
                    'false_count' => 0,
                    'true_count' => 0,
                    'finish_time' => Carbon::now()->add(env('MODULE_TEST_MIN') . ' minute')
                ]);
            }
        }

        return redirect()->route('student-start-test');
    }

    public function start(Request $request)
    {
        $report = TestsReport::where('user_id', auth()->user()->id)->whereNull('passed')->where('finish_time', '>', Carbon::now())->first();
//        if (!$this->checkTime()) {
//            return redirect()->route('student-courses');
//        };

        if ($report) {
            $max_count = $report->module_id !== null ? TestsReport::MODULE_COUNT : TestsReport::COURSE_COUNT;
            if (session()->has('has-validation-error') || session('current-test')) {
                return view('academy.student.test.test', ['model' => Quizzes::find(session()->get('current-test')), 'report' => $report, 'max_count' => $max_count]);
            }
            if (!$report->quizzes || count($report->quizzes) < $max_count) {
                if ($report->module_id) {
                    $quizzes = Quizzes::where('modules_id', $report->module_id)->whereNotIn('id', $report->quizzes ?? [])->inRandomOrder()->first();
                } else {
                    $modules = Modules::where('courses_id', $report->course_id)->pluck('id')->toArray();
                    $quizzes = Quizzes::whereIn('modules_id', $modules)->whereNotIn('id', session('quizzes', []))->inRandomOrder()->first();
                }
                $report->quizzes = array_merge(!empty($report->quizzes) ? $report->quizzes : [], [$quizzes->id]);
                $report->save();
                $this->deleteSessions();
                session()->put('current-test', $quizzes->id);
                return view('academy.student.test.test', [
                    'model' => $quizzes,
                    'report' => $report,
                    'max_count' => $max_count
                ]);
            } else {
                TestReportService::calculatePercent($report);
                $report->save();
                $this->deleteSessions();
                TestReportService::deactivateUser();
                return redirect()->route('student-report', $report);
            }
        } else {
            $report = TestsReport::where('old', false)->orderBy('id', 'desc')->first();
            if ($report) {
                $this->deleteSessions();
                TestReportService::checkOldReports();
                TestReportService::deactivateUser();
                return redirect()->route('student-report', $report);
            } else {
                abort(404, 'TEST NOT FOUND');
            }
        }
    }

    private function deleteSessions()
    {
        session()->remove('current-test');
        session()->remove('has-validation-error');
    }

    public function startCourseTest(Modules $module)
    {
        if (\Session::get('test-module')) {
            \Session::put('test-start', now());
            \Session::put('test-course', $module->id);
        }

        return redirect()->route('student-start-test');
    }

    public function check(Request $request)
    {
        $validation = \Validator::make($request->all(),
            [
                'answer' => 'required',
                'quize' => 'required',
            ]
        );
        if ($validation->fails()) {
            session()->put('has-validation-error', true);
            return redirect()->back();
        }
        session()->remove('has-validation-error');
        session()->remove('current-test');
        $quize = Quizzes::find($request->get('quize'));
        $answer = $quize->answers()->where('id', $request->get('answer'))->first();
        $report = TestsReport::where('user_id', auth()->user()->id)->whereNull('passed')->where('finish_time', '>', Carbon::now())->first();
        if ($answer->true_answer) {
            $report->true_count += 1;
        } else {
            $report->false_count += 1;
        }
        $report->save();
        return redirect()->route('student-start-test');
    }

    public function finish(Request $request)
    {

    }

    #report
    public function report(TestsReport $report)
    {
        $goToFinalTest = true;

        if ($report->passed && $report->module_id === null){
            if (auth()->user()->coursesCompleted()->where('courses_id', $report->course_id)->first() === null){
                auth()->user()->coursesCompleted()->attach($report->course);
                (new GenerateCertificateService())->run($report->course);
            }
        }
        if($report->module_id !== null) {
            $goToFinalTest = TestReportService::checkForFinalTest($report->module);
        }
        return view('academy.student.test.report', ['model' => $report,'goToFinalTest' => $goToFinalTest]);
    }
    #endreport
}
