<?php

namespace App\Http\Livewire;

use App;
use DB;
use Livewire\Component;
use Livewire\WithPagination;

class Dictionary extends Component
{
    use WithPagination;

    public $search;
    public $letter;

    public function changeLetter($letter){
        $this->letter = $letter;
    }

    public function removeFilter(){
        $this->search = '';
        $this->letter = '';
    }


    public function render()
    {
        $query = \App\Models\Dictionary::orderBy('name_' . App::getLocale(), 'asc');
        $query->where(DB::raw('lower(name_' . App::getLocale() . ')'), 'LIKE', "%". mb_strtolower($this->search) ."%");
        $query->where(DB::raw('lower(name_' . App::getLocale() . ')'), 'LIKE', mb_strtolower($this->letter) ."%");
        $model = $query->paginate(10);
        return view('livewire.dictionary',[
            'model' => $model
        ]);
    }
}
