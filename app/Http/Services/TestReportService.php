<?php


namespace App\Http\Services;


use App\Models\Modules;
use App\Models\TestsReport;
use App\Models\User;

class TestReportService
{
    public static function checkOldReports()
    {
        $models = TestsReport::where('user_id', auth()->user()->id)->whereNull('passed')->where('finish_time', '<', now())->get();
        foreach ($models as $model) {
            self::calculatePercent($model);
            $model->save();
        }
        self::endModule();
    }

    public static function deactivateUser()
    {
        $models = TestsReport::where('user_id', auth()->user()->id)->where('old', false)->count();
        if ($models >= 3) {
            $user = User::find(auth()->user()->id);
            $user->test_access = false;
            $user->save();
        }
    }

    public static function calculatePercent(TestsReport &$report)
    {
        if (!$report->module_id) {
            $total = $report::COURSE_COUNT;
        } else {
            $total = $report::MODULE_COUNT;
        }
        if ($report->true_count + $report->false_count !== $total) {
            $report->false_count = $total - $report->true_count;
        }
        if ((($report->true_count * 100) / $total) >= 70) {
            $report->passed = true;
        } else {
            $report->passed = false;
        }
    }

    public static function isTestGoing()
    {
        if (TestsReport::where('user_id', auth()->user()->id)->whereNull('passed')->where('finish_time', '>', now())->first()) {
            return true;
        }
        return false;
    }

    public static function returnPercent(TestsReport $report)
    {
        if (!$report->module_id) {
            $total = $report::COURSE_COUNT;
        } else {
            $total = $report::MODULE_COUNT;
        }
        return ($report->true_count * 100) / $total;
    }

    public static function endModule()
    {
        $models = TestsReport::where('user_id', auth()->user()->id)->where('passed', true)->get()->pluck('module_id')->toArray();
        auth()->user()->modules()->sync($models);
    }

    public static function checkForFinalTest(Modules $modules)
    {
        $allModules = Modules::where('courses_id', $modules->courses_id)->pluck('id')->toArray();
        $passedModules = TestsReport::where([
            ['course_id', '=', $modules->courses_id],
            ['passed', '=', 1]
        ])->pluck('module_id')->toArray();
        return $passedModules == $allModules;
    }

}
