<?php


namespace App\Http\Services;


use App\Models\Career;
use App\Models\Modules;
use App\Models\TestsReport;
use App\Models\User;

class RearrangeService
{
    public static function run($data, $parent_id = 0, $recursive = false)
    {
        $order = 1;
        foreach ($data as $menu) {
            $model = Career::find($menu['id']);
            $model->order = $order;
            $model->parent_id = $parent_id;
            $model->save();
            $order += 1;
            if (isset($menu['children'])) {
                $childrenOrder = 1;
                foreach ($menu['children'] as $child) {
                    $childMenu = Career::find($child['id']);
                    $childMenu->order = $childrenOrder;
                    $childMenu->parent_id = $menu['id'];
                    $childMenu->save();
                    $childrenOrder += 1;
                    if (isset($child['children']))
                        self::run($child['children'], $child['id'], true);
                }
            }
        }
    }

    public static function generate($model){
        $content = '';
        foreach($model as $item){
            $content .= '<li class="dd-item" data-id="' . $item->id . '">
                <div class="dd-handle dd3-handle">Drag</div>
                <div class="dd-content" style="background: '. $item->color .'80">' . $item->getName() . '
                <span >
                <a href="'. route('career-update-form', [ 'career' => $item])  .'"
                class="btn btn-default" ><i class="zmdi zmdi-edit"></i></a>
                <a data-id="'. $item->id .'"
                                       class="btn btn-danger waves-effect delete" ><i
                                            class="zmdi zmdi-delete "></i></a>
                </span>
                </div>';
            if(count($item->childs) > 0){
                $content .= '<ol class="dd-list">';
                    $content .= self::generate($item->childs);
                $content .= '</li></ol>';
            }
            $content .= '</li>';
        }
        return $content;
    }

    public static function generateBlade($model){
        $content = '';
        foreach ($model as $item) {
            $content .= '<li><span style="background: '. $item->color .'80" data-id="'. $item->id .'" data-name="'. $item->getName() .'">'. $item->getName() .'</span>';
            if(count($item->childs) > 0){
                $content .= '<ol><li>';
                $content .= self::generateBlade($item->childs);
                $content .= '</ol></li>';
            }
            $content .= '</li>';
        }
        return $content;
    }
}
