<?php


namespace App\Http\Services;


use App\Models\Courses;
use App\Models\Modules;
use App\Models\TestsReport;
use App\Models\User;
use PDF;
use PhpOffice\PhpWord\TemplateProcessor;
use QrCode;

class GenerateCertificateService
{
    private $user;

    public function __construct()
    {
        $this->user = auth()->user();
    }

    public function run(Courses $courses){
        $path = public_path("/certificates/{$this->user->id}/{$courses->id}/");
        \File::ensureDirectoryExists($path);
        QrCode::format('png')->size(80)->color(0,0,0)->generate(route('main-cert', [$this->user, $courses]), $path.'qrcode.png');
        $filename = $path . "{$courses->getName()}.jpg";
        $source = imagecreatefromjpeg(public_path('images/cert.jpg'));
        $watermark = imagecreatefrompng($path. 'qrcode.png');

        $water_width = imagesx($watermark);
        $water_height = imagesy($watermark);

        $dime_x = 100;
        $dime_y = 820;

        imagecopy($source, $watermark, $dime_x, $dime_y, 0, 0, $water_width, $water_height);
        $font = public_path('fonts/cert.ttf');
        $color = imagecolorallocate($source, 0, 0, 0);

        imagettftext($source, 40,0,320,485,$color,$font, $this->user->name);
        imagettftext($source, 25,0,250,620,$color,$font, $courses->name_ru);

        imagejpeg($source,  $filename, 100);
        imagedestroy($source);
        \File::delete($path.'qrcode.png');
    }

    public function test() {
        QrCode::format('png')->size(80)->color(0,0,0)->generate('hello', 'qrcode.png');
        $source = imagecreatefromjpeg(public_path('images/cert.jpg'));
        $watermark = imagecreatefrompng( 'qrcode.png');

        $water_width = imagesx($watermark);
        $water_height = imagesy($watermark);

        $dime_x = 100;
        $dime_y = 820;

        imagecopy($source, $watermark, $dime_x, $dime_y, 0, 0, $water_width, $water_height);
        $font = public_path('fonts/cert.ttf');
        $color = imagecolorallocate($source, 0, 0, 0);

        imagettftext($source, 35,0,205,485,$color,$font, "Shakhrizod Nurmukhammadov Alisher o'g'li");
        imagettftext($source, 25,0,250,620,$color,$font, "Организация экскурсионного обслуживания");

        imagejpeg($source,  'test.jpg', 100);
        imagedestroy($source);
    }
}
