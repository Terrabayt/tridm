<?php

namespace App\Http\Middleware;

use App\Http\Services\TestReportService;
use Closure;

class CheckRole
{
    public function handle($request, Closure $next, $role)
    {
        if ($request->user() === null){
            return redirect()->route('main-login-register');
        }

        if ($request->user()->role !== $role) {
            abort(401, 'This action is unauthorized.');
        }
        if ($request->user()->role === 'student' && TestReportService::isTestGoing() && !str_ends_with(\Route::current()->getName(), 'test')) {
            return redirect()->route('student-start-test');
        }
        return $next($request);
    }
}
