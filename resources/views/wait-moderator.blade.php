@extends('Main.main-layout')
@section('title',  __('messages.Login') . '/' . __('messages.Register'))

@section('main-content')
    <main class="section-sm-70">
    <h1 class="text-center">{{ trans('messages.Wait moderator to allow enter') }}</h1>
    </main>
@endsection
