@extends('academy.main-layout')

@section('main-content')

    <div class="container">
        <div class="flex d-flex flex-column flex-sm-row align-items-end justify-content-between">
            <a href="{{ route('instructor-courses-show', ['course' => $model->courses_id]) }}">
                <h4>{{ $model->course->name_ru }}</h4>
            </a>

            <div class="mb-24pt mb-sm-0 mr-sm-24pt ">
                <a href="{{ route('instructor-modules-update-form', ['module' => $model]) }}" class="btn btn-warning">Изменить</a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <p class="form-label">Название модула</p>
                <div class=" mb-24pt">
                    <h4>{{ $model->name_ru }}</h4>
                </div>
                <p class="form-label">Описания</p>
                <div class=" mb-24pt">
                    {!!   $model->desc_ru !!}
                </div>
                <p class="form-label">Kurs nomi</p>
                <div class=" mb-24pt">
                    <h4>{{ $model->name_uz }}</h4>
                </div>

                <p class="form-label">Tavsif</p>
                <div class=" mb-24pt">
                    {!!   $model->desc_uz !!}
                </div>
                <div class="page-separator">
                </div>
                <div class="accordion js-accordion accordion--boxed mb-24pt" id="parent"
                     data-domfactory-upgraded="accordion">
                    <div class="accordion__item open">
                        <a href="#" class="accordion__toggle" data-toggle="collapse" data-target="#course-toc-1"
                           data-parent="#parent">
                            <span class="flex">Уроки</span>
                            <span class="accordion__toggle-icon material-icons">keyboard_arrow_down</span>
                        </a>

                        @foreach($model->lessons as $lesson)
                        <div class="accordion__menu collapse show" id="course-toc-1">
                            <div class="accordion__menu-link">
                                <i class="material-icons text-70 icon-16pt icon--left">drag_handle</i>
                                <a class="flex" href="{{ route('instructor-lessons-show', ['lesson' => $lesson]) }}">{{ $lesson->name_ru }}</a>
                                <a href="{{ route('instructor-lessons-update-form', ['lesson' => $lesson]) }}"> <span class=" material-icons">edit</span></a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="text-right mb-3">
                    <a href="{{ route('instructor-lessons-create', ['module' => $model]) }}" class="btn btn-outline-primary mb-24pt mb-sm-0">Добавить урок</a>
                </div>

                <div class="page-separator">
                </div>
                <div class="accordion js-accordion accordion--boxed mb-24pt" id="parent1"
                     data-domfactory-upgraded="accordion">
                    <div class="accordion__item open">
                        <a href="#" class="accordion__toggle" data-toggle="collapse" data-target="#course-toc-2"
                           data-parent="#parent1">
                            <span class="flex">Вопросы</span>
                            <span class="accordion__toggle-icon material-icons">keyboard_arrow_down</span>
                        </a>
                        @foreach($model->quizzes as $quizzes)
                            <div class="accordion__menu collapse show" id="course-toc-2">
                                <div class="accordion__menu-link">
                                    <i class="material-icons text-70 icon-16pt icon--left">drag_handle</i>
                                    <a class="flex" href="{{ route('instructor-quizzes-update-form', ['quizzes' => $quizzes]) }}">{{ $quizzes->name_ru }}</a>
                                    <a href="{{ route('instructor-quizzes-update-form', ['quizzes' => $quizzes]) }}"> <span class=" material-icons">edit</span></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="text-right mb-3">
                    <a href="{{ route('instructor-quizzes-create', ['module' => $model]) }}" class="btn btn-outline-primary mb-24pt mb-sm-0">Добавить вопрос</a>
                </div>
            </div>
        </div>
    </div>
@endsection
