@extends('academy.main-layout')

@section('main-content')
    <form
        action="{{ isset($model) ? route('instructor-modules-update', [ 'module' => $model]) : route('instructor-modules-store') }}"
        method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card p-relative o-hidden mb-0">
            <div class="card-header card-header-tabs-basic nav px-0 justify-content-center" role="tablist">
                <a href="#uz" class="active" data-toggle="tab" role="tab" aria-selected="true"
                   aria-controls="nav-home">Uzbek</a>
                <a href="#ru" data-toggle="tab" role="tab" aria-selected="true" class=""
                   aria-controls="nav-profile">Russian</a>
                <a href="#en" data-toggle="tab" role="tab" aria-selected="true" class=""
                   aria-controls="nav-contact">English</a>
            </div>
            <div class="card-body tab-content" id="nav-tabContent">
                @foreach(Config::get('languages') as $lang => $name)
                    <div class="tab-pane fade {{ $lang === 'uz' ? 'show active' :'' }}" id="{{$lang}}"
                         role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="form-group">
                            <label class="form-label" for="name_{{ $lang }}">Название модула:</label>
                            <input type="text"
                                   value="{{ old('name_' . $lang, isset($model) ? $model->{ 'name_'. $lang } : '') }}"
                                   class="form-control" name="name_{{ $lang }}" id="name_{{ $lang }}"
                                   placeholder="Введите название модула ...">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="desc_{{ $lang }}">Описания модула:</label>

                            <textarea type="text"
                                      class="form-control ckeditor" name="desc_{{ $lang }}" id="desc_{{ $lang }}"
                                      placeholder="Введите описания модула ...">{{ old('desc_' . $lang, isset($model) ? $model->{ 'desc_'. $lang } : '') }}</textarea>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <input type="number" hidden name="courses_id" value="{{ $course }}">
        <button type="submit" class="btn btn-primary">{{ isset($model) ? 'Изменить' : 'Создать' }}</button>
    </form>
@endsection
@section('js')
    <script src="//cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>
@endsection
