@extends('academy.main-layout')

@section('main-content')
    <form
        action="{{ isset($model) ? route('instructor-courses-update', [ 'courses' => $model]) : route('instructor-courses-store') }}"
        method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card p-relative o-hidden mb-0">
            <div class="card-header card-header-tabs-basic nav px-0 justify-content-center" role="tablist">
                <a href="#uz" class="active" data-toggle="tab" role="tab" aria-selected="true"
                   aria-controls="nav-home">Uzbek</a>
                <a href="#ru" data-toggle="tab" role="tab" aria-selected="true" class=""
                   aria-controls="nav-profile">Russian</a>
                <a href="#en" data-toggle="tab" role="tab" aria-selected="true" class=""
                   aria-controls="nav-contact">English</a>
            </div>
            <div class="card-body tab-content" id="nav-tabContent">
                @foreach(Config::get('languages') as $lang => $name)
                    <div class="tab-pane fade {{ $lang === 'uz' ? 'show active' :'' }}" id="{{$lang}}"
                         role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="form-group">
                            <label class="form-label" for="name_{{ $lang }}">Название курса:</label>
                            <input type="text"
                                   value="{{ old('name_' . $lang, isset($model) ? $model->{ 'name_'. $lang } : '') }}"
                                   class="form-control" name="name_{{ $lang }}" id="name_{{ $lang }}"
                                   placeholder="Введите название курса ...">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="desc_{{ $lang }}">Описания курса:</label>

                            <textarea type="text"
                                      class="form-control ckeditor" name="desc_{{ $lang }}" id="desc_{{ $lang }}"
                                      placeholder="Введите описания курса ...">{{ old('desc_' . $lang, isset($model) ? $model->{ 'desc_'. $lang } : '') }}</textarea>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="form-group pt-24pt">
            <label class="form-label" for="select01">Категория</label>
            <select id="select01" name="categories_id" data-toggle="select"
                    class="form-control select2-hidden-accessible"
                    data-select2-id="select01" tabindex="-1" aria-hidden="true">
                @foreach($category as $cat)
                    <option
                        value="{{ $cat->id }}" {{ isset($model) ? $model->categories_id === $cat->id ? 'selected' : '' : '' }}>{{ $cat->name_ru }}</option>
                @endforeach
            </select>
        </div>
        @if (isset($model) && $model->getFirstMediaUrl('courses'))
            <img src="{{$model->getFirstMediaUrl('courses')}}" height="150px" alt="">
        @endif

        <div class="form-group">
            <div class="custom-file">
                <input type="file" name="photo" id="photo" class="custom-file-input">
                <label for="photo" class="custom-file-label">Загрузить фото</label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">{{ isset($model) ? 'Изменить' : 'Создать' }}</button>
    </form>
@endsection
@section('js')
    <script src="//cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>
@endsection
