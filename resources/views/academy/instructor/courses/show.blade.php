@extends('academy.main-layout')

@section('main-content')
    <div class="container">
        <div class="flex d-flex flex-column flex-sm-row align-items-end justify-content-between">
            <a href="{{ route('instructor-courses') }}"><h4>Все курсы</h4></a>

            <div class="mb-24pt mb-sm-0 mr-sm-24pt ">
                <a href="{{ route('instructor-courses-update-form', ['courses' => $model]) }}" class="btn btn-warning">Изменить</a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <p class="form-label">Название курса</p>
                <div class=" mb-24pt">
                    <h4>{{ $model->name_ru }}</h4>
                </div>
                <p class="form-label">Категория</p>
                <div class=" mb-24pt">
                    <h4>{{ $model->categories->name_ru }}</h4>
                </div>
                <p class="form-label">Описания</p>
                <div class=" mb-24pt">
                    {!!   $model->desc_ru !!}
                </div>
                <p class="form-label">Kurs nomi</p>
                <div class=" mb-24pt">
                    <h4>{{ $model->name_uz }}</h4>
                </div>
                <p class="form-label">Kategoriya</p>
                <div class=" mb-24pt">
                    <h4>{{ $model->categories->name_uz }}</h4>
                </div>
                <p class="form-label">Tavsif</p>
                <div class=" mb-24pt">
                    {!!   $model->desc_uz !!}
                </div>
                <div class="page-separator">
                </div>
                <div class="accordion js-accordion accordion--boxed mb-24pt" id="parent"
                     data-domfactory-upgraded="accordion">
                    <div class="accordion__item open">
                        <a href="#" class="accordion__toggle" data-toggle="collapse" data-target="#course-toc-2"
                           data-parent="#parent">
                            <span class="flex">Модули</span>
                            <span class="accordion__toggle-icon material-icons">keyboard_arrow_down</span>
                        </a>
                        @foreach($model->modules as $module)
                        <div class="accordion__menu collapse show" id="course-toc-2">
                            <div class="accordion__menu-link">
                                <i class="material-icons text-70 icon-16pt icon--left">drag_handle</i>
                                <a class="flex" href="{{ route('instructor-modules-show', ['module' => $module]) }}">{{ $module->name_ru }}</a>
                                <a href="{{ route('instructor-modules-update-form', ['module' => $module]) }}"> <span class=" material-icons">edit</span></a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="text-right">
                    <a href="{{ route('instructor-modules-create', ['course' => $model]) }}" class="btn btn-outline-primary mb-24pt mb-sm-0">Добавить модул</a>
                </div>
            </div>
        </div>
    </div>
@endsection
