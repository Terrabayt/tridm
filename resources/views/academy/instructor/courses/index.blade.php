@extends('academy.main-layout')

@section('main-content')
    <div class="row justify-content-end" role="tablist">
        <div class="col-auto ">
            <a href="{{ route('instructor-courses-create') }}"
               class="btn btn-outline-secondary">Создать</a>
        </div>
    </div>
    @foreach($model as $cat => $item)
    <div class="page-separator">
        <div class="page-separator__text">{{ \App\Models\Category::where('id', $cat)->first()->name_ru }}</div>
    </div>
    <div class="row">
        @foreach($item as $course)
            <div class="col-lg-3 card-group-row__col">
                <div class="card card-sm card--elevated p-relative o-hidden overlay overlay--primary-dodger-blue js-overlay card-group-row__card">
                    <a href="{{ route('instructor-courses-show', ['course' => $course]) }}" class="card-img-top js-image" data-position="center" data-height="200"  style="display: block; position: relative; overflow: hidden; background-image: url({{ $course->getFirstMediaUrl('courses') }}); background-size: contain; background-repeat: no-repeat; background-position: center center; height: 140px;">
                        <img src="{{ $course->getFirstMediaUrl('courses') }}" alt="course" style="visibility: hidden; object-fit: contain">
                    </a>
                    <div class="card-body flex">
                        <div class="d-flex">
                            <div class="flex">
                                <a href="{{ route('instructor-courses-show', ['course' => $course]) }}"><p class="card-title">{{ $course->getName() }}</p></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row justify-content-end">
                            <a href="{{ route('instructor-courses-update-form', ['courses' => $course]) }}">
                                <div class="col-auto d-flex align-items-center">
                                    <span class="material-icons icon-24pt text-50 mr-4pt">edit</span>
                                </div>
                            </a>
                            <a href="{{ route('instructor-courses-show', ['course' => $course]) }}">
                                <div class="col-auto d-flex align-items-center">
                                    <span class="material-icons icon-24pt text-50 mr-4pt">remove_red_eye</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    @endforeach
@endsection
