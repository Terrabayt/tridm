@extends('academy.main-layout')
@section('main-content')
    <form action="{{ isset($model) ? route('instructor-courses-category-update', [ 'category' => $model]) : route('instructor-courses-category-store') }}" method="POST">
        @csrf
    <div class="card p-relative o-hidden mb-0">


            <div class="card-header card-header-tabs-basic nav px-0 justify-content-center" role="tablist">
                <a href="#uz" class="active" data-toggle="tab" role="tab" aria-selected="true"
                   aria-controls="nav-home">Uzbek</a>
                <a href="#ru" data-toggle="tab" role="tab" aria-selected="true" class=""
                   aria-controls="nav-profile">Russian</a>
                <a href="#en" data-toggle="tab" role="tab" aria-selected="true" class=""
                   aria-controls="nav-contact">English</a>
            </div>
            <div class="card-body tab-content" id="nav-tabContent">
                @foreach(Config::get('languages') as $lang => $name)
                    <div class="tab-pane fade {{ $lang === 'uz' ? 'show active' :'' }}" id="{{$lang}}"
                         role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="form-group">
                            <label class="form-label" for="name_{{ $lang }}">Название категории:</label>
                            <input type="text"
                                   value="{{ old('name_' . $lang, isset($model) ? $model->{ 'name_'. $lang } : '') }}"
                                   class="form-control" name="name_{{ $lang }}" id="name_{{ $lang }}"
                                   placeholder="Введите название категории ...">
                        </div>
                    </div>
                @endforeach
            </div>
            <button type="submit" class="btn btn-primary">Создать</button>

    </div>
    </form>
@endsection

