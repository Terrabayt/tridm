@extends('academy.main-layout')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/b-2.0.1/r-2.2.9/sp-1.4.0/datatables.min.css"/>


@endsection
@section('main-content')
        <div class="mb-2 page__container d-flex flex-column flex-md-row justify-content-end text-center text-sm-left">

            <div class="row" role="tablist">
                <div class="col-auto">
                    <a href="{{ route('instructor-courses-category-create') }}" class="btn btn-outline-secondary">{{ 'Создать' }}</a>
                </div>
            </div>

        </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Название</th>
            <th scope="col">Nomi</th>
            <th scope="col">Name</th>
            <th scope="col">Команды</th>
        </tr>
        </thead>
        <tbody>
        @foreach($category as $cat)
        <tr>
            <th scope="row">{{ ++$loop->index }}</th>
            <td>{{ $cat->name_ru }}</td>
            <td>{{ $cat->name_uz }}</td>
            <td>{{ $cat->name_en }}</td>
            <td><a href="{{ route('instructor-courses-category-update-form',['category' => $cat]) }}" class="btn btn-light">
                    <i class="material-icons">edit</i>
                </a></td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection
@section('js')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/b-2.0.1/r-2.2.9/sp-1.4.0/datatables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('table').DataTable({
                language: {
                    url: '{{ asset('data-table-ru.json') }}'
                }
            });
        } );
    </script>
@endsection
