@extends('academy.main-layout')

@section('main-content')
    <div class="row justify-content-end" role="tablist">
        <div class="col-auto ">
            <a href="{{ route('instructor-students-create') }}"
               class="btn btn-outline-secondary">Создать</a>
        </div>
    </div>
        <table class="text-center table table-striped">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">ФИО</th>
                <th scope="col">Статус</th>
                <th scope="col">Доступ к тесту</th>
                <th scope="col">Команды</th>
            </tr>
            </thead>
            <tbody>
            @foreach($model as $student)
                <tr>
                    <th scope="row">{{ $student->id }}</th>
                    <td>{{ $student->name }}</td>
                    <td class="text-center" data-id="{{ $student->id }}">
                        @if ($student->active)
                            <button class="status btn btn-success">Актив</button>
                        @else
                            <button class="status btn btn-danger">Неактивный</button>
                        @endif
                    </td>
                    <td class="text-center">
                        @if ($student->test_access)
                            <a class="access btn btn-success" data-id="{{ $student->id }}">Есть доступ</a>
                        @else
                            <a class="access btn btn-danger" data-id="{{ $student->id }}">Нет доступа</a>
                        @endif
                    </td>
                    <td><a href="{{ route('instructor-students-update-form',['student' => $student]) }}" class="btn btn-light">
                            <i class="material-icons">edit</i>
                        </a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
@endsection
@section('js')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/b-2.0.1/r-2.2.9/sp-1.4.0/datatables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('table').DataTable({
                language: {
                    url: '{{ asset('data-table-ru.json') }}'
                }
            });

            $('.access').on('click',function (e){
                id = $(this).data('id');
                $.ajax({
                    url: '/academy/instructor/students/test-access/' + id,
                    method: 'post',
                    data: {
                      "_token" : "{{ csrf_token() }}"
                    },
                    success: function (data){
                        window.location.reload()
                    }
                })
            })
        } );
    </script>
@endsection
