@extends('academy.main-layout')

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('main-content')
    <form
        action="{{ isset($model) ? route('instructor-students-update', ['student' => $model]) : route('instructor-students-store') }}"
        method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card p-relative o-hidden mb-0 p-2">
            <div class="form-group">
                <label class="form-label" for="name_">ФИО:</label>
                <input type="text"
                       value="{{ old('name', isset($model) ? $model->name : '') }}"
                       class="form-control" name="name" id="name"
                       placeholder="ФИО Студента ...">
            </div>
            <div class="row">
            <div class="col-md-6">

            <div class="form-group">
                <label class="form-label" for="email">Email:</label>
                <input type="email"
                       value="{{ old('name', isset($model) ? $model->email : '') }}"
                       class="form-control" name="email" id="email"
                       placeholder="Email ...">
            </div>

            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label class="form-label" for="name_">Пароль:</label>
                <input type="password"
                       value="{{ old('pass') }}"
                       class="form-control" name="pass" id="name"
                       placeholder="Пароль ...">
            </div>
            </div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
{{--                    <input id="customCheck01" name="active" type="checkbox" hidden value="0" class="custom-control-input">--}}
                    <input id="customCheck01" name="active" type="checkbox" {{ isset($model) && $model->active ? 'checked' :'' }} value="1" class="custom-control-input">
                    <label for="customCheck01" class="custom-control-label">Актив</label>
                </div>
            </div>
            <div class="form-group">
                <label class="form-label" for="select03">Курсы</label>
                <select multiple="" name="courses[]" class="form-control select2" >
                    @foreach($courses as $course)
                    <option {{ isset($model) && in_array($course->id,$current_courses) ? 'selected' : '' }} value="{{ $course->id }}">{{ $course->name_ru }}</option>
                    @endforeach
                </select>
            </div>

            @if (isset($model) && $model->getFirstMediaUrl('avatar'))
                <img src="{{$model->getFirstMediaUrl('avatar')}}" height="150px" width="200px" alt="">
            @endif

            <div class="form-group">
                <div class="custom-file">
                    <input type="file" name="photo" id="photo" class="custom-file-input">
                    <label for="photo" class="custom-file-label">Загрузить фото</label>
                </div>
            </div>
            <input type="text" hidden name="role" value="student">
            <button type="submit" class="btn btn-primary">{{ isset($model) ? 'Изменить' : 'Создать' }}</button>
    </form>
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(".select2").select2({
            tags: true
        });
    </script>
    {{--    <script src="//cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>--}}
@endsection
