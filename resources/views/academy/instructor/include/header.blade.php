<div id="header"
     class="mdk-header js-mdk-header mb-0"
     data-fixed
     data-effects="">
    <div class="mdk-header__content">

        <!-- Navbar -->

        <div class="navbar navbar-expand pr-0 navbar-dark-pickled-bluewood navbar-shadow"
             id="default-navbar"
             data-primary>

            <!-- Navbar Toggler -->

            <button class="navbar-toggler w-auto mr-16pt d-block rounded-0"
                    type="button"
                    data-toggle="sidebar">
                <span class="material-icons">short_text</span>
            </button>

            <!-- // END Navbar Toggler -->

            <!-- Navbar Brand -->

            <a href="https://luma.humatheme.com/Demos/Fixed_Layout/index.html"
               class="navbar-brand mr-16pt">

                            <span class="avatar avatar-sm navbar-brand-icon mr-0 mr-lg-8pt">

                                <span class="avatar-title rounded bg-primary"><img src="/images/academy/illustration/student/128/white.svg"
                                                                                   alt="logo"
                                                                                   class="img-fluid" /></span>

                            </span>

                <span class="d-none d-lg-block">Tridm</span>
            </a>


            <div class="flex"></div>

            <!-- Navbar Menu -->

            <div class="nav navbar-nav flex-nowrap d-flex mr-16pt">
                <div class="nav-item dropdown">
                    <a href="instructor-dashboard.html#"
                       class="nav-link d-flex align-items-center dropdown-toggle"
                       data-toggle="dropdown"
                       data-caret="false">
                                    <span class="avatar avatar-sm mr-8pt2">
                                        <span class="avatar-title rounded-circle bg-primary"><i class="material-icons">account_box</i></span>
                                    </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-header"><strong>Аккаунт</strong></div>
{{--                        <a class="dropdown-item"--}}
{{--                           href="https://luma.humatheme.com/Demos/Fixed_Layout/edit-account.html">Edit Account</a>--}}
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        $('#logout').submit(); " role="button"><i class="zmdi zmdi-time-restore"></i> Выйти</a>
                        <form method="POST" id="logout" action="{{ route('logout') }}">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
            <!-- // END Navbar Menu -->
        </div>
        <!-- // END Navbar -->
    </div>
</div>
