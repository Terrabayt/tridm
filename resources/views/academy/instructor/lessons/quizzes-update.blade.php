@extends('academy.main-layout')

@section('main-content')
    <form
        action="{{ route('instructor-quizzes-update-form', [ 'quizzes' => $model]) }}"
        method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card p-relative o-hidden mb-0">
            <div class="card-header card-header-tabs-basic nav px-0 justify-content-center" role="tablist">
                <a href="#uz" class="active" data-toggle="tab" role="tab" aria-selected="true"
                   aria-controls="nav-home">Uzbek</a>
                <a href="#ru" data-toggle="tab" role="tab" aria-selected="true" class=""
                   aria-controls="nav-profile">Russian</a>
                <a href="#en" data-toggle="tab" role="tab" aria-selected="true" class=""
                   aria-controls="nav-contact">English</a>
            </div>
            <div class="card-body tab-content" id="nav-tabContent">
                @foreach(Config::get('languages') as $lang => $name)
                    <div class="tab-pane fade {{ $lang === 'uz' ? 'show active' :'' }}" id="{{$lang}}"
                         role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="form-group">
                            <label class="form-label" for="name_{{ $lang }}">Вопрос:</label>
                            <input type="text"
                                   value="{{ old('name_' . $lang, isset($model) ? $model->{ 'name_'. $lang } : '') }}"
                                   class="form-control" name="name_{{ $lang }}" id="name_{{ $lang }}"
                                   placeholder="Введите вопрос ...">
                        </div>
                        <?php $right_answer = null ?>
                        @foreach($model->answers as $answer)
                            <div class="form-group">
                                <label class="form-label" for="name_{{ $lang }}">Ответь: №{{ ++$loop->index }}</label>
                                <input type="text"
                                       value="{{ old('Answer[' . $answer->id . ']name_' . $lang, $answer->{ 'name_'. $lang }) }}"
                                       class="form-control" name="Answers[{{ $loop->index }}][{{ $answer->id }}][name_{{ $lang }}]" id="name_{{ $lang }}"
                                       placeholder="Введите ответь ...">
                            </div>
                            <?php $answer->true_answer ? $right_answer = $loop->index : null ?>
                        @endforeach
                    </div>
                @endforeach

            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="form-label" for="custom-select">Правильный ответ</label>
            <select id="custom-select" name="true_answer" class="form-control custom-select">
                <option selected="">Выберите правильный ответ</option>
                @for($i = 1; $i <= (int)env('ANSWERS_COUNT'); $i++)
                <option {{ $right_answer === $i ? 'selected' : ''}} value="{{ $i }}">{{ $i }}</option>
                    @endfor
            </select>
        </div>

        <div class="text-right mt-2">
            <button type="submit" class="btn btn-primary">{{ isset($model) ? 'Изменить' : 'Создать' }}</button>
        </div>
    </form>
@endsection
@section('js')

@endsection
