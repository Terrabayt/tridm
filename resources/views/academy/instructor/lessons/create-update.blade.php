@extends('academy.main-layout')

@section('main-content')
    <form
        action="{{ isset($model) ? route('instructor-lessons-update', [ 'lesson' => $model]) : route('instructor-lessons-store') }}"
        method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card p-relative o-hidden mb-0">
            <div class="card-header card-header-tabs-basic nav px-0 justify-content-center" role="tablist">
                <a href="#uz" class="active" data-toggle="tab" role="tab" aria-selected="true"
                   aria-controls="nav-home">Uzbek</a>
                <a href="#ru" data-toggle="tab" role="tab" aria-selected="true" class=""
                   aria-controls="nav-profile">Russian</a>
                <a href="#en" data-toggle="tab" role="tab" aria-selected="true" class=""
                   aria-controls="nav-contact">English</a>
            </div>
            <div class="card-body tab-content" id="nav-tabContent">
                @foreach(Config::get('languages') as $lang => $name)
                    <div class="tab-pane fade {{ $lang === 'uz' ? 'show active' :'' }}" id="{{$lang}}"
                         role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="form-group">
                            <label class="form-label" for="name_{{ $lang }}">Название урока:</label>
                            <input type="text"
                                   value="{{ old('name_' . $lang, isset($model) ? $model->{ 'name_'. $lang } : '') }}"
                                   class="form-control" name="name_{{ $lang }}" id="name_{{ $lang }}"
                                   placeholder="Введите название урока ...">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="content_{{ $lang }}">Контент урока:</label>
                            <textarea type="text"
                                      class="form-control ckeditor" name="content_{{ $lang }}" id="content_{{ $lang }}"
                                      placeholder="Контент урока ...">{{ old('content_' . $lang, isset($model) ? $model->{ 'content_'. $lang } : '') }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="link_{{ $lang }}">Ссылка на видео:</label>
                            <input type="text"
                                   value="{{ old('link_' . $lang, isset($model) ? $model->{ 'link_'. $lang } : '') }}"
                                   class="form-control" name="link_{{ $lang }}" id="link_{{ $lang }}"
                                   placeholder="Ссылка ...">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <input type="number" hidden name="modules_id" value="{{ $module }}"/>
        <div class="text-right mt-2">
        <button type="submit" class="btn btn-primary">{{ isset($model) ? 'Изменить' : 'Создать' }}</button>
        </div>
    </form>
@endsection
@section('js')
    <script src="//cdn.ckeditor.com/4.17.1/full/ckeditor.js"></script>

    <script>
        CKEDITOR.replace('content_uz', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.replace('content_ru', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.replace('content_en', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.editorConfig = function (config) {
            config.extraPlugins = 'abbr';
        };
    </script>
@endsection
