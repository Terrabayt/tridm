@extends('academy.main-layout')

@section('main-content')
    <div class="container">
        <div class="flex d-flex flex-column flex-sm-row align-items-center justify-content-between">
            <a href="{{ route('instructor-modules-show', ['module' => $model->module]) }}"><h4>{{ $model->module->name_ru }}</h4></a>
            <div class="mb-24pt mb-sm-0 mr-sm-24pt ">
                <a href="{{ route('instructor-lessons-update-form', ['lesson' => $model]) }}" class="btn btn-warning">Изменить</a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <p class="form-label">Название урока</p>
                <div class=" mb-24pt">
                    <h4>{{ $model->name_ru }}</h4>
                </div>

                <p class="form-label">Контент</p>
                <div class=" mb-24pt">
                   {!!   $model->content_ru !!}
                </div>
                <p class="form-label">Dars nomi</p>
                <div class=" mb-24pt">
                    <h4>{{ $model->name_uz }}</h4>
                </div>

                <p class="form-label">Matn</p>
                <div class=" mb-24pt">
                    {!!   $model->content_uz !!}
                </div>

            </div>
        </div>
    </div>
@endsection
