@extends('academy.main-layout')

@section('main-content')
    <div class="mdk-header-layout__content page-content ">

        <div class="pt-32pt">
            <div
                class="container page__container d-flex flex-column flex-md-row align-items-center text-center text-sm-left">
                <div class="flex d-flex flex-column flex-sm-row align-items-center mb-24pt mb-md-0">

                    <div class="mb-24pt mb-sm-0 mr-sm-24pt">
                        <h2 class="mb-0">{{ trans('menu.Home') }}</h2>
                    </div>
                </div>


            </div>
        </div>

        <div class="page-section border-bottom-2">
            <div class="container page__container">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="card border-1 border-left-3 border-left-accent text-center mb-lg-0">
                            <div class="card-body">
                                <h4 class="h2 mb-0">&dollar;1,569.00</h4>
                                <div>Earnings this month</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card text-center mb-lg-0">
                            <div class="card-body">
                                <h4 class="h2 mb-0">&dollar;3,917.80</h4>
                                <div>Account Balance</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card text-center mb-lg-0">
                            <div class="card-body">
                                <h4 class="h2 mb-0">&dollar;10,211.50</h4>
                                <div>Total Sales</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection
