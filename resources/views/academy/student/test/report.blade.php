@extends('academy.main-layout')

@section('main-content')
    <div class="card p-relative o-hidden mb-0">
        <div class="card-body text-center">
            @if ($model->passed)
                <h4 class="text-success">{{ trans('messages.Congratulations you have passed test') }}</h4>
            @else
                <h4 class="text-danger">{{ trans("messages.Sorry you haven't passed test") }}</h4>
            @endif
            <table class="table d-flex justify-content-center table-responsive table-striped m-t-20 m-b-20">
                <tr>
                    <td class="font-weight-bold">Правильно</td>
                    <td>{{ $model->true_count }}</td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Не правильно</td>
                    <td>{{ $model->false_count }}</td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Процент</td>
                    <td>{{ \App\Http\Services\TestReportService::returnPercent($model) }}%</td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Время начало теста</td>
                    <td>{{ $model->created_at }}</td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Закончено</td>
                    <td>{{ $model->updated_at }}</td>
                </tr>
            </table>
            <div class="mt-3">
                @if($model->passed && $model->module_id && !$goToFinalTest)
                    <a href="{{ route('student-lessons-start',['module' => $next_module]) }}" class="btn btn-success">Продолжить
                        курс</a>
                @elseif(($model->passed && $model->module_id === null))
                    <a href="{{ route('student-certs', '') }}"
                       class="btn btn-success">Мои сертификаты</a>
                @elseif ($model->passed && $model->module_id === null && $goToFinalTest)
                    <a href="{{ $model->module_id ? route('student-module-test', $model->module_id) : route('student-module-test', $model->getLastModule()) }}"
                       class="btn btn-warning">Пройти к финальному тесту</a>
                @else
                    <a href="{{ $model->module_id ? route('student-module-test', $model->module_id) : route('student-module-test', $model->getLastModule()) }}"
                       class="btn btn-warning">Пройти к тесту</a>
                @endif
            </div>
        </div>
    </div>
@endsection
