@extends('academy.main-layout')

@section('main-content')
    <div class="bg-primary pb-lg-64pt py-32pt">
        <div class="container">
            <div class="d-flex flex-wrap align-items-end justify-content-end mb-16pt">
                <h1 class="text-white flex m-0">{{ trans('messages.Question') }} {{ $report->quizzes ? count($report->quizzes) : 1 }} of {{ $max_count }}</h1>
                <p class="h1 text-white-50 font-weight-light m-0" id="time"></p>
            </div>
            <p class="hero__lead measure-hero-lead text-white-50">{{ $model->getName() }}</p>
        </div>
    </div>

    <form method="POST" action="{{ route('student-check-test') }}" enctype="multipart/form-data">
        @csrf
        <div class="navbar navbar-expand-md navbar-list navbar-light bg-white border-bottom-2 "
             style="white-space: nowrap;">
            <div class="container page__container">
                <ul class="nav navbar-nav flex navbar-list__item">
                    <li class="nav-item">
                        <i class="material-icons text-50 mr-8pt">tune</i>
                        {{ trans('messages.Choose the correct answer below') }}
                    </li>
                </ul>
                <div class="nav navbar-nav ml-sm-auto navbar-list__item">
                    <div class="nav-item d-flex flex-column flex-sm-row ml-sm-16pt">
                        @if (is_array($report->quizzes) && count($report->quizzes) === $max_count)
                            <button type="submit"
                                    class="btn justify-content-center btn-accent w-100 w-sm-auto mb-16pt mb-sm-0 ml-sm-16pt">{{ trans('messages.Finish test') }}
                                <i class="material-icons icon--right">keyboard_arrow_right</i></button>
                        @else
                        <button type="submit"
                                class="btn justify-content-center btn-accent w-100 w-sm-auto mb-16pt mb-sm-0 ml-sm-16pt">{{ trans('messages.Next question') }}
                            <i class="material-icons icon--right">keyboard_arrow_right</i></button>
                            @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="container page__container">
            <div class="page-section">
                <div class="page-separator">
                    <div class="page-separator__text">{{ trans('messages.Your answer') }}</div>
                </div>
                <input type="hidden" name="quize" value="{{ $model->id }}">

                <div class="form-group">
                    <div class="custom-controls-stacked">
                        @foreach ($model->answers as $answer)
                            <div class="custom-control custom-radio">
                                <input id="radioStacked{{$loop->index}}" name="answer" value="{{ $answer->id }}" type="radio"
                                       class="custom-control-input">
                                <label for="radioStacked{{$loop->index}}" class="custom-control-label">{{ $answer->getName() }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>


            </div>
        </div>
    </form>
@endsection
@section('js')
    <script>
        testFinish = "{{ \Carbon\Carbon::parse($report->finish_time)->timestamp }}"
    </script>
@endsection
