@extends('academy.main-layout')

@section('main-content')
    <div class="text-center">
        <h4>Вы изучили курс {{ $model->course->getName() }}.</h4>
        <div class="row">
            <div class="col-md-6 mb-3">
                <p>Пройдите тест, чтобы завершить курс</p>
                <table class="table d-flex justify-content-center table-responsive table-striped m-t-20 m-b-20">
                    <tr>
                        <td class="font-weight-bold">Вопросы</td>
                        <td>{{ \App\Models\TestsReport::COURSE_COUNT }}</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Разрешено попыток</td>
                        <td>3 (осталось {{ 3 - $test_count }})</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Проходной уровень</td>
                        <td>70%</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Ограничение времени</td>
                        <td>{{ env('COURSE_TEST_MIN') }} минут</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Возвращаться к предыдущим вопросам</td>
                        <td>Запрещено</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6">
                <p>Попиткы</p>
                <table class="table d-flex justify-content-center table-responsive table-striped m-t-20 m-b-20">
                    <tr>
                        <th class="font-weight-bold">№</th>
                        <th class="font-weight-bold">Правилных ответов</th>
                        <th class="font-weight-bold">Неправилных ответов</th>
                        <th class="font-weight-bold">Процент</th>
                        <th class="font-weight-bold">Время окончания теста</th>
                    </tr>
                    @foreach($reports as $item)
                        <tr class="bg-{{ $item->passed ? 'success' : 'danger' }}">
                            <td>{{ ++$loop->index }}</td>
                            <td>{{ $item->true_count }}</td>
                            <td>{{ $item->false_count }}</td>
                            <td>{{ \App\Http\Services\TestReportService::returnPercent($item) }}%</td>
                            <td>{{ $item->finish_time }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

    </div>
    @if ($success)
        <div class="text-center" style="margin-top: 20px;">
            <a href="{{ route('student-lessons-start',['module' => $next_module]) }}"
               class="btn m-t-20 btn-primary">Перейти к {{ $next_module->getName() }}
            </a>
        </div>
    @else
        <div class="text-center" style="margin-top: 20px;">
            <a href="{{ route('student-module-test-start', ['module' => $model]) }}"
               class="btn m-t-20 btn-primary">{{ trans('messages.Start test') }}
            </a>
        </div>
    @endif
@endsection
