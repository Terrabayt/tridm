<div class="mdk-drawer js-mdk-drawer"
     id="default-drawer">
    <div class="mdk-drawer__content">
        <div class="sidebar sidebar-dark-pickled-bluewood sidebar-left"
             data-perfect-scrollbar>

            <!-- Sidebar Content -->

            <a href="https://luma.humatheme.com/Demos/Fixed_Layout/index.html"
               class="sidebar-brand ">
                <!-- <img class="sidebar-brand-icon" src="/images/academy/illustration/teacher/128/white.svg" alt="Luma"> -->

                <span class="avatar avatar-xl sidebar-brand-icon h-auto">

                            <span class="avatar-title rounded bg-primary"><img src="/images/academy/illustration/teacher/128/white.svg"
                                                                               class="img-fluid"
                                                                               alt="logo" /></span>

                        </span>

                <span>Tridm</span>
            </a>

            <ul class="sidebar-menu">

{{--                <li class="sidebar-menu-item active">--}}
{{--                    <a class="sidebar-menu-button"--}}
{{--                       href="{{ route('student-dashboard') }}">--}}
{{--                        <span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">school</span>--}}
{{--                        <span class="sidebar-menu-text">{{ trans('menu.Home') }}</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button"
                       href="{{ route('student-courses') }}">
                        <span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">import_contacts</span>
                        <span class="sidebar-menu-text">{{ trans('messages.My Courses') }}</span>
                    </a>
                </li>
                <li class="sidebar-menu-item">
                    <a class="sidebar-menu-button"
                       href="{{ route('student-certs') }}">
                        <span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">view_list</span>
                        <span class="sidebar-menu-text">{{ trans('messages.My certificates') }}</span>
                    </a>
                </li>
            </ul>


            <!-- // END Sidebar Content -->

        </div>
    </div>
</div>
