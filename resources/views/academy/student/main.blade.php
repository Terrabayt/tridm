@extends('academy.main-layout')
@section('title', __('messages.Scientific research institute for the study of the problems of cultural heritage objects and tourism development'))
@section('main-content')
    <div class="mdk-header--bg-dark mt-5 bg-dark mb-0" data-effects="parallax-background waterfall"
         data-fixed="" data-condenses="" data-retarget-mouse-scroll="" data-domfactory-upgraded="mdk-header"
         style="padding-top: 64px; will-change: transform; transform: translate3d(0px, 0px, 0px);">
        <div class="mdk-header__bg" style="visibility: visible;">
            <div class="mdk-header__bg-front"
                 style="background-image: url(/images/academy/photodune-4161018-group-of-students-m.jpg); transform: translate3d(0px, 0px, 0px);"></div>
            <div class="mdk-header__bg-rear"
                 style="transform: translate3d(0px, 0px, 0px); margin-top: -229.531px;"></div>
        </div>
        <div class="mdk-header__content justify-content-center">


            <div class="hero container page__container text-center text-md-left py-112pt">
                <h1 class="text-white text-shadow">Learn to Code</h1>
                <p class="lead measure-hero-lead mx-auto mx-md-0 text-white text-shadow mb-48pt">Business, Technology
                    and Creative Skills taught by industry experts. Explore a wide range of skills with our professional
                    tutorials.</p>

                <a href="courses.html" class="btn btn-lg btn-white btn--raised mb-16pt">Browse Courses</a>

                <p class="mb-0"><a href="" class="text-white text-shadow"><strong>Are you a teacher?</strong></a></p>

            </div>
            <div class="mdk-header-layout__content page-content ">

                <div class="border-bottom-2 py-16pt navbar-light bg-white border-bottom-2">
                    <div class="container page__container">
                        <div class="row align-items-center">
                            <div
                                class="d-flex col-md align-items-center border-bottom border-md-0 mb-16pt mb-md-0 pb-16pt pb-md-0">
                                <div
                                    class="rounded-circle bg-primary w-64 h-64 d-inline-flex align-items-center justify-content-center mr-16pt">
                                    <i class="material-icons text-white">subscriptions</i>
                                </div>
                                <div class="flex">
                                    <div class="card-title mb-4pt">8,000+ Courses</div>
                                    <p class="card-subtitle text-70">Explore a wide range of skills.</p>
                                </div>
                            </div>
                            <div
                                class="d-flex col-md align-items-center border-bottom border-md-0 mb-16pt mb-md-0 pb-16pt pb-md-0">
                                <div
                                    class="rounded-circle bg-primary w-64 h-64 d-inline-flex align-items-center justify-content-center mr-16pt">
                                    <i class="material-icons text-white">verified_user</i>
                                </div>
                                <div class="flex">
                                    <div class="card-title mb-4pt">By Industry Experts</div>
                                    <p class="card-subtitle text-70">Professional development from the best people.</p>
                                </div>
                            </div>
                            <div class="d-flex col-md align-items-center">
                                <div
                                    class="rounded-circle bg-primary w-64 h-64 d-inline-flex align-items-center justify-content-center mr-16pt">
                                    <i class="material-icons text-white">update</i>
                                </div>
                                <div class="flex">
                                    <div class="card-title mb-4pt">Unlimited Access</div>
                                    <p class="card-subtitle text-70">Unlock Library and learn any topic with one
                                        subscription.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
