@extends('academy.main-layout')

@section('main-content')
    <div class="mb-24pt mb-sm-0 mr-sm-24pt">
        <h2 class="mb-0">Сертификати</h2>
    </div>
    <hr>
    <div class="row card-group-row mb-lg-8pt">
        @foreach($model as $item)
        <div class="col-lg-3 card-group-row__col">
            <div class="card card-sm card--elevated p-relative o-hidden overlay overlay--primary-dodger-blue js-overlay card-group-row__card">
                <a href="{{ $item->generatePathCert() }}" class="card-img-top js-image" data-position="center" data-height="140"  style="display: block; position: relative; overflow: hidden; background-image: url({{ $item->generatePathCert() }}); background-size: contain; background-repeat: no-repeat; background-position: center center; height: 140px;">
                    <img src="{{ $item->generatePathCert() }}" alt="course" style="visibility: hidden; object-fit: contain">
                </a>
                <div class="card-body flex">
                    <div class="d-flex">
                        <div class="flex">
                            <p class="card-title">{{ $item->getName() }}</p>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row justify-content-end">
                        <a href="{{ route('student-download-certs', $item) }}">
                        <div class="col-auto d-flex align-items-center">
                            <span class="material-icons icon-16pt text-50 mr-4pt">file_download</span>
                            <p class="flex text-50 lh-1 mb-0"><small>Скачать</small></p>
                        </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection
