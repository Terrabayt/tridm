@extends('academy.main-layout')

@section('main-content')
    <div class="card p-relative o-hidden mb-0">
        <div class="card-body ">
            <div class="hero py-64pt text-center text-sm-left">
                <div class=" page__container">
                    <h2 class="text-center">{{ $model->getName() }}</h2>
                    {!! $model->getDesc() !!}
                    <div class="d-flex flex-column flex-sm-row align-items-center justify-content-end">
                        <a href="{{ route('student-lessons-start', ['module' => $model->id]) }}"
                           class="btn  btn-primary">{{ trans('messages.Start Lessons') }}</a>
                    </div>
                </div>
            </div>
            <div class="accordion js-accordion accordion--boxed list-group-flush" id="parent"
                 data-domfactory-upgraded="accordion">
                <div class="accordion__item open">
                    <a href="#" class="accordion__toggle" data-toggle="collapse" data-target="#course-toc-2"
                       data-parent="#parent" aria-expanded="true">
                        <span class="flex">{{ trans('messages.Lessons') }}</span>
                        <span class="accordion__toggle-icon material-icons">keyboard_arrow_down</span>
                    </a>
                    <div class="accordion__menu collapse show" id="course-toc-2" style="">
                        @foreach ($model->lessons as $lesson)
                            <div class="accordion__menu-link {{ in_array($lesson->id, $passed_lessons) ? '' : 'active' }}">
                                @if (in_array($lesson->id, $passed_lessons))
                                    <span
                                        class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                                    <i class="material-icons icon-16pt">check_circle</i>
                                                </span>
                                @else
                                    <i class="material-icons text-70 icon-16pt icon--left">drag_handle</i>
                                @endif
                                <a class="flex" href="{{ route('student-lessons-show', ['lesson' => $lesson]) }}">{{ $lesson->getName() }}</a>
                                    <span class="text-muted">{{ in_array($lesson->id, $passed_lessons) ? trans('messages.Completed') : '' }}</span>
                            </div>
                        @endforeach
                        <div class="accordion__menu-link">
                                               <span
                                                   class="icon-holder icon-holder--small icon-holder--light rounded-circle d-inline-flex icon--left">
                                                    <i class="material-icons icon-16pt">hourglass_empty</i>
                                                </span>
                            <a class="flex"
                               href="{{ route('student-lessons-show', ['lesson' => $lesson]) }}">{{ trans('messages.Test') }} {{ $lesson->getName() }}
                            </a>
                            <span class="text-muted">8m 42s</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
