@extends('academy.main-layout')
@section('title', $model->getName())
@section('main-content')
    <div class="row">
        <div class="col-md-9">
            <div class="card p-relative o-hidden mb-0">
                <div class="card-body ">
                    <h2 class="text-center">{{ $model->getName() }}</h2>
                    <hr>
                    {!! $model->getContent() !!}
                </div>
            </div>
            <div class="text-center">
                @if ($prev_lesson && $next_lesson)
                    <a href="{{ route('student-lessons-show', ['lesson' => $prev_lesson]) }}" class="mt-3 btn btn-outline-primary">
                        <i class="material-icons icon--left">chevron_left</i>
                        {{ $prev_lesson->getName() }}
                    </a>
                @endif
                @if ($next_lesson)
                    <a href="{{ route('student-lessons-show', ['lesson' => $next_lesson]) }}" class="mt-3 btn btn-outline-primary">
                        {{ $next_lesson->getName() }}
                        <i class="material-icons icon--right">navigate_next</i>
                    </a>
                @else
                        <a href="{{ route('student-module-test',['module' => $model->module->id]) }}" class="mt-3 btn btn-outline-primary">
                            {{ trans('messages.Start test') }}
                        </a>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="accordion js-accordion accordion--boxed list-group-flush" id="parent"
                 data-domfactory-upgraded="accordion">
                @foreach ($model->module->course->modules as $module)
                <div class="accordion__item {{ $module->id === $model->module->id ? 'open' : '' }}">
                    <a href="#" class="accordion__toggle" data-toggle="collapse" data-target="#course-toc-{{ $module->id }}"
                       data-parent="#parent" aria-expanded="true">
                        <span class="flex">{{ $module->getName() }}</span>
                        <span class="accordion__toggle-icon material-icons">keyboard_arrow_down</span>
                    </a>
                    <div class="accordion__menu collapse {{ $module->id === $model->module->id ? 'show' : '' }}" id="course-toc-{{ $module->id }}" style="">
                        @foreach($module->lessons as $lesson)
                            <div class="accordion__menu-link {{ in_array($lesson->id, $passed_lessons) ? '' : 'active' }}">
                                @if (in_array($lesson->id, $passed_lessons))
                                    <span
                                        class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                                    <i class="material-icons icon-16pt">check_circle</i>
                                                </span>
                                @else
                                     <i class="material-icons text-70 icon-16pt icon--left">drag_handle</i>
                                @endif
                                <a class="flex"
                                   @if (in_array($lesson->id, $passed_lessons))
                                   href="{{ route('student-lessons-show', ['lesson' => $lesson]) }}"
                                   @endif
                                   >{{ $lesson->getName() }}</a>
                                <span class="text-muted">{{ in_array($lesson->id, $passed_lessons) ? trans('messages.Completed') : '' }}</span>
                            </div>
                        @endforeach

                        <div class="accordion__menu-link">
                                               <span
                                                   class="icon-holder icon-holder--small icon-holder--light rounded-circle d-inline-flex icon--left">
                                                    <i class="material-icons icon-16pt">hourglass_empty</i>
                                                </span>
                            <a class="flex">{{ trans('messages.Test') }} {{ $module->getName() }}</a>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="accordion__item p-2">
                    <span
                        class="icon-holder icon-holder--small icon-holder--light rounded-circle d-inline-flex icon--left">
                                                    <i class="material-icons icon-16pt">hourglass_empty</i>
                                                </span>
                    <a class="flex">Финалный {{ trans('messages.Test') }} </a>
                </div>
            </div>
        </div>
    </div>
@endsection
