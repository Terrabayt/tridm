@extends('academy.main-layout')

@section('main-content')
    <div class="card p-relative o-hidden mb-0">
        <div class="card-body ">
            <div class="hero py-64pt text-center text-sm-left">
                <div class=" page__container">
                    <h2 class="text-center">{{ $model->getName() }}</h2>
                    {!! $model->getDesc() !!}
                    <div class="d-flex flex-column flex-sm-row align-items-center justify-content-center">
                        <a href="{{ route('student-lessons-start', ['course' => $model]) }}"
                           class="btn  btn-primary">{{ trans('messages.Start Course') }}</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
