<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:400,700%7CRoboto:400,500%7CExo+2:600&display=swap"
          rel="stylesheet">
    <!-- Preloader -->
    <link type="text/css" href="{{ asset('css/academy/spinkit.css') }}" rel="stylesheet">
    <!-- Perfect Scrollbar -->
    <link type="text/css" href="{{ asset('css/academy/perfect-scrollbar.css') }}" rel="stylesheet">
    <!-- Material Design Icons -->
    <link type="text/css" href="{{ asset('css/academy/material-icons.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <link type="text/css" href="{{ asset('css/academy/fontawesome.css') }}" rel="stylesheet">
    <!-- Preloader -->
    <link type="text/css" href="{{ asset('css/academy/preloader.css') }}" rel="stylesheet">
    <!-- App CSS -->
    <link type="text/css" href="{{ asset('css/academy/app.css') }}" rel="stylesheet">
    @yield('css')
    <link href="{{ asset('images/other/gerb1.png') }}" rel="icon">


</head>
<body class="layout-sticky-subnav layout-default ">

<div class="preloader">
    <div class="sk-chase">
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
        <div class="sk-chase-dot"></div>
    </div>
</div>

<!-- Header Layout -->
<div class="mdk-header-layout js-mdk-header-layout">

    <!-- Header -->
@if(Auth::user()->role === 'student')
    @include('academy.student.include.header')
@elseif(Auth::user()->role === 'instructor')
    @include('academy.instructor.include.header')
@endif
<!-- // END Header -->
    <div class="mdk-header-layout__content page-content " style="padding-top: 64px;">
        <div class="pt-32pt col-lg-12 d-flex align-items-center">
            <div class="flex page__container" style="max-width: 100%">
                @yield('main-content')
            </div>
        </div>
    </div>
    <!-- Footer -->

{{--    <div class="bg-white border-top-2 mt-auto">--}}
{{--        <div class="container page__container page-section d-flex flex-column">--}}

{{--            <p class="text-50 small mt-n1 mb-0">Copyright {{ date('Y') }} &copy; All rights reserved.</p>--}}
{{--        </div>--}}
{{--    </div>--}}

    <!-- // END Footer -->

</div>
<!-- // END Header Layout -->

<!-- Drawer -->
@if(Auth::user()->role === 'student')
    @include('academy.student.include.menu')
@elseif(Auth::user()->role === 'instructor')
    @include('academy.instructor.include.menu')
@endif

<!-- // END Drawer -->

<!-- jQuery -->
<script src="{{ asset('js/academy/jquery.min.js') }}"></script>

<!-- Bootstrap -->
<script src="{{ asset('js/academy/popper.min.js') }}"></script>
<script src="{{ asset('js/academy/bootstrap.min.js') }}"></script>

<!-- Perfect Scrollbar -->
<script src="{{ asset('js/academy/perfect-scrollbar.min.js') }}"></script>

<!-- DOM Factory -->
<script src="{{ asset('js/academy/dom-factory.js') }}"></script>

<!-- MDK -->
<script src="{{ asset('js/academy/material-design-kit.js') }}"></script>

<!-- App JS -->
<script src="{{ asset('js/academy/app.js') }}"></script>

<!-- Preloader -->
<script src="{{ asset('js/academy/preloader.js') }}"></script>

<!-- Global Settings -->
<script src="{{ asset('js/academy/settings.js') }}"></script>

<!-- Moment.js -->
<script src="{{ asset('js/academy/moment.min.js') }}"></script>
<script src="{{ asset('js/academy/moment-range.js') }}"></script>

<!-- Chart.js -->
{{--<script src="{{ asset('js/academy/Chart.min.js') }}"></script>--}}

{{--<!-- UI Charts Page JS -->--}}
{{--<script src="{{ asset('js/academy/chartjs-rounded-bar.js') }}"></script>--}}
{{--<script src="{{ asset('js/academy/chartjs.js') }}"></script>--}}

<!-- Chart.js Samples -->
{{--<script src="{{ asset('js/academy/page.instructor-dashboard.js') }}"></script>--}}

<!-- List.js -->
<script src="{{ asset('js/academy/list.min.js') }}"></script>
@yield('js')
<script src="{{ asset('js/academy/custom.js') }}"></script>
</body>
</html>
