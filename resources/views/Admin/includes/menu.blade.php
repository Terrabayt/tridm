<aside id="sidebar" class="sidebar c-overflow">
    <div class="profile-menu">
        <a href="">
            <div class="profile-pic">
                <img src="/images/admin/profile-pics/1.jpg" alt="">
            </div>

            <div class="profile-info">
                {{ auth()->user()->name }}

                <i class="zmdi zmdi-caret-down"></i>
            </div>
        </a>

        <ul class="main-menu">
            <li>
                <a href="#"><i class="zmdi zmdi-account"></i> {{ __('View Profile')}}</a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                        $('#logout').submit(); " role="button"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                <form method="POST" id="logout" action="{{ route('logout') }}">
                    @csrf
                </form>
            </li>
        </ul>
    </div>

    <ul class="main-menu">
        <li class="{{ (request()->is('admin')) ? 'active' : '' }}">
            <a href="{{ route('dashboard') }}"><i class="zmdi zmdi-home"></i> Home</a>
        </li>
        <li class="{{ (request()->is('admin/news*')) ? 'active' : '' }}">
            <a href="{{ route('news-index') }}"><i class="zmdi zmdi-receipt"></i> {{ __('messages.News') }}</a>
        </li>
        <li class="{{ (request()->is('admin/events*')) ? 'active' : '' }}">
            <a href="{{ route('events-index') }}"><i class="zmdi zmdi-calendar"></i> {{ __('menu.Events') }}</a>
        </li>
        <li class="{{ (request()->is('admin/menu*')) ? 'active' : '' }}">
            <a href="{{ route('menu-index') }}"><i class="zmdi zmdi-menu"></i> {{ __('messages.Menu') }}</a>
        </li>
        <li class="{{ (request()->is('admin/dictionary*')) ? 'active' : '' }}">
            <a href="{{ route('dictionary-index') }}"><i
                    class="zmdi zmdi-view-list"></i> {{ __('messages.Tour dictionary') }}</a>
        </li>
        <li class="{{ (request()->is('admin/career*')) ? 'active' : '' }}">
            <a href="{{ route('career-group-index') }}"><i class="zmdi zmdi-menu"></i> {{ 'Карера' }}</a>
        </li>
    </ul>
</aside>
