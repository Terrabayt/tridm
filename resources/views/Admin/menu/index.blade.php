@extends('Admin.layout')
@section('title', __('messages.Menu'))

@section('actions')
    <h2><a href="{{ route('menu-index') }}">{{ __('messages.List of menu') }}</a></h2>
    <ul class="actions">
        <li>
            <a href="{{ route('menu-create') }}">
                <i class="zmdi zmdi-plus-circle-o"></i>
            </a>
        </li>

    </ul>
@endsection

@section('content')
    <div class="card-body card-padding">
        <div class="dd">
            <ol class="dd-list">
                @foreach($menus as $menu)
                    <li class="dd-item" data-id="{{ $menu->id }}">
                        <div class="dd-handle dd3-handle">Drag</div>
                        <div class="dd-content"> {{ $menu->{ 'name_'. app()->getLocale()}  }}
                            <span style="position: absolute; right: 0; top: 0;">
                                    <a href="{{ route('menu-update-form', [ 'menu' => $menu]) }}"
                                       class="btn btn-default" ><i class="zmdi zmdi-edit"></i></a>
                                    <a data-id="{{ $menu->id }}"
                                       class="btn btn-danger waves-effect delete" ><i
                                            class="zmdi zmdi-delete "></i></a>
                                </span>
                        </div>
                        @if(count($menu->childs) > 0)
                            <ol class="dd-list">
                                @foreach($menu->childs as $child)
                                    <li class="dd-item" data-id="{{ $child->id }}">
                                        <div class="dd-handle dd3-handle">Drag</div>
                                        <div
                                            class="{{ $child->system ? 'dd-nodrag' : 'dd-content' }}">{{ $child->{ 'name_'. app()->getLocale()} }}</div>
                                        <span style="position: absolute; right: 0; top: 0;">
                                    <a href="{{ route('menu-update-form', [ 'menu' => $child]) }}"
                                       class="btn btn-default" ><i class="zmdi zmdi-edit"></i></a>
                                    <a data-id="{{ $child->id }}"
                                       class="btn btn-danger waves-effect delete" ><i
                                            class="zmdi zmdi-delete"></i></a>
                                </span>
                                        @if(count($child->childs) > 0)
                                            <ol class="dd-list">
                                                @foreach($child->childs as $children)
                                                    <li class="dd-item" data-id="{{ $children->id }}">
                                                        <div class="dd-handle dd3-handle">Drag</div>
                                                        <div
                                                            class="{{ $children->system ? 'dd-nodrag' : 'dd-content' }}">{{ $children->{ 'name_'. app()->getLocale()} }}</div>
                                                        <span style="position: absolute; right: 0; top: 0;">
                                    <a href="{{ route('menu-update-form', [ 'menu' => $children]) }}"
                                       class="btn btn-default" ><i class="zmdi zmdi-edit"></i></a>
                                    <a data-id="{{ $children->id }}"
                                       class="btn btn-danger waves-effect delete" ><i
                                            class="zmdi zmdi-delete"></i></a>
                                </span>
                                                    </li>
                                                @endforeach
                                            </ol>
                                        @endif
                                    </li>
                                @endforeach
                            </ol>
                        @endif
                    </li>
                @endforeach
            </ol>
        </div>
    </div>

    <style>
        .dd-content {
            display: block;
            height: 30px;
            margin: 5px 0;
            padding: 5px 10px 5px 40px;
            color: #333;
            text-decoration: none;
            font-weight: bold;
            border: 1px solid #ccc;
            background: #fafafa;
            background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
            background: -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
            background: linear-gradient(top, #fafafa 0%, #eee 100%);
            -webkit-border-radius: 3px;
            border-radius: 3px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        .dd-content:hover {
            color: #2ea8e5;
            background: #fff;
        }

        .dd-dragel > .dd-item > .dd-content {
            margin: 0;
        }

        .dd-item > button {
            margin-left: 30px;
        }

        .dd-handle {
            position: absolute;
            margin: 0;
            left: 0;
            top: 0;
            cursor: pointer;
            width: 30px;
            text-indent: 30px;
            white-space: nowrap;
            overflow: hidden;
            border: 1px solid #aaa;
            background: #ddd;
            background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
            background: -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
            background: linear-gradient(top, #ddd 0%, #bbb 100%);
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .dd-handle:before {
            content: '=';
            display: block;
            position: absolute;
            left: 0;
            top: 3px;
            width: 100%;
            text-align: center;
            text-indent: 0;
            color: #fff;
            font-size: 20px;
            font-weight: normal;
        }

        .dd-handle:hover {
            background: #ddd;
        }
    </style>
@endsection

@section('css')
    <link href="{{ asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css') }}" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css" rel="stylesheet">
    <link href="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}"
          rel="stylesheet">
    rel="stylesheet">

@endsection

@section('js_after')
    <script src="//cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js"></script>
    <script src="{{ asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js') }}"></script>

    <script>
        $('.dd').nestable({
            maxDepth: 2,
            scroll: true,
            callback: function (l, e) {
                $.ajax({
                    url: "{{ route('menu-rearrange') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'data': $('.dd').nestable('serialize')
                    },
                    method: 'post'
                })
            }
        });
        $('.delete').click(function(e){
            e.preventDefault()
            menuId = $(this).data('id');
            swal({
                    title: "{{ __('messages.Are you sure?') }}",
                    text: "{{ __('messages.Your will not be able to recover this menu!') }}",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "{{ __('messages.Cancel') }}",
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "{{ __('messages.Yes, delete it!') }}",
                    closeOnConfirm: false
                },
                function(){
                    $.ajax({
                        url: "/admin/menu/" + menuId + "/delete",
                        type: "delete",
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function (response) {
                            swal("{{ __('messages.Deleted') }}", "{{ __('messages.Your post has been deleted.') }}", "success");
                            window.location.reload()
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal("Error something went wrong!", textStatus, "danger");
                        }
                    });

                });
        })
    </script>
@endsection


