@extends('Admin.layout')
@section('title', !isset($menu) ? __('messages.Create menu') : __('messages.Update menu'))

@section('actions')
    <h2><a href="{{ route('menu-index') }}">{{ __('messages.List of menu') }}</a></h2>
@endsection

@section('content')
    <div class="card-body card-padding">
        <form action="{{ isset($menu) ? route('menu-update', ['menu' => $menu]) : route('menu-create') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <p class="c-black f-500 m-b-20 m-t-20">System</p>
            <div class="checkbox m-b-15">
                <label>
                    <input type="checkbox" value="" name="system" {{ isset($menu) ? ($menu->system ? 'checked' : '') : '' }}>
                    <i class="input-helper"></i>
                    System
                </label>
            </div>

            <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Link') }}</p>
            <div class="form-group">
                <div class="fg-line">
                    <input type="text" class="form-control" name="link"
                           placeholder="{{ __('messages.Link') }}" value="{{ isset($menu) ? $menu->link : '' }}">
                </div>
            </div>
            <div class="form-group">
                <div class="fg-line">
                    <p class="f-500 m-b-15 c-black">Главное меню</p>

                    <select class="selectpicker bs-select-hidden" name="parent_id" data-live-search="true">
                        <option value="">---</option>
                        @foreach($menus as $item)
                            <option value="{{$item->id}}" {{ isset($menu) ? ($item->id === $menu->parent_id ? 'selected' : '') : '' }}>{{ $item->{ 'name_'. app()->getLocale() } }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div role="tabpanel">
                <ul class="tab-nav text-center" role="tablist">
                    <li class="active"><a href="#uz" aria-controls="uz" role="tab" data-toggle="tab"
                                          aria-expanded="true">{{ __('messages.Uzbek') }}</a></li>
                    <li role="presentation" class=""><a href="#ru" aria-controls="ru" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.Russian') }}</a></li>
                    <li role="presentation" class=""><a href="#en" aria-controls="en" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.English') }}</a></li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="uz">
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Name') }}</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <input type="text" class="form-control" name="name_uz"
                                       placeholder="{{ __('messages.Name') }}" value="{{ isset($menu) ? $menu->name_uz : '' }}">
                            </div>
                        </div>

                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Content') }}</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <textarea class="form-control ckeditor" name="content_uz"
                                          placeholder="{{ __('messages.Description') }}">{{ isset($menu) ? $menu->content_uz : '' }}</textarea>
                            </div>
                        </div>

                        <legend>SEO</legend>

                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Description') }}</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <textarea class="form-control auto-size" name="description_uz"
                                          placeholder="{{ __('messages.Description') }}" data-autosize-on="true"
                                          style="overflow: hidden; overflow-wrap: break-word; height: 43px;">{{ isset($menu) ? $menu->desc_uz : '' }}</textarea>
                            </div>
                        </div>

                        <p class="c-black f-500 m-b-20 m-t-20">Keywords</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <textarea class="form-control auto-size" name="keywords_uz"
                                          placeholder="Keywords" data-autosize-on="true"
                                          style="overflow: hidden; overflow-wrap: break-word; height: 43px;">{{ isset($menu) ? $menu->keywords_uz : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="ru">
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Name') }}</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <input type="text" class="form-control" name="name_ru"
                                       placeholder="{{ __('messages.Name') }}" value="{{ isset($menu) ? $menu->name_ru : '' }}">
                            </div>
                        </div>

                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Content') }}</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <textarea class="form-control ckeditor" name="content_ru"
                                          placeholder="{{ __('messages.Description') }}">{{ isset($menu) ? $menu->content_ru : '' }}</textarea>
                            </div>
                        </div>

                        <legend>SEO</legend>

                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Description') }}</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <textarea class="form-control auto-size" name="description_ru"
                                          placeholder="{{ __('messages.Description') }}" data-autosize-on="true"
                                          style="overflow: hidden; overflow-wrap: break-word; height: 43px;">{{ isset($menu) ? $menu->description_ru : '' }}</textarea>
                            </div>
                        </div>

                        <p class="c-black f-500 m-b-20 m-t-20">Keywords</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <textarea class="form-control auto-size" name="keywords_ru"
                                          placeholder="Keywords" data-autosize-on="true"
                                          style="overflow: hidden; overflow-wrap: break-word; height: 43px;">{{ isset($menu) ? $menu->keywords_ru : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="en">
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Name') }}</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <input type="text" class="form-control" name="name_en"
                                       placeholder="{{ __('messages.Name') }}" value="{{ isset($menu) ? $menu->name_en : '' }}">
                            </div>
                        </div>

                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Content') }}</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <textarea class="form-control ckeditor" name="content_en"
                                          placeholder="{{ __('messages.Description') }}">{{ isset($menu) ? $menu->content_en : '' }}</textarea>
                            </div>
                        </div>

                        <legend>SEO</legend>

                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Description') }}</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <textarea class="form-control auto-size" name="description_en"
                                          placeholder="{{ __('messages.Description') }}" data-autosize-on="true"
                                          style="overflow: hidden; overflow-wrap: break-word; height: 43px;">{{ isset($menu) ? $menu->description_en : '' }}</textarea>
                            </div>
                        </div>

                        <p class="c-black f-500 m-b-20 m-t-20">Keywords</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <textarea class="form-control auto-size" name="keywords_en"
                                          placeholder="Keywords" data-autosize-on="true"
                                          style="overflow: hidden; overflow-wrap: break-word; height: 43px;">{{ isset($menu) ? $menu->keywords_en : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary waves-effect">{{ __('messages.Save') }}</button>
            </div>
        </form>
    </div>
@endsection

@section('css-before')
    <link href="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}"
          rel="stylesheet">
    <link href="{{ asset('vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css') }}"
          rel="stylesheet">
@endsection

@section('css')
    <link href="{{ asset('vendors/vakata-jstree-4a77e59/dist/themes/default-dark/style.css') }}"
          rel="stylesheet">
@endsection

@section('js_after')
    <script
        src="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('vendors/autosize/dist/autosize.min.js') }}"></script>
    <script src="{{ asset('vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.16.1/full/ckeditor.js"></script>
    <script src="{{ asset('vendors/vakata-jstree-4a77e59/dist/jstree.js') }}"></script>
    <script>
        autosize($('.auto-size'));
        CKEDITOR.replace('content_uz', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token()])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.replace('content_ru', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.replace('content_en', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.editorConfig = function (config) {
            config.extraPlugins = 'abbr';
        };
    </script>
@endsection

