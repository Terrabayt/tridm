@extends('Admin.layout')
@section('title', $news->getName())

@section('actions')
    <h2><a href="{{ route('dictionary-index') }}">{{ __('messages.List of tour dictionary') }}</a></h2>

    <ul class="actions">
        <li>
            <a href="{{ route('dictionary-update-form', [ 'news' => $news]) }}">
                <i class="zmdi zmdi-edit"></i>
            </a>
        </li>

    </ul>
@endsection

@section('content')
    <div class="card-body card-padding">
            <div role="tabpanel">
                <ul class="tab-nav text-center" role="tablist">
                    <li class="active"><a href="#uz" aria-controls="uz" role="tab" data-toggle="tab"
                                          aria-expanded="true">{{ __('messages.Uzbek') }}</a></li>
                    <li role="presentation" class=""><a href="#ru" aria-controls="ru" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.Russian') }}</a></li>
                    <li role="presentation" class=""><a href="#en" aria-controls="en" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.English') }}</a></li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="uz">
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Title') }}</p>
                        <p>{{ $news->name_uz }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Description') }}</p>
                        <p>{{ $news->desc_uz }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Content') }}</p>
                        {!! $news->content_uz !!}
                        <hr>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="ru">
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Title') }}</p>
                        <p>{{ $news->name_ru }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Description') }}</p>
                        <p>{{ $news->desc_ru }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Content') }}</p>
                        {!! $news->content_ru !!}
                        <hr>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="en">
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Title') }}</p>
                        <p>{{ $news->name_en }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Description') }}</p>
                        <p>{{ $news->desc_en }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Content') }}</p>
                        {!! $news->content_en !!}
                        <hr>
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('css-before')
    <link href="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}"
          rel="stylesheet">
@endsection

@section('js_after')
    <script src="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>

@endsection


