@extends('Admin.layout')
@section('title', __('messages.Tour dictionary'))

@section('actions')
    <h2><a href="{{ route('dictionary-index') }}">{{ __('messages.List of tour dictionary') }}</a></h2>
    <ul class="actions">
        <li>
            <a href="{{ route('dictionary-create') }}">
                <i class="zmdi zmdi-plus-circle-o"></i>
            </a>
        </li>

    </ul>
@endsection

@section('content')
    <div class="card-body card-padding">
        <div class="table-responsive">
            <table id="data-table-basic" class="table table-striped bootgrid-table" aria-busy="false">
                <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Заголовок</th>
                    <th class="text-center">Sarlavha</th>
                    <th class="text-center">Title</th>
                    <th class="text-center">{{ __('messages.Commands') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($model as $dictionary)
                    <tr data-row-id="{{ $dictionary->id }}">
                        <td class="text-center" style="">{{ $dictionary->id }}</td>
                        <td class="text-center" style=""><a href="{{ route('dictionary-show', ['dictionary' => $dictionary]) }}">{{ $dictionary->name_ru}}</a></td>
                        <td class="text-center" style=""><a href="{{ route('dictionary-show', ['dictionary' => $dictionary]) }}">{{ $dictionary->name_uz}}</a></td>
                        <td class="text-center" style=""><a href="{{ route('dictionary-show', ['dictionary' => $dictionary]) }}">{{ $dictionary->name_en}}</a></td>
                        <td class="text-center" style="">
                            <a href="{{ route('dictionary-update-form', ['dictionary' => $dictionary]) }}" class="btn btn-icon command-edit waves-effect waves-circle"
                                    data-row-id="10253"><span class="zmdi zmdi-edit"></span></a>
                            <button data-id="{{ $dictionary->id }}" type="button" class="btn btn-icon command-delete waves-effect waves-circle"
                                    data-row-id="10253"><span class="zmdi zmdi-delete"></span></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <style>
        td{
            font-size: 14px;
        }
    </style>
@endsection

@section('css')
    <link href="{{ asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css') }}" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('js_after')
    <script src="{{ asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js') }}"></script>
    <script src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script>
        $('#data-table-basic').DataTable(
            {
                columnDefs: [

                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.25/i18n/Russian.json"
                }
            }
        )

        $('.command-delete').click(function(e){
            e.preventDefault()
            newsId = $(this).data('id');
            swal({
                    title: "{{ __('messages.Are you sure?') }}",
                    text: "{{ __('messages.Your will not be able to recover this post!') }}",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "{{ __('messages.Cancel') }}",
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "{{ __('messages.Yes, delete it!') }}",
                    closeOnConfirm: false
                },
                function(){
                    $.ajax({
                        url: "/admin/dictionary/" + newsId + "/delete",
                        type: "post",
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function (response) {
                            swal("{{ __('messages.Deleted!') }}", "{{ __('messages.Your post has been deleted.') }}", "success");
                            window.location.reload()
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal("Error something went wrong!", textStatus, "danger");
                        }
                    });

                });
        })
    </script>
@endsection


