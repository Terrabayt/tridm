@extends('Admin.layout')
@section('title', __('messages.News'))

@section('actions')
    <h2><a href="{{ route('news-index') }}">{{ __('messages.List of news') }}</a></h2>
@endsection

@section('content')
    <div class="card-body card-padding">
        <form action="{{ isset($model) ? route('career-update', $model) : route('career-store') }}" method="POST"
              enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <div class="fg-line">
                    <p class="f-500 m-b-15 c-black">Под</p>

                    <select class="selectpicker bs-select-hidden" name="parent_id" data-live-search="true">
                        <option value="">---</option>
                        @if(isset($careers))
                            @foreach($careers as $item)
                                <option
                                    value="{{$item->id}}" {{ isset($model) ? ($item->id === $model->parent_id ? 'selected' : '') : '' }}>{{ $item->{ 'name_'. app()->getLocale() } }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div role="tabpanel">
                <ul class="tab-nav text-center" role="tablist">
                    <li class="active"><a href="#uz" aria-controls="uz" role="tab" data-toggle="tab"
                                          aria-expanded="true">{{ __('messages.Uzbek') }}</a></li>
                    <li role="presentation" class=""><a href="#ru" aria-controls="ru" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.Russian') }}</a></li>
                    <li role="presentation" class=""><a href="#en" aria-controls="en" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.English') }}</a></li>
                </ul>

                <div class="tab-content">
                    @foreach(Config::get('languages') as $lang => $name)
                        <div role="tabpanel" class="tab-pane {{ $loop->index === 0 ? 'active' : '' }}" id="{{ $lang }}">
                            <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Name') }}</p>
                            <div class="form-group">
                                <div class="fg-line">
                                    <input type="text" class="form-control" name="name_{{ $lang }}"
                                           placeholder="{{ __('messages.Name') }}"
                                           value="{{ isset($model) ? $model->{'name_' . $lang} : old('name_'. $lang) }}">
                                </div>
                            </div>
                            <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Content') }}</p>
                            <div class="form-group">
                                <div class="fg-line">
                                <textarea class="form-control ckeditor" name="content_{{ $lang }}"
                                          placeholder="{{ __('messages.Description') }}">{{ isset($model) ? $model->{'content_'.$lang} : '' }}</textarea>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <input type="hidden" value="{{ isset($model) ? $model->group_id : $group->id }}" name="group_id">
            <p class="c-black f-500 m-b-20 m-t-20">Цвет фона</p>
            <input name="color" type="color" value="{{ isset($model) ? $model->color : old('color') }}"
                   title="load your color">

            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary waves-effect">{{ __('messages.Save') }}</button>
            </div>
        </form>
    </div>
@endsection

@section('css-before')
    <link href="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}"
          rel="stylesheet">
    <link
        href="{{ asset('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}"
        rel="stylesheet">
@endsection

@section('js_after')
    <script
        src="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('vendors/fileinput/fileinput.min.js') }}"></script>
    <script
        src="{{ asset('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('vendors/autosize/dist/autosize.min.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.16.1/full/ckeditor.js"></script>
    <script>
        autosize($('.auto-size'));
        CKEDITOR.replace('content_uz', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.replace('content_ru', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.replace('content_en', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.editorConfig = function (config) {
            config.extraPlugins = 'abbr';
        };
    </script>
@endsection


