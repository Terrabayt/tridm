@extends('Admin.layout')
@section('title', isset($model) ? $model->{'name_'. app()->getLocale()} : 'Категория')

@section('actions')
    <h2><a href="{{ route('career-group-index') }}">{{ isset($model) ? $model->{'name_'. app()->getLocale()} : 'Категория' }}</a></h2>
@endsection

@section('content')
    <div class="card-body card-padding">
        <form action="{{ !isset($model) ? route('career-group-store') : route('career-group-update', $model) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div role="tabpanel">
                <ul class="tab-nav text-center" role="tablist">
                    <li class="active"><a href="#uz" aria-controls="uz" role="tab" data-toggle="tab"
                                          aria-expanded="true">{{ __('messages.Uzbek') }}</a></li>
                    <li role="presentation" class=""><a href="#ru" aria-controls="ru" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.Russian') }}</a></li>
                    <li role="presentation" class=""><a href="#en" aria-controls="en" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.English') }}</a></li>
                </ul>

                <div class="tab-content">
                    @foreach(Config::get('languages') as $lang => $name)
                    <div role="tabpanel" class="tab-pane {{ $loop->index === 0 ? 'active' :'' }}" id="{{ $lang }}">
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Title') }}</p>
                        <div class="form-group">
                            <div class="fg-line">
                                <input type="text" class="form-control" value="{{ isset($model) ? $model->{'name_'.$lang} : old('name_'.$lang) }}" name="name_{{ $lang }}"
                                       placeholder="{{ __('messages.Title') }}">
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <input name="color" type="color" value="{{ isset($model) ? $model->color : old('color') }}" title="load your color" onclick="">

            <div class="form-group text-right">
            <button type="submit" class="btn btn-primary waves-effect">{{ __('messages.Save') }}</button>
            </div>
        </form>
    </div>
@endsection

@section('css-before')
    <link href="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}"
          rel="stylesheet">
    <link href="{{ asset('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}"
          rel="stylesheet">
@endsection

@section('js_after')
    <script src="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
@endsection


