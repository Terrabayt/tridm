@extends('Admin.layout')
@section('title', $events->getName() ?? $events->id)

@section('actions')
    <h2><a href="{{ route('events-index') }}">{{ __('messages.List of events') }}</a></h2>
@endsection

@section('content')
    <div class="card-body card-padding">
        <form action="{{ route('events-update', ['events' => $events]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="checkbox m-b-15">
                <label>
                    <input type="checkbox" {{ $events->active ? 'checked' : '' }} id="active" name="active">
                    <i class="input-helper"></i>
                    {{ __('messages.Publish') }}
                </label>
            </div>
            <div role="tabpanel">
                <ul class="tab-nav text-center" role="tablist">
                    <li class="active"><a href="#uz" aria-controls="uz" role="tab" data-toggle="tab"
                                          aria-expanded="true">{{ __('messages.Uzbek') }}</a></li>
                    <li role="presentation" class=""><a href="#ru" aria-controls="ru" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.Russian') }}</a></li>
                    <li role="presentation" class=""><a href="#en" aria-controls="en" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.English') }}</a></li>
                </ul>
                <div class="tab-content">
                    @foreach (Config::get('languages') as $lang => $language)
                        <div role="tabpanel" class="tab-pane {{ $lang == 'uz' ? 'active': '' }}" id="{{ $lang }}">
                            <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Title') }}</p>
                            <div class="form-group">
                                <div class="fg-line">
                                    <input type="text" class="form-control" value="{{ $events->{'name_' . $lang} }}"
                                           name="{{'name_' . $lang}}"
                                           placeholder="{{ __('messages.Title') }}">
                                </div>
                            </div>

                            <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Content') }}</p>
                            <div class="form-group">
                                <div class="fg-line">
                                <textarea class="form-control ckeditor" name="{{'content_'.app()->getLocale()}}"
                                          value="{{ $events->{'content_'.$lang} }}"
                                          placeholder="{{ __('messages.Description') }}">{{ $events->{'content_'.$lang} }}</textarea>
                                </div>
                            </div>

                            <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Comment') }}</p>
                            <div class="form-group">
                                <div class="fg-line">
                                <textarea class="form-control auto-size" name="{{'comment_' . $lang}}"
                                          placeholder="{{ __('messages.Comment') }}" data-autosize-on="true"
                                          style="overflow: hidden; overflow-wrap: break-word; height: 43px;">{{ $events->{'comment_' . $lang} }}</textarea>
                                </div>
                            </div>

                            <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Location') }}</p>
                            <div class="form-group">
                                <div class="fg-line">
                                    <input type="text" class="form-control" value="{{ $events->{'location_' . $lang} }}"
                                           name="{{'location_' . $lang}}"
                                           placeholder="{{ __('messages.Location') }}">
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Fee') }}</p>
            <div class="form-group">
                <div class="fg-line">
                    <input type="text" class="form-control" value="{{ $events->fee }}" name="fee"
                           placeholder="{{ __('messages.Fee') }}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Start date') }}</p>
                    <div class="input-group form-group text-right">
                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                        <div class="dtp-container fg-line">
                            <input type="text" class="form-control date-time-picker"
                                   value="{{ $events->start_date }}" name="start_date"
                                   placeholder="{{ __('messages.Start date') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.End date') }}</p>
                    <div class="input-group form-group text-right">
                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                        <div class="dtp-container fg-line">
                            <input type="text" class="form-control date-time-picker"
                                   value="{{ $events->end_date }}" name="end_date"
                                   placeholder="{{ __('messages.End date') }}">
                        </div>
                    </div>
                </div>
            </div>


            <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Photo') }}</p>
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput">
                    @if($events->image)
                        <img src="{{ asset('images/events/main/' . $events->image) }}" alt="">
                    @endif
                </div>
                <div>
                                    <span class="btn btn-info btn-file waves-effect">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="photo">
                                    </span>
                    <a href="#" class="btn btn-danger fileinput-exists waves-effect" data-dismiss="fileinput">Remove</a>
                </div>
            </div>

            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary waves-effect">{{ __('messages.Save') }}</button>
            </div>
        </form>
    </div>
@endsection

@section('css-before')
    <link href="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}"
          rel="stylesheet">
    <link
        href="{{ asset('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}"
        rel="stylesheet">
@endsection

@section('js_after')
    <script
        src="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('vendors/fileinput/fileinput.min.js') }}"></script>
    <script
        src="{{ asset('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('vendors/autosize/dist/autosize.min.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.16.1/full/ckeditor.js"></script>
    <script>
        autosize($('.auto-size'));
        CKEDITOR.replace('content_uz', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.replace('content_ru', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.replace('content_en', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form',
        });
        CKEDITOR.editorConfig = function (config) {
            config.extraPlugins = 'abbr';
        };

    </script>
@endsection


