@extends('Admin.layout')
@section('title', $events->getName())

@section('actions')
    <h2><a href="{{ route('events-index') }}">{{ __('messages.List of events') }}</a></h2>

    <ul class="actions">
        <li>
            <a href="{{ route('events-update-form', [ 'events' => $events]) }}">
                <i class="zmdi zmdi-edit"></i>
            </a>
        </li>

    </ul>
@endsection

@section('content')
    <div class="card-body card-padding">
            <span style="color: white" class="p-5 {{ $events->active ? 'bgm-green ' : 'bgm-red'}}">{{ $events->active ? (__('messages.Published')) :  (__('messages.Not published'))}}</span>
            <div role="tabpanel">
                <ul class="tab-nav text-center" role="tablist">
                    <li class="active"><a href="#uz" aria-controls="uz" role="tab" data-toggle="tab"
                                          aria-expanded="true">{{ __('messages.Uzbek') }}</a></li>
                    <li role="presentation" class=""><a href="#ru" aria-controls="ru" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.Russian') }}</a></li>
                    <li role="presentation" class=""><a href="#en" aria-controls="en" role="tab"
                                                        data-toggle="tab"
                                                        aria-expanded="false">{{ __('messages.English') }}</a></li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="uz">
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Title') }}</p>
                        <p>{{ $events->name_uz }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Content') }}</p>
                        {!! $events->content_uz !!}
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Comment') }}</p>
                        <p>{{ $events->comment_uz }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Location') }}</p>
                        <p>{{ $events->location_uz }}</p>
                        <hr>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="ru">
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Title') }}</p>
                        <p>{{ $events->name_ru }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Content') }}</p>
                        {!! $events->content_ru !!}
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Comment') }}</p>
                        <p>{{ $events->comment_ru }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Location') }}</p>
                        <p>{{ $events->location_ru }}</p>
                        <hr>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="en">
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Title') }}</p>
                        <p>{{ $events->name_en }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Content') }}</p>
                        {!! $events->content_en !!}
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Comment') }}</p>
                        <p>{{ $events->comment_en }}</p>
                        <hr>
                        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Location') }}</p>
                        <p>{{ $events->location_en }}</p>
                        <hr>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-md-6">
                <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Start date') }}</p>
                <p>{{ $events->start_date }}</p>
            </div>
            <div class="col-md-6">
                <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.End date') }}</p>
                <p>{{ $events->end_date }}</p>
            </div>
        </div>
        <hr>
        <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Fee') }}</p>
        <p>{{ $events->fee }}</p>
            <p class="c-black f-500 m-b-20 m-t-20">{{ __('messages.Photo') }}</p>
        <img src="{{ asset('images/events/main/' . $events->image) }}" alt="">
    </div>
@endsection

@section('css-before')
    <link href="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css') }}"
          rel="stylesheet">
@endsection

@section('js_after')
    <script src="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
@endsection


