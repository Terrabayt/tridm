@extends('Admin.layout')
@section('title', __('menu.Events'))

@section('actions')
    <h2><a href="{{ route('events-index') }}">{{ __('messages.List of events') }}</a></h2>
    <ul class="actions">
        <li>
            <a href="{{ route('events-create') }}">
                <i class="zmdi zmdi-plus-circle-o"></i>
            </a>
        </li>

    </ul>
@endsection

@section('content')
    <div class="card-body card-padding">
        <div class="table-responsive">
            <table id="data-table-basic" class="table table-striped bootgrid-table" aria-busy="false">
                <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Заголовок</th>
                    <th class="text-center">Sarlavha</th>
                    <th class="text-center">Title</th>
                    <th class="text-center">{{ __('messages.Start date') }}</th>
                    <th class="text-center">{{ __('messages.Commands') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($model as $events)
                    <tr data-row-id="{{ $events->id }}">
                        <td class="text-center" style="">{{ $events->id }}</td>
                        <td class="text-center" style=""><a href="{{ route('events-show', ['events' => $events]) }}">{{ $events->name_ru}}</a></td>
                        <td class="text-center" style=""><a href="{{ route('events-show', ['events' => $events]) }}">{{ $events->name_uz}}</a></td>
                        <td class="text-center" style=""><a href="{{ route('events-show', ['events' => $events]) }}">{{ $events->name_en}}</a></td>
                        <td class="text-center" style="">{{ $events->start_date }}</td>
                        <td class="text-center" style="">
                            <a href="{{ route('events-update-form', ['events' => $events]) }}" class="btn btn-icon command-edit waves-effect waves-circle"
                                    data-row-id="10253"><span class="zmdi zmdi-edit"></span></a>
                            <button data-id="{{ $events->id }}" type="button" class="btn btn-icon command-delete waves-effect waves-circle"
                                    data-row-id="10253"><span class="zmdi zmdi-delete"></span></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <style>
        td{
            font-size: 14px;
        }
    </style>
@endsection

@section('css')
    <link href="{{ asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css') }}" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('js_after')
    <script src="{{ asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js') }}"></script>
    <script src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script>
        $('#data-table-basic').DataTable(
            {
                columnDefs: [
                    { orderable: false, targets: 5 }
                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.25/i18n/Russian.json"
                },
                "order": [[ 4, "asc" ]]
            }
        )

        $('.command-delete').click(function(e){
            e.preventDefault()
            eventsId = $(this).data('id');
            swal({
                    title: "{{ __('messages.Are you sure?') }}",
                    text: "{{ __('messages.Your will not be able to recover this post!') }}",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "{{ __('messages.Cancel') }}",
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "{{ __('messages.Yes, delete it!') }}",
                    closeOnConfirm: false
                },
                function(){
                    $.ajax({
                        url: "/admin/events/" + eventsId + "/delete",
                        type: "post",
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function (response) {
                            swal("{{ __('messages.Deleted') }}", "{{ __('messages.Your post has been deleted.') }}", "success");
                            window.location.reload()
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal("Error something went wrong!", textStatus, "danger");
                        }
                    });

                });
        })
    </script>
@endsection


