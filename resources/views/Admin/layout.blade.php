<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    @yield('css-before')
    <!-- Vendor CSS -->
    <link href="{{asset('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet"/>
    <link
        href="{{ asset('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css') }}"
        rel="stylesheet"/>

    <!-- CSS -->
    <link href="{{ asset('css/admin/app.min.1.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/admin/app.min.2.css') }}" rel="stylesheet"/>
    @yield('css')
</head>
<body>
<header id="header" class="clearfix" data-current-skin="blue">
    <ul class="header-inner">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>

        <li class="logo hidden-xs">
            <a href="index.html">{{ __('messages.Admin panel') }}</a>
        </li>
    </ul>
</header>
<section id="main" data-layout="layout-1">
   @include('Admin.includes.menu')
    <section id="content">
        <div class="container-fluid">
            <div class="block-header">
                @yield('actions')
            </div>
            <div class="card">
                @yield('content')
            </div>
        </div>
    </section>
</section>

<script src="{{ asset('vendors/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendors/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('vendors/bower_components/Waves/dist/waves.min.js') }}"></script>
<script src="{{ asset('vendors/bootstrap-growl/bootstrap-growl.min.js') }}"></script>
<script src="{{ asset('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
@yield('js_after')
<script src="{{ asset('js/admin/functions.js') }}"></script>
</body>
</html>
