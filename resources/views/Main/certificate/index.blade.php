@extends('Main.layout')
@section('title', $user->name)

@section('breadcrumb')
    @include('Main.includes.breadcrumb', [
    'title' => $user->name,
    'items' => [
]
])
@endsection
@section('main-content')
    <section class="section-70 section-md-114">
        <h4><b>{{ __('messages.Full name') }}</b>: {{ $user->name }}</h4>
        <h4><b>{{ __('messages.Course name') }}</b>: {{ $course->getName() }}</h4>
        <h4><b>{{ __('messages.Finished date') }}</b>: {{ $course->created_at->format('d-m-Y') }}</h4>
        <a href="{{ asset("certificates/{$user->id}/{$course->id}/{$course->name_ru}.jpg") }}">
            <img src="{{ asset("certificates/{$user->id}/{$course->id}/{$course->name_ru}.jpg") }}" height="400"  alt="">
        </a>
    </section>
@endsection
