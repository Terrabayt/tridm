@extends('Main.main-layout')
@section('main-content')
    <section class="section-70 section-md-114">
        <div class="shell">
            <div class="range range-xs-center">
                <div class="cell-sm-8 cell-md-8 text-md-left">
                    @yield('content')
                </div>
                <div class="cell-md-4 text-left cell-xs-8 offset-top-85 offset-md-top-0">
                    <aside class="inset-md-left-30">
                        @include('Main.includes.sidebar')
                    </aside>
                </div>
            </div>
        </div>
    </section>
@endsection
