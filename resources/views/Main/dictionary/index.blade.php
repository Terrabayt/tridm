@extends('Main.main-layout')
@section('title', __('messages.Tour dictionary'))

@section('breadcrumb')
    @include('Main.includes.breadcrumb', ['title' => __('messages.Tour dictionary'),'items' => [__('messages.Tour dictionary') => '']])
@endsection
@section('main-content')
    @livewireStyles
    <main class="section-sm-70">
        <div class="container">
        <h5 class="text-right" style="margin-bottom: 20px;">{{ __('messages.in database', ['count' => $count]) }}</h5>
        </div>
    @livewire('dictionary')
    </main>
    @livewireScripts
@endsection
