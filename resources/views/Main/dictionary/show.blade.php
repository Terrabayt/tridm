@extends('Main.layout')
@section('title', $model->{ 'name_' . app()->getLocale()})
@section('description', $model->{ 'description_' . app()->getLocale()})
@section('keywords', $model->{ 'description_' . app()->getLocale()})

@section('breadcrumb')
    @include('Main.includes.breadcrumb', [
    'title' => $model->{ 'name_' . app()->getLocale() } ,
    'items' => [
        __('messages.Tour dictionary') => route('main-dictionary-index'),
        $model->{ 'name_' . app()->getLocale() } => ''
]
])
@endsection
@section('content')

    <div id="fb-root" class=" fb_reset">
        <div style="position: absolute; top: -10000px; width: 0px; height: 0px;">
            <div></div>
        </div>
    </div>

    <h2 class="text-bold">
        {{ $model->{ 'name_' . app()->getLocale() } }}
    </h2>
    <hr class="divider bg-madison hr-md-left-0">
    <div class="offset-md-top-47 offset-top-20">
        <ul class="post-news-meta list list-inline list-inline-xl">
        </ul>
    </div>
    <div class="offset-top-30">
        {!! $model->{ 'content_' . app()->getLocale() } !!}
    </div>




@endsection
