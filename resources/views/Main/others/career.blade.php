@extends('Main.main-layout',[])
@section('url', url()->full())

@section('breadcrumb')
    @include('Main.includes.breadcrumb', [
    'title' => __('messages.My Career') ,
    'items' => [
        __('messages.My Career') => '',
]
])
@endsection
@section('main-content')
    <div class="section-70 section-md-114">
    <div class="container-fluid " style="padding-top: 2rem;">
        <div class="demo">
            <div class="">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            @foreach($model as $key => $item)
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading{{ $loop->index }}">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                               href="#collapse{{ $loop->index }}" aria-expanded="false"
                                               aria-controls="collapse{{ $loop->index }}" style="background: {{ getColorFromGroup('name_' . app()->getLocale(), $key)  }}">
                                                {{ $key }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{ $loop->index }}" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="heading{{ $loop->index }}">
                                        <div class="panel-body">
                                            <ol class="wtree">
                                                    {!! \App\Http\Services\RearrangeService::generateBlade($item) !!}
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div id="preloader-ajax"><div class="lds-grid"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    </div>
    <div class="modal fade" id="modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <style>
        #accordion .panel {
            border: none;
            border-radius: 5px;
            box-shadow: none;
            margin-bottom: 10px;
            background: transparent;
        }
        #accordion .panel-heading {
            padding: 0;
            border: none;
            border-radius: 5px;
            background: transparent;
            position: relative;
        }
        #accordion .panel-title a {
            display: block;
            padding: 20px 30px;
            margin: 0;
            background: rgba(0, 0, 0, 0.4);
            font-size: 17px;
            font-weight: bold;
            color: #fff;
            text-transform: uppercase;
            letter-spacing: 1px;
            border: none;
            border-radius: 5px;
            position: relative;
        }
        #accordion .panel-title a.collapsed {
            border: none;
        }
        #accordion .panel-title a:before,
        #accordion .panel-title a.collapsed:before {
            content: "\f107";
            font-family: "FontAwesome";
            width: 30px;
            height: 30px;
            line-height: 27px;
            text-align: center;
            font-size: 25px;
            font-weight: 900;
            color: #fff;
            position: absolute;
            top: 15px;
            right: 30px;
            transform: rotate(180deg);
            transition: all .4s cubic-bezier(0.080, 1.090, 0.320, 1.275);
        }

        #accordion .panel-title a.collapsed:before {
            color: rgba(255, 255, 255, 0.5);
            transform: rotate(0deg);
        }

        #accordion .panel-body {
            padding: 20px 30px;
            background: rgb(255 255 255 / 49%);
            font-size: 15px;
            color: #fff;
            line-height: 28px;
            letter-spacing: 1px;
            border-top: none;
            border-radius: 5px;
        }

        ol {
            margin-left: 30px;
            counter-reset: item;
        }

        .wtree li {
            list-style-type: none;
            margin: 10px 0 10px 10px;
            position: relative;
        }

        .wtree li:before {
            content: "";
            counter-increment: item;
            position: absolute;
            top: -10px;
            left: -30px;
            border-left: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            width: 30px;
            height: 15px;
        }

        .wtree li:after {
            position: absolute;
            content: "";
            top: 5px;
            left: -30px;
            border-left: 1px solid #ddd;
            border-top: 1px solid #ddd;
            width: 30px;
            height: 100%;
        }

        .wtree li:last-child:after {
            display: none;
        }

        .wtree li span {
            display: block;
            border: 1px solid #ddd;
            padding: 10px;
            color: #666666;
            text-decoration: none;
            cursor: pointer;
        }

        .wtree li span:before {
            /*content: counters(item, ".") " ";*/
        }

        .wtree li span:hover, .wtree li span:focus {
            color: #000;
            border: 1px solid #474747;
        }

        .wtree li span:hover + ol li span, .wtree li span:focus + ol li span {
            color: #000;
            border: 1px solid #474747;
        }

        .wtree li span:hover + ol li:after, .wtree li span:hover + ol li:before, .wtree li span:focus + ol li:after, .wtree li span:focus + ol li:before {
            border-color: #474747;
        }

        input, label {
            margin: 12px 0px 20px 0px;
        }

        label {
            padding-left: 6px;
            padding-right: 12px;
        }
        .modal-dialog{
            background: white;
        }
        /*loader*/
        .lds-grid {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }
        .lds-grid div {
            position: absolute;
            width: 16px;
            height: 16px;
            border-radius: 50%;
            background: #fff;
            animation: lds-grid 1.2s linear infinite;
        }
        .lds-grid div:nth-child(1) {
            top: 8px;
            left: 8px;
            animation-delay: 0s;
        }
        .lds-grid div:nth-child(2) {
            top: 8px;
            left: 32px;
            animation-delay: -0.4s;
        }
        .lds-grid div:nth-child(3) {
            top: 8px;
            left: 56px;
            animation-delay: -0.8s;
        }
        .lds-grid div:nth-child(4) {
            top: 32px;
            left: 8px;
            animation-delay: -0.4s;
        }
        .lds-grid div:nth-child(5) {
            top: 32px;
            left: 32px;
            animation-delay: -0.8s;
        }
        .lds-grid div:nth-child(6) {
            top: 32px;
            left: 56px;
            animation-delay: -1.2s;
        }
        .lds-grid div:nth-child(7) {
            top: 56px;
            left: 8px;
            animation-delay: -0.8s;
        }
        .lds-grid div:nth-child(8) {
            top: 56px;
            left: 32px;
            animation-delay: -1.2s;
        }
        .lds-grid div:nth-child(9) {
            top: 56px;
            left: 56px;
            animation-delay: -1.6s;
        }
        @keyframes lds-grid {
            0%, 100% {
                opacity: 1;
            }
            50% {
                opacity: 0.5;
            }
        }
        #preloader-ajax{
            background: rgb(0 0 0 / 80%);
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            z-index: 99999;
            display: none;
            justify-content: center;
            align-items: center;

        }
    </style>

@endsection
@section('js_after')
    <script>
        $(document).ready(function(){
            $('.wtree li span').click(function(){
                // AJAX request
                $('#preloader-ajax').css('display','flex')
                $.ajax({
                    url: '/get-career/' + $(this).data('id'),
                    type: 'get',
                    success: function(response){
                        // Add response in Modal body
                        $('.modal-body').html(response);
                        $('.modal-title').text($(this).data('name'));

                        // Display Modal
                        $('#preloader-ajax').css('display','none')
                        $('#modal').modal('show');
                    }
                });
            });
        });
    </script>
@endsection
