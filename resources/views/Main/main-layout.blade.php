<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="description" content="@yield('description')">
    <meta property="og:title" content="@yield('title')">
    <meta property="og:image" content="@yield('image')">
    <meta property="og:image:width" content="900">
    <meta property="og:image:height" content="470">
    <meta property="og:url" content="@yield('url')">
    <meta property="og:site_name" content="Tridm.uz">
    <meta property="og:type" content="@yield('type')">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="@yield('title')">
    <meta property="twitter:description" content="@yield('description')">
    <meta property="twitter:image" content="@yield('image')">
    <meta property="twitter:url" content="@yield('url')">
    <meta name="title" content="@yield('title')">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ asset('css/main/style.css') }}">
    <link href="{{ asset('images/other/gerb1.png') }}" rel="icon">
</head>
<body>
<!-- Page-->
<div class="page text-center">
    <!-- Page Header-->
@include('Main.includes.header')
<!-- Page Content-->
    @if(!request()->is('/'))
    @yield('breadcrumb')
    @endif
    <main class="page-content">
        @yield('main-content')
    </main>
    <!-- Corporate footer-->
    <footer class="page-footer">
        <div class="shell-wide">
            <div class="hr bg-gray-light"></div>
        </div>
        <div class="shell section-60">
            <div class="range range-lg-justify range-xs-center">
                <div class="cell-md-3 cell-lg-3">
                    <!--Footer brand--><a class="reveal-inline-block" href="/"><img width='170' height='140'
                                                                                    src='{{ asset('images/other/институт развития туризма лого.png') }}'
                                                                                    alt=''/>
                        <div>
                            <h6 class="barnd-name text-bold offset-top-25">{{ __('messages.Scientific research institute for the study of the problems of cultural heritage objects and tourism development') }}</h6>
                        </div>
                        <div>
                            <p class="brand-slogan text-gray text-italic font-accent">{{ __('messages.under the Ministry of Tourism and cultural heritage of the Republic of Uzbekistan') }}</p>
                        </div>
                    </a>
                </div>
                <div class="cell-xs-10 cell-md-5 cell-lg-4 text-lg-left offset-top-50 offset-md-top-0">
                    <h6 class="text-bold">{{ __('menu.Contacts') }}</h6>
                    <div class="text-subline"></div>
                    <div class="offset-top-30">
                        <ul class="list-unstyled contact-info list">
                            <li>
                                <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                                    <div class="unit-left"><span
                                            class="icon mdi mdi-phone text-middle icon-xs text-madison"></span></div>
                                    <div class="unit-body">
                                        <a class="text-dark" href="tel:+998712687371">+99871 268-73-71,</a>
                                        <a class="text-dark reveal-block reveal-md-inline-block"
                                           href="tel:+998712687367">+99871 268-73-67,</a>
                                        <a class="text-dark reveal-block reveal-md-inline-block"
                                           href="tel:+998712680025">+99871 268-00-25</a>
                                    </div>
                                </div>
                            </li>
                            <li class="offset-top-15">
                                <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                                    <div class="unit-left"><span
                                            class="icon mdi mdi-map-marker text-middle icon-xs text-madison"></span>
                                    </div>
                                    <div class="unit-body text-left"><a class="text-dark"
                                                                        href="#">{{ __('messages.self address') }}</a>
                                    </div>
                                </div>
                            </li>
                            <li class="offset-top-15">
                                <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                                    <div class="unit-left"><span
                                            class="icon mdi mdi-email-open text-middle icon-xs text-madison"></span>
                                    </div>
                                    <div class="unit-body"><a href="mailto:info@tridm.uz">info@tridm.uz</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="offset-top-15 text-left">
                        <ul class="list-inline list-inline-xs list-inline-madison">
                            <li><a class="icon icon-xxs fa-facebook icon-circle icon-gray-light-filled" target="_blank"
                                   href="https://www.facebook.com/tridm.uz/"></a></li>
                            {{--                                <li><a class="icon icon-xxs fa-twitter icon-circle icon-gray-light-filled" href="#"></a></li>--}}
                            {{--                                <li><a class="icon icon-xxs fa-google icon-circle icon-gray-light-filled" href="#"></a></li>--}}
                                                            <li><a class="icon icon-xxs fa-instagram icon-circle icon-gray-light-filled" target="_blank" href="https://www.instagram.com/tourism_development_institute/"></a></li>
                                                            <li><a class="icon icon-xxs fa-youtube-play icon-circle icon-gray-light-filled" target="_blank" href="https://www.youtube.com/channel/UCVa0pTUO2nvqV5d31LWGxwQ"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="cell-xs-10 cell-md-8 cell-lg-4 text-lg-left offset-top-50 offset-lg-top-0">
                    <h6 class="text-bold">{{ __('messages.Newsletter') }}</h6>
                    <div class="text-subline"></div>
                    <div class="offset-top-30 text-left">
                        <p>{{ __('messages.Enter your email address to receive the latest Tourism Development Institute news') }}</p>
                    </div>
                    <div class="offset-top-10">
                        <form class="rd-mailform form-subscribe" data-form-output="form-subscribe-footer"
                              data-form-type="subscribe" method="post" action="#">
                            <div class="form-group">
                                <div class="input-group input-group-sm">
                                    <input class="form-control" placeholder="{{ __('messages.Your email') }}"
                                           type="email" name="email" data-constraints="@Required @Email"/><span
                                        class="input-group-btn">
                        <button class="btn btn-sm btn-primary"
                                type="submit">{{ __('messages.Subscribe') }}</button></span>
                                </div>
                            </div>
                            <div class="form-output" id="form-subscribe-footer"></div>
                        </form>
                        <IMG height=31 src="https://cnt0.www.uz/counter/collect?id=46488&pg=http%3A//uzinfocom.uz&&col=F49918&amp;t=ffffff&amp;p=008ACC" width=88 border=0 alt="Топ рейтинг www.uz" style="float: right">
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-madison context-dark">
            <div class="shell text-md-left section-5">
                <p>&copy; <span id="copyright-year"></span></p>
            </div>
        </div>
    </footer>
</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<!-- Java script-->
<script src="{{ asset('js/main/core.min.js') }}"></script>
<script src="{{ asset('js/main/script.js') }}"></script>
<!-- START WWW.UZ TOP-RATING -->
<SCRIPT language="javascript" type="text/javascript">
    top_js="1.0";top_r="id=46488&r="+escape(document.referrer)+"&pg="+escape(window.location.href);document.cookie="smart_top=1; path=/"; top_r+="&c="+(document.cookie?"Y":"N")
</SCRIPT>
<SCRIPT language="javascript1.1" type="text/javascript">
    top_js="1.1";top_r+="&j="+(navigator.javaEnabled()?"Y":"N")
</SCRIPT>
<SCRIPT language="javascript1.2" type="text/javascript">
    top_js="1.2";top_r+="&wh="+screen.width+'x'+screen.height+"&px="+
        (((navigator.appName.substring(0,3)=="Mic"))?screen.colorDepth:screen.pixelDepth)
</SCRIPT>
<SCRIPT language="javascript1.3" type="text/javascript">
    top_js="1.3";
</SCRIPT>
<SCRIPT language="JavaScript" type="text/javascript">
    top_rat="&col=F49918&t=ffffff&p=008ACC";top_r+="&js="+top_js+"";document.write('<a href="http://www.uz/ru/res/visitor/index?id=46488" target=_top><img src="https://cnt0.www.uz/counter/collect?'+top_r+top_rat+'" width=88 height=31 style="display:none;" border=0 alt="Топ рейтинг www.uz"></a>')//-->
</SCRIPT><NOSCRIPT><IMG height=0 src="https://cnt0.www.uz/counter/collect?id=46488&pg=http%3A//uzinfocom.uz&&col=F49918&amp;t=ffffff&amp;p=008ACC" width=0 border=0 alt="Топ рейтинг www.uz" style="display: none"></NOSCRIPT><!-- FINISH WWW.UZ TOP-RATING -->
@yield('js_after')
</body>
</html>
