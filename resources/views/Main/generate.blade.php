@extends('Main.layout')
@section('title', $content->{'name_' . app()->getLocale()})
@section('breadcrumb')
    @include('Main.includes.breadcrumb', [
        'title' => $content->{'name_' . app()->getLocale()},
        'items' => [
                   "{$content->{'name_' . app()->getLocale()}}" => '',

        ]
    ])
@endsection
@section('content')

        {!! $content->{'content_' .  app()->getLocale() } !!}
@endsection
