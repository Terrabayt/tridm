@extends('Main.layout')
@section('title', __('messages.News'))

@section('breadcrumb')
    @include('Main.includes.breadcrumb', ['title' => __('messages.News'),'items' => [__('messages.News') => '']])
@endsection
@section('content')
    <div class="range text-sm-left range-xs-center">
        @foreach($model as $news)
            <div class="cell-sm-6">
                <article class="post-news"><a
                        href="{{ route('main-news-show', [ 'news' => $news]) }}"><img
                            class="img-responsive" src="{{ asset('images/news/main/' . $news->image) }}"
                            alt=""></a>
                    <div class="post-news-body">
                        <h6>
                            <a href="{{ route('main-news-show', [ 'news' => $news]) }}">{{ $news->{'name_' . app()->getLocale()} }}</a>
                        </h6>
                        <div class="offset-top-20">
                            <p>{{ \Illuminate\Support\Str::limit($news->{ 'desc_' . app()->getLocale()}, 150, $end='...') }}</p>
                        </div>
                        <div class="post-news-meta offset-top-20"><span
                                class="icon icon-xs mdi mdi-calendar-clock text-middle text-madison"></span><span
                                class="text-middle inset-left-10 text-italic text-black">{{ date('d-m-Y', strtotime($news->date)) }}</span>
                            <span style="float: right"><i class="icon mdi mdi-eye text-middle icon-xs text-madison"></i> {{ $news->views ?? 0 }}</span>

                        </div>
                    </div>
                </article>
            </div>
        @endforeach
        <div class="offset-top-60 text-center">
            {!! $model->links() !!}
        </div>
    </div>
@endsection
