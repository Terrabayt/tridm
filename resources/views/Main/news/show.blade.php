@extends('Main.layout',[])
@section('title', $news->{ 'name_' . app()->getLocale()})
@section('description', $news->getDesc())
@section('keywords', $news->{ 'description_' . app()->getLocale()})
@section('image', asset('images/news/main/' . $news->image))
@section('type', 'article')
@section('url', url()->full())

@section('breadcrumb')
    @include('Main.includes.breadcrumb', [
    'title' => $news->{ 'name_' . app()->getLocale() } ,
    'items' => [
        __('messages.News') => route('main-news-index'),
        $news->{ 'name_' . app()->getLocale() } => ''
]
])
@endsection
@section('content')

    <div id="fb-root" class=" fb_reset">
        <div style="position: absolute; top: -10000px; width: 0px; height: 0px;">
            <div></div>
        </div>
    </div>

    <h2 class="text-bold">
        {{ $news->{ 'name_' . app()->getLocale() } }}
    </h2>
    <hr class="divider bg-madison hr-md-left-0">
    <div class="offset-md-top-47 offset-top-20">
        <ul class="post-news-meta list list-inline list-inline-xl">
            <li><span class="icon icon-xs mdi mdi-calendar-clock text-middle text-madison"></span><span
                    class="text-middle inset-left-10 text-italic text-black">{{ date('d-m-Y', strtotime($news->date)) }}</span></li>
        </ul>
    </div>
    <div class="offset-top-30">
        {!! $news->{ 'content_' . app()->getLocale() } !!}
    </div>




@endsection
