@extends('Main.main-layout')
@section('title', __('messages.Scientific research institute for the study of the problems of cultural heritage objects and tourism development'))
@section('main-content')
    <section>
        <!-- Swiper-->
        <div class="swiper-container swiper-slider" data-autoplay="true" data-height="42.1875%" data-loop="true"
             data-dragable="false" data-min-height="480px" data-slide-effect="true">
            <div class="swiper-wrapper">
                <div class="swiper-slide overlay" data-slide-bg="images/main/slide-02-1920x810.jpg"
                     style="background-position: 80% center">
                    <div class="swiper-slide-caption">
                        <div class="container">
                            <div class="range range-xs-center range-lg-left">
                                <div class="cell-md-9 text-md-left cell-xs-10">
                                    <div data-caption-animate="fadeInUp" data-caption-delay="100">
                                        <h1 class="text-bold"><a href="academics.html">Инвестиции в знания.</a></h1>
                                    </div>
                                    <div
                                        class="offset-top-20 offset-xs-top-40 offset-xl-top-60 inset-lg-right-100 hidden-xs"
                                        data-caption-animate="fadeInUp" data-caption-delay="150">
                                        <h5>В институте вы можете преуспеть во многих областях исследований и получить
                                            выгоду от инвестиций в свое образование и знания, которые помогут вам стать
                                            опытным специалистом..</h5>
                                    </div>
                                    {{--                                    <div class="offset-top-60 offset-sm-top-20 offset-xl-top-40" data-caption-animate="fadeInUp" data-caption-delay="400"><a class="btn btn-primary" href="login-register.html">Sign Up for Excursion</a>--}}
                                    {{--                                        <div class="inset-xs-left-30 reveal-lg-inline-block"><a class="btn btn-default veil reveal-lg-inline-block" href="academics.html">Learn More</a></div>--}}
                                    {{--                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide overlay" data-slide-bg="images/main/slide-01-1920x810.jpg"
                     style="background-position: 80% center">
                    <div class="swiper-slide-caption">
                        <div class="container">
                            <div class="range range-xs-center range-lg-left">
                                <div class="cell-md-9 text-md-left cell-xs-10">
                                    <div data-caption-animate="fadeInUp" data-caption-delay="100">
                                        <h1 class="text-bold"><a href="academics.html">Вдохновение, инновации и
                                                открытия.</a></h1>
                                    </div>
                                    <div
                                        class="offset-top-20 offset-xs-top-40 offset-xl-top-60 inset-lg-right-100 hidden-xs"
                                        data-caption-animate="fadeInUp" data-caption-delay="150">
                                        <h5>Любая успешная карьера начинается с хорошего образования. Вместе с нами вы
                                            получите более глубокие знания по предметам, которые будут особенно полезны
                                            вам при восхождении по карьерной лестнице.</h5>
                                    </div>
                                    {{--                                    <div class="offset-top-60 offset-sm-top-20 offset-xl-top-40 offset" data-caption-animate="fadeInUp" data-caption-delay="400"><a class="btn btn-primary" href="login-register.html">Sign Up for Excursion</a>--}}
                                    {{--                                        <div class="inset-xs-left-30 reveal-lg-inline-block"><a class="btn btn-default veil reveal-lg-inline-block" href="academics.html">Learn More</a></div>--}}
                                    {{--                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide overlay" data-slide-bg="images/main/slide-03-1920x810.jpg"
                     style="background-position: 80% center">
                    <div class="swiper-slide-caption">
                        <div class="container">
                            <div class="range range-xs-center range-lg-left">
                                <div class="cell-md-9 text-md-left cell-xs-10">
                                    <div data-caption-animate="fadeInUp" data-caption-delay="100">
                                        <h1 class="text-bold"><a href="academics.html">Открытые умы. <br
                                                    class="veil reveal-md-inline-block"> Создают будущее.</a></h1>
                                    </div>
                                    <div
                                        class="offset-top-20 offset-xs-top-40 offset-xl-top-60 inset-lg-right-100 hidden-xs"
                                        data-caption-animate="fadeInUp" data-caption-delay="150">
                                        <h5>Стройте свое будущее вместе с нами! Образовательные программы нашего
                                            университета дадут вам необходимые навыки, обучение и знания, чтобы все,
                                            чему вы здесь научились, работало на вас в будущем..</h5>
                                    </div>
                                    {{--                                    <div class="offset-top-60 offset-sm-top-20 offset-xl-top-40" data-caption-animate="fadeInUp" data-caption-delay="400"><a class="btn btn-primary" href="login-register.html">Sign Up for Excursion</a>--}}
                                    {{--                                        <div class="inset-xs-left-30 reveal-lg-inline-block"><a class="btn btn-default veil reveal-lg-inline-block" href="academics.html">Learn More</a></div>--}}
                                    {{--                                    </div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Swiper Pagination-->
            <div class="swiper-pagination"></div>
        </div>
    </section>
    <!-- A Few Words About the University-->
    <section class="section-70 section-md-114">
        <div class="shell">
            <div class="range text-sm-left range-sm-middle range-sm-justify">
                <div class="cell-sm-5 cell-md-4 cell-sm-push-2"><a href="index.html"><img
                            class="img-responsive reveal-inline-block"
                            src="{{ asset('images/other/институт развития туризма лого.png') }}" width="330"
                            height="330" alt=""></a></div>
                <div class="cell-sm-7 cell-md-7 cell-sm-push-1 offset-top-50 offset-md-top-0">
                    <h2 class="text-bold">Деятельность института</h2>
                    <hr class="divider bg-madison hr-sm-left-0">
                    <div class="offset-top-35 offset-md-top-60">
                        <p>Основными целями деятельности Института является проведение фундаментальных и прикладных
                            научных и научно-практических исследований и изысканий в сфере туризма, предоставление услуг
                            в сфере науки и образования, подготовка и переподготовка кадров для туристской отрасли, а
                            также осуществление иной деятельности в области науки и образования.</p>
                    </div>
                    {{--                    <div class="offset-top-30"><a class="btn btn-icon btn-icon-right btn-default" href="history.html"><span class="icon fa-arrow-right"></span><span>Learn More</span></a></div>--}}
                </div>
            </div>
        </div>
    </section>
    <!--Our Featured Courses-->
    <section class="bg-madison context-dark bg-pattern">
        <div class="shell">
            <div class="range range-xs-center range-sm-right offset-top-0">
                <div class="cell-xs-10 cell-sm-7 section-image-aside section-image-aside-left text-sm-left">
                    <div class="section-image-aside-img veil reveal-sm-block"
                         style="background-image: url(images/main/home-01-846x1002.jpg)"></div>
                    <div class="section-image-aside-body section-70 section-md-114 inset-sm-left-70 inset-lg-left-110">
                        <h2 class="text-bold">{{ __('messages.Our Featured Courses') }}</h2>
                        <hr class="divider hr-sm-left-0 bg-white">
                        {{--                        <div class="offset-top-35 offset-md-top-60 text-light">Our Featured Courses are selected through a rigorous process and uniquely created for each semester.</div>--}}
                        <div class="text-left">
                            <div class="offset-top-60">
                                <article class="post-vacation"><a class="post-vacation-img-wrap bg-image"
                                                                  href="academics.html"
                                                                  style="background-image: url(images/main/course-01-150x120.jpg)"></a>
                                    <div class="post-vacation-body">
                                        <div>
                                            <h6 class="post-vacation-title"><a href="academics.html">«Организация
                                                    экскурсионного обслуживания»</a></h6>
                                        </div>
                                        <div class="offset-lg-top-10">
                                            <p>Гид-экскурсовод</p>
                                        </div>
                                        {{--                                        <div class="post-vacation-meta offset-top-10">--}}
                                        {{--                                            <time class="text-dark" datetime="2018-01-01">June 3, 2018</time>--}}
                                        {{--                                            <ul class="list-inline list-unstyled list-inline-primary">--}}
                                        {{--                                                <li><a class="icon icon-xxs mdi mdi-timer-sand" href="#" data-toggle="tooltip" data-placement="top" title="Part time"></a></li>--}}
                                        {{--                                                <li><a class="icon icon-xxs mdi mdi-star" href="#" data-toggle="tooltip" data-placement="top" title="Certified"></a></li>--}}
                                        {{--                                                <li><a class="icon icon-xxs mdi mdi-laptop-chromebook" href="#" data-toggle="tooltip" data-placement="top" title="Online Course"></a></li>--}}
                                        {{--                                            </ul>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                </article>
                            </div>
                            <div class="offset-top-30">
                                <article class="post-vacation"><a class="post-vacation-img-wrap bg-image"
                                                                  href="academics.html"
                                                                  style="background-image: url(images/main/course-02-150x120.jpg)"></a>
                                    <div class="post-vacation-body">
                                        <div>
                                            <h6 class="post-vacation-title"><a href="academics.html">«Организация
                                                    туроператорской деятельности»</a></h6>
                                        </div>
                                        <div class="offset-lg-top-10">
                                            <p>Туроператор</p>
                                        </div>
                                        {{--                                        <div class="post-vacation-meta offset-top-10">--}}
                                        {{--                                            <time class="text-dark" datetime="2018-01-01">June 3, 2018</time>--}}
                                        {{--                                            <ul class="list-inline list-unstyled list-inline-primary">--}}
                                        {{--                                                <li><a class="icon icon-xxs mdi mdi-battery-50" href="#" data-toggle="tooltip" data-placement="top" title="Facultative"></a></li>--}}
                                        {{--                                                <li><a class="icon icon-xxs mdi mdi-star" href="#" data-toggle="tooltip" data-placement="top" title="Certified"></a></li>--}}
                                        {{--                                            </ul>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                </article>
                            </div>
                            <div class="offset-top-30">
                                <article class="post-vacation"><a class="post-vacation-img-wrap bg-image"
                                                                  href="academics.html"
                                                                  style="background-image: url(images/main/course-03-150x120.jpg)"></a>
                                    <div class="post-vacation-body">
                                        <div>
                                            <h6 class="post-vacation-title"><a href="academics.html">«Стандарты
                                                    обслуживания в гостинице»</a></h6>
                                        </div>
                                        <div class="offset-lg-top-10">
                                            <p>Администратор гостиницы</p>
                                        </div>
                                        {{--                                        <div class="post-vacation-meta offset-top-10">--}}
                                        {{--                                            <time class="text-dark" datetime="2018-01-01">June 3, 2018</time>--}}
                                        {{--                                            <ul class="list-inline list-unstyled list-inline-primary">--}}
                                        {{--                                                <li><a class="icon icon-xxs mdi mdi-battery-50" href="#" data-toggle="tooltip" data-placement="top" title="Facultative"></a></li>--}}
                                        {{--                                                <li><a class="icon icon-xxs mdi mdi-star" href="#" data-toggle="tooltip" data-placement="top" title="Certified"></a></li>--}}
                                        {{--                                                <li><a class="icon icon-xxs mdi mdi-laptop-chromebook" href="#" data-toggle="tooltip" data-placement="top" title="Online Course"></a></li>--}}
                                        {{--                                            </ul>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="offset-top-60"><a class="btn btn-primary"
                                                      href="academics.html">{{ __('messages.All courses') }}</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Counters-->
    <section class="section-70 section-md-114">
        <div class="shell">
            <h2 class="text-bold">{{ __('messages.Statistics') }}</h2>
            <hr class="divider bg-madison">
            {{--            <div class="range range-xs-center offset-top-60">--}}
            {{--                <div class="cell-xs-10 cell-sm-8">--}}
            {{--                    <p>John Harrison University was founded on the principle that by pursuing big ideas and sharing what we learn, we make the world a better place. For more than 135 years, we haven’t strayed from that vision.</p>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            <div class="range range-xs-center range-md-left offset-top-65 counters">
                <div class="cell-sm-6 cell-md-3">
                    <!-- Counter type 1-->
                    <div class="counter-type-1"><span
                            class="icon icon-lg icon-outlined text-madison mdi mdi-school"></span>
                        <div class="h3 text-bold text-primary offset-top-15"><span class="counter" data-speed="1300"
                                                                                   data-from="0"
                                                                                   data-to="97"></span><span>%</span>
                        </div>
                        <hr class="divider bg-gray-light divider-sm"/>
                        <div class="offset-top-10">
                            <h6 class="text-black font-accent">{{ __('messages.Graduates') }}</h6>
                        </div>
                    </div>
                </div>
                <div class="cell-sm-6 cell-md-3 offset-top-65 offset-sm-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1"><span
                            class="icon icon-lg icon-outlined text-madison mdi mdi-wallet-travel"></span>
                        <div class="h3 text-bold text-primary offset-top-15"><span class="counter" data-speed="1250"
                                                                                   data-from="0"
                                                                                   data-to="30"></span><span>+</span>
                        </div>
                        <hr class="divider bg-gray-light divider-sm"/>
                        <div class="offset-top-10">
                            <h6 class="text-black font-accent">Сертифицированные профессора</h6>
                        </div>
                    </div>
                </div>
                <div class="cell-sm-6 cell-md-3 offset-top-65 offset-md-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1"><span
                            class="icon icon-lg icon-outlined text-madison mdi mdi-domain"></span>
                        <div class="h3 text-bold text-primary offset-top-15"><span class="counter" data-step="1500"
                                                                                   data-from="0" data-to="8"></span>
                        </div>
                        <hr class="divider bg-gray-light divider-sm"/>
                        <div class="offset-top-10">
                            <h6 class="text-black font-accent">Курсы</h6>
                        </div>
                    </div>
                </div>
                <div class="cell-sm-6 cell-md-3 offset-top-65 offset-md-top-0">
                    <!-- Counter type 1-->
                    <div class="counter-type-1"><span
                            class="icon icon-lg icon-outlined text-madison mdi mdi-account-multiple-outline"></span>
                        <div class="h3 text-bold text-primary offset-top-15"><span class="counter" data-step="500"
                                                                                   data-from="0" data-to="6510"></span>
                        </div>
                        <hr class="divider bg-gray-light divider-sm"/>
                        <div class="offset-top-10">
                            <h6 class="text-black font-accent">{{ __('messages.Students') }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Trusted by Over 6000+ Students-->
    <section class="context-dark">
        <div class="bg-vide overlay-video"
             data-vide-bg="https://livedemo00.template-help.com/wt_prod-16621/university/video/bg-video-1-lg.mp4"
             data-vide-options="posterType: jpg">
            <div class="shell section-100 section-md-160">
                <h2 class="text-bold">Нам доверяют более 6000+ студентов</h2>
                <div class="offset-top-30">
                    {{--                    <p>Join our community of students around the world helping you succeed.</p>--}}
                </div>
                {{--                <div class="offset-top-35 offset-lg-top-70"><a class="btn btn-primary" href="login-register.html">Get Started</a></div>--}}
            </div>
        </div>
    </section>
    <!-- Events-->
    <section class="bg-catskill">
        <div class="shell-wide section-70 section-md-114">
            <h2 class="text-bold">{{ __('menu.Events') }}</h2>
            <hr class="divider bg-madison">
            <div class="range offset-top-60 range-xs-center">
                @foreach($events as $event)
                    <div class="cell-sm-6 cell-md-5 cell-xl-3 offset-top-50 offset-sm-top-0">
                        <article class="post-event">
                            <div class="post-event-img-overlay"><img class="img-responsive"
                                                                     src="{{ asset('images/events/main/' . $event->image) }}"
                                                                     width="420" height="420" alt="">
                                <div class="post-event-overlay context-dark">
                                    <div class="offset-top-20"><a class="btn btn-default"
                                                                  href="{{ route('main-news-show', [ 'news' => $event]) }}">{{ __('messages.Learn more') }}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="unit unit-lg unit-lg-horizontal">
                                <div class="unit-left">
                                    <div class="post-event-meta text-center">

                                        <div
                                            class="h3 text-bold reveal-inline-block reveal-lg-block">{{ date('d', strtotime($event->start_date)) }}</div>
                                        <p class="reveal-inline-block reveal-lg-block">{{ __('months.'. date('F', strtotime($event->start_date))) }}</p>
                                        <span
                                            class="text-bold reveal-inline-block reveal-lg-block inset-left-10 inset-lg-left-0">{{ date('H:i', strtotime($event->start_date)) }}</span>
                                    </div>
                                </div>
                                <div class="unit-body">
                                    <div class="post-event-body text-lg-left">
                                        <h6>
                                            <a href="{{ route('main-events-show', [ 'event' => $event]) }}">{{ $event->getName() }}</a>
                                        </h6>
                                        <i class="icon mdi mdi-eye text-middle icon-xs text-madison"></i> {{ $event->views ?? 0 }}
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                @endforeach
            </div>
            <div class="offset-top-50 offset-lg-top-56"><a class="btn btn-icon btn-icon-right btn-primary"
                                                           href="{{ route('main-events-index') }}"><span
                        class="icon fa-arrow-right"></span><span>{{ __('menu.Events calendar') }}</span></a></div>
        </div>
    </section>
    <!-- Testimonials-->
    <section class="bg-madison context-dark position-relative bg-pattern">
        <div class="owl-carousel owl-carousel-default veil-xl-owl-dots veil-owl-nav reveal-xl-owl-nav" data-items="1"
             data-nav="true" data-dots="true"
             data-nav-class="[&quot;owl-prev fa-angle-left&quot;, &quot;owl-next fa-angle-right&quot;]">
            <div>
                <div class="shell section-70 section-xl-100 section-xl-bottom-114">
                    <div class="range range-xs-center range-xs-middle">
                        <div class="cell-sm-3 text-sm-right cell-sm-push-2"><img
                                class="img-responsive reveal-inline-block img-circle"
                                src="images/main/users/user-debra-banks-230x230.jpg" width="230" height="230" alt="">
                        </div>
                        <div class="cell-sm-9 cell-sm-push-1">
                            <div class="quote-classic-boxed text-left">
                                <div class="quote-body">
                                    <q>Когда вы работаете полный рабочий день во время учебы, вам нужно жертвовать
                                        личным временем, а это означало, что я серьезно относился к своей учебе. Моей
                                        целью было не только успешно завершить обучение, но и максимально использовать
                                        время, потраченное на учебу.</q>
                                    <div class="offset-top-30 text-right">
                                        <cite class="font-accent">Debra Banks</cite>
                                        <div class="offset-top-5">
                                            <p class="text-dark text-italic">Diploma for Graduates in Management,
                                                USA</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="shell section-70 section-xl-100 section-xl-bottom-114">
                    <div class="range range-xs-center range-xs-middle">
                        <div class="cell-sm-3 text-sm-right cell-sm-push-2"><img
                                class="img-responsive reveal-inline-block img-circle"
                                src="images/main/users/user-steven-alvarez-230x230.jpg" width="230" height="230" alt="">
                        </div>
                        <div class="cell-sm-9 cell-sm-push-1">
                            <div class="quote-classic-boxed text-left">
                                <div class="quote-body">
                                    <q>Когда я изучил доступные программы, я понял, что институт предлагает именно тот
                                        тип программы международного развития, который меня интересовал.</q>
                                    <div class="offset-top-30 text-right">
                                        <cite class="font-accent">Steven Alvarez</cite>
                                        <div class="offset-top-5">
                                            <p class="text-dark text-italic">Diploma for Graduates in Management,
                                                USA</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Latest news-->
    <section class="bg-catskill">
        <div class="shell section-70 section-md-114">
            <h2 class="text-bold">{{ __('messages.Latest News') }}</h2>
            <hr class="divider bg-madison">
            <div class="range offset-top-60 text-left range-xs-center">
                @foreach($news as $item)
                    <div class="cell-sm-6 offset-top-10">
                        <article class="post-news"><a
                                href="{{ route('main-news-show', [ 'news' => $item]) }}"><img
                                    class="img-responsive" src="{{ asset('images/news/main/' . $item->image) }}"
                                    alt=""></a>
                            <div class="post-news-body">
                                <h6>
                                    <a href="{{ route('main-news-show', [ 'news' => $item]) }}">{{ $item->{'name_' . app()->getLocale()} }}</a>
                                </h6>
                                <div class="offset-top-20">
                                    <p>{{ \Illuminate\Support\Str::limit($item->{ 'desc_' . app()->getLocale()}, 150, $end='...') }}</p>
                                </div>
                                <div class="post-news-meta offset-top-20"><span
                                        class="icon icon-xs mdi mdi-calendar-clock text-middle text-madison"></span><span
                                        class="text-middle inset-left-10 text-italic text-black">{{ date('d-m-Y', strtotime($item->date)) }}</span>
                                    <span style="float: right"><i class="icon mdi mdi-eye text-middle icon-xs text-madison"></i> {{ $item->views ?? 0 }}</span>
                                </div>
                            </div>
                        </article>
                    </div>
                @endforeach
            </div>
            <div class="offset-top-50"><a class="btn btn-primary"
                                          href="{{ route('main-news-index') }}">{{ __('messages.All news') }}</a></div>
        </div>
    </section>
    <!-- Gallery-->
    <section>
        <div class="owl-carousel flickr" data-items="1" data-xs-items="2" data-sm-items="3" data-lg-items="4"
             data-xl-items="6" data-nav="false" data-dots="false" data-mouse-drag="true" data-lightgallery="group"
             data-stage-padding="0" data-xl-stage-padding="0"><a class="thumbnail-default thumbnail-flickr"
                                                                 data-lightgallery="item"
                                                                 href="images/main/gallery/gallery-01-1200x800.jpg"><img
                    width="320" height="320" src="images/main/gallery/gallery-01-320x320.jpg" alt=""/><span
                    class="icon fa-search-plus"></span></a><a class="thumbnail-default thumbnail-flickr"
                                                              data-lightgallery="item"
                                                              href="images/main/gallery/gallery-02-1200x800.jpg"><img
                    width="320" height="320" src="images/main/gallery/gallery-02-320x320.jpg" alt=""/><span
                    class="icon fa-search-plus"></span></a><a class="thumbnail-default thumbnail-flickr"
                                                              data-lightgallery="item"
                                                              href="images/main/gallery/gallery-03-1200x800.jpg"><img
                    width="320" height="320" src="images/main/gallery/gallery-03-320x320.jpg" alt=""/><span
                    class="icon fa-search-plus"></span></a><a class="thumbnail-default thumbnail-flickr"
                                                              data-lightgallery="item"
                                                              href="images/main/gallery/gallery-04-1200x800.jpg"><img
                    width="320" height="320" src="images/main/gallery/gallery-04-320x320.jpg" alt=""/><span
                    class="icon fa-search-plus"></span></a><a class="thumbnail-default thumbnail-flickr"
                                                              data-lightgallery="item"
                                                              href="images/main/gallery/gallery-05-1200x800.jpg"><img
                    width="320" height="320" src="images/main/gallery/gallery-05-320x320.jpg" alt=""/><span
                    class="icon fa-search-plus"></span></a><a class="thumbnail-default thumbnail-flickr"
                                                              data-lightgallery="item"
                                                              href="images/main/gallery/gallery-06-1200x800.jpg"><img
                    width="320" height="320" src="images/main/gallery/gallery-06-320x320.jpg" alt=""/><span
                    class="icon fa-search-plus"></span></a>
        </div>
    </section>
    <!-- Google map-->
    <section class="google-map-container" data-zoom="14" data-center="6036 Richmond hwy., Alexandria, VA, 2230"
             data-icon="images/main/gmap_marker.png" data-icon-active="images/main/gmap_marker_active.png"
             data-styles="[{&quot;featureType&quot;:&quot;administrative&quot;,&quot;elementType&quot;:&quot;labels.text.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#444444&quot;}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#f2f2f2&quot;}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;poi.business&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;road&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:45}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;simplified&quot;}]},{&quot;featureType&quot;:&quot;road.arterial&quot;,&quot;elementType&quot;:&quot;labels.icon&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#b4d4e1&quot;},{&quot;visibility&quot;:&quot;on&quot;}]}]">
        <div style="position:relative;overflow:hidden;"><a
                href="https://yandex.uz/maps/org/131157846718/?utm_medium=mapframe&utm_source=maps"
                style="color:#eee;font-size:12px;position:absolute;top:0px;">Учебно-консалтинговый центр туризма</a><a
                href="https://yandex.uz/maps/10335/tashkent/category/educational_center/184106168/?utm_medium=mapframe&utm_source=maps"
                style="color:#eee;font-size:12px;position:absolute;top:14px;">Учебный центр в Ташкенте</a><a
                href="https://yandex.uz/maps/10335/tashkent/category/advanced_training_centre/184106156/?utm_medium=mapframe&utm_source=maps"
                style="color:#eee;font-size:12px;position:absolute;top:28px;">Центр повышения квалификации в
                Ташкенте</a>
            <iframe src="https://yandex.{{ app()->getLocale() }}/map-widget/v1/-/CCUazBSLHB" width="100%" height="400"
                    frameborder="1" allowfullscreen="true" style="pointer-events: none;position:relative;"></iframe>
        </div>
    </section>

@endsection
