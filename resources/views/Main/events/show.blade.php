@extends('Main.main-layout')
@section('title', $event->{ 'name_' . app()->getLocale()})
@section('description', $event->{ 'description_' . app()->getLocale()})
@section('keywords', $event->{ 'description_' . app()->getLocale()})

@section('breadcrumb')
    @include('Main.includes.breadcrumb', [
    'title' => $event->getName() ,
    'items' => [
        __('messages.Events') => route('main-events-index'),
        $event->{ 'name_' . app()->getLocale() } => ''
]
])
@endsection
@section('main-content')
    <section class="section-70 section-md-114">
        <div class="shell">
            <div class="range range-xs-center">
                <div class="cell-sm-6 text-left">
                    <div class="inset-sm-right-30"><img class="img-responsive reveal-inline-block"
                                                        src="{{ asset('images/events/main/' . $event->image) }}" width="540" height="540"
                                                        alt="">

                    </div>
                </div>
                <div class="cell-sm-6 text-left offset-top-50 offset-sm-top-0">
                    <h2 class="text-bold">{{ $event->getName() }}</h2>
                    <div class="hr divider bg-madison hr-left-0"></div>
                    <div class="offset-top-30 offset-sm-top-60">
                        {!! $event->getContent() !!}
                    </div>
                    <div class="offset-top-30 offset-sm-top-60">
                        <h6 class="text-bold">{{ __('messages.Event details') }}</h6>
                        <div class="text-subline"></div>
                    </div>
                    <div class="offset-top-17">
                        <ul class="list list-unstyled">
                            <li><span class="text-black">{{ __('messages.Date') }}:</span>
                                <span>{{ $event->start_date }} {{ $event->end_date ? '- '.$event->end_date : '' }} </span>
                            </li>
                            <li><span class="text-black">{{ __('messages.Fee') }}:</span> <span>{{ $event->fee }}</span>
                            </li>
                            <li><span
                                    class="text-black">{{ __('messages.Location') }}:</span> {{ $event->getLocation() }}
                                <span></span>
                            </li>
                        </ul>
                        <div class="offset-top-30 offset-sm-top-60">
                            <h6 class="text-bold">{{ __('messages.Comment') }}</h6>
                            <div class="text-subline"></div>
                        </div>
                        <div class="offset-top-20">
                            {!!  $event->getComment()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-catskill">
        <div class="shell-wide section-70 section-md-114">
            <h2 class="text-bold">{{ __('messages.Other Events') }}</h2>
            <hr class="divider bg-madison">
            <div class="range offset-top-60 range-xs-center">
                @foreach($otherEvents as $item)
                    <div class="cell-sm-6 cell-md-5 cell-xl-3 offset-top-50 offset-sm-top-0">
                        <article class="post-event">
                            <div class="post-event-img-overlay"><img class="img-responsive"
                                                                     src="{{ asset('images/events/main/' . $item->image) }}"
                                                                     width="420" height="420" alt="">
                                <div class="post-event-overlay context-dark">
                                    <div class="offset-top-20"><a class="btn btn-default"
                                                                  href="{{ route('main-news-show', [ 'news' => $item]) }}">{{ __('messages.Learn more') }}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="unit unit-lg unit-lg-horizontal">
                                <div class="unit-left">
                                    <div class="post-event-meta text-center">

                                        <div
                                            class="h3 text-bold reveal-inline-block reveal-lg-block">{{ date('d', strtotime($item->start_date)) }}</div>
                                        <p class="reveal-inline-block reveal-lg-block">{{ __('months.'. date('F', strtotime($item->start_date))) }}</p>
                                        <span
                                            class="text-bold reveal-inline-block reveal-lg-block inset-left-10 inset-lg-left-0">{{ date('H:i', strtotime($item->start_date)) }}</span>
                                    </div>
                                </div>
                                <div class="unit-body">
                                    <div class="post-event-body text-lg-left">
                                        <h6>
                                            <a href="{{ route('main-events-show', [ 'event' => $item]) }}">{{ $item->getName() }}</a>
                                        </h6>
                                        <i class="icon mdi mdi-eye text-middle icon-xs text-madison"></i> {{ $item->views ?? 0 }}
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
