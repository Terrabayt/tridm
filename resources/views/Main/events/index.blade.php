@extends('Main.main-layout')
@section('title', __('messages.Events'))

@section('breadcrumb')
    @include('Main.includes.breadcrumb', ['title' => __('messages.Events'),'items' => [__('messages.Events') => '']])
@endsection
@section('main-content')
    <section class="section-70 section-md-114">
        <div class="">
            <div class="slider slider-single carousel-parent calendar-carousel">
                @foreach(Config::get('months') as $key => $month)
                    <h2 class="text-bold">{{ __('months.' . $month) }} {{ date('Y') }}</h2>
                @endforeach
            </div>
            <div class="hr divider bg-madison"></div>
            <div class="">
                <div class="slider slider-nav">
                    @foreach(Config::get('months') as $key => $item)
                        @foreach($model as $month => $events)
                            <div class="item">
                                <div class="shell-wide">
                                    <div class="range range-xs-center  range-xl-left">
                                        @if((int)($key + 1) === (int)$month)
                                            @foreach($events as $event)
                                                <div
                                                    class="cell-sm-6 cell-md-5 cell-xl-3 offset-top-50 {{ !$loop->first || !$loop->last ? '' : 'offset-top-50 offset-sm-top-0'  }}">
                                                    <article class="post-event">
                                                        <div class="post-event-img-overlay"><img class="img-responsive"
                                                                                                 src="{{ asset('images/events/main/' . $event->image) }}"
                                                                                                 width="420"
                                                                                                 height="420"
                                                                                                 alt="">
                                                            <div class="post-event-overlay context-dark">
                                                                <div class="offset-top-20"><a class="btn btn-default"
                                                                                              href="{{ route('main-events-show', ['event' => $event]) }}"
                                                                                              tabindex="0">{{ __('messages.Learn more') }}</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="unit unit-lg unit-lg-horizontal">
                                                            <div class="unit-left">
                                                                <div class="post-event-meta text-center">
                                                                    <div
                                                                        class="h3 text-bold reveal-inline-block reveal-lg-block">
                                                                        {{ date('d', strtotime($event->start_date)) }}
                                                                    </div>
                                                                    <p class="reveal-inline-block reveal-lg-block">
                                                                        {{ __('months.'. date('F', strtotime($event->start_date))) }}</p>
                                                                    <span
                                                                        class="text-bold reveal-inline-block reveal-lg-block inset-left-10 inset-lg-left-0">{{ date('H:i', strtotime($event->start_date)) }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="unit-body">
                                                                <div class="post-event-body text-lg-left">
                                                                    <h6><a href="{{ route('main-events-show', ['event' => $event]) }}"
                                                                           tabindex="0">{{ $event->getName() }}</a></h6>
                                                                    <i class="icon mdi mdi-eye text-middle icon-xs text-madison"></i> {{ $event->views ?? 0 }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection
@section('js_after')
    <script>
        currentMonth = {{ date('m') }}
        $('.slider-single').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: false,
            initialSlide: currentMonth - 1,
            adaptiveHeight: true,
            infinite: false,
            useTransform: true,
            speed: 400,
            cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
        });

        $('.slider-nav')
            .on('init', function (event, slick) {
                $('.slider-nav .slick-slide.slick-current').addClass('is-active');
            })
            .slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: false,
                initialSlide: currentMonth - 1,
                focusOnSelect: false,
                infinite: false,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }, {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }, {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }]
            });

        $('.slider-single').on('afterChange', function (event, slick, currentSlide) {
            $('.slider-nav').slick('slickGoTo', currentSlide);
            var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
            $('.slider-nav .slick-slide.is-active').removeClass('is-active');
            $(currrentNavSlideElem).addClass('is-active');
        });

        $('.slider-nav').on('click', '.slick-slide', function (event) {
            var goToSingleSlide = $(this).data('slick-index');

            $('.slider-single').slick('slickGoTo', goToSingleSlide);
        });
    </script>
@endsection
