@extends('Main.main-layout')
@section('title', )
@section('description', $model->{ 'description_' . app()->getLocale()})
@section('keywords', $model->{ 'description_' . app()->getLocale()})

@section('breadcrumb')
    @include('Main.includes.breadcrumb', [
    'title' => $model->{ 'name_' . app()->getLocale() } ,
    'items' => [
        $model->{ 'name_' . app()->getLocale() } => ''
]
])
@endsection
@section('main-content')
    <div class="section-70 ">
        <div class="shell-wide">
        <div class="range range-xs-center">
            <iframe name="Framename" src="https://map.uzbekistan.travel/ru" frameborder="0" scrolling="false" class="frame-area">
            </iframe>
        </div>
        </div>
    </div>


    <style>
        iframe{
            width: 95%;
            height: 100vh;
        }
    </style>
@endsection
@section('right-sidebar')
@endsection
