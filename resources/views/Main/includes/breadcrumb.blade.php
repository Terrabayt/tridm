<section class="breadcrumb-classic context-dark">
    <div class="shell section-30 section-sm-top-70 section-sm-bottom-60">
        <h1 class="veil reveal-sm-block">{{ $title }}</h1>
        <div class="offset-sm-top-35">
            <ul class="list-inline list-inline-lg list-inline-dashed p">
                <li><a href="/">{{ __('menu.Home') }}</a></li>

                @foreach($items as $name => $link)
                    @if($link !== '')
                        <li><a href="{{ $link }}">{{ $name }}</a></li>
                    @else
                    <li>{{ $name }}</li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</section>
