<header class="page-head @if(request()->is('/')) header-panel-absolute @endif">
    <!-- RD Navbar Transparent-->
    <div class="rd-navbar-wrap">
        <nav class="rd-navbar rd-navbar-default" data-md-device-layout="rd-navbar-static"
             data-lg-device-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static"
             data-stick-up-offset="210" data-xl-stick-up-offset="85" data-lg-auto-height="true" data-auto-height="false"
             data-md-layout="rd-navbar-static" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
            <div class="rd-navbar-inner">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                    <!-- RD Navbar Toggle-->
                    <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap">
                        <span></span></button>
                    <h4 class="panel-title veil-md" style="height: inherit;overflow: hidden;">@yield('title')</h4>
                    <!-- RD Navbar Right Side Toggle-->
                    <button class="rd-navbar-top-panel-toggle veil-md" data-rd-navbar-toggle=".rd-navbar-top-panel">
                        <span></span></button>
                    <div class="rd-navbar-top-panel">
                        <div class="rd-navbar-top-panel-left-part">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                                        <div class="unit-left"><span class="icon mdi mdi-phone text-middle"></span>
                                        </div>
                                        <div class="unit-body">
                                            <a href="tel:+998712687371">+99871 268-73-71,</a>
                                            <a class="reveal-block reveal-md-inline-block" href="tel:+998712687367">+99871
                                                268-73-67,</a>
                                            <a class="reveal-block reveal-md-inline-block" href="tel:+998712680025">+99871
                                                268-00-25</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                                        <div class="unit-left"><span class="icon mdi mdi-map-marker text-middle"></span>
                                        </div>
                                        <div class="unit-body">
                                            <a href="https://goo.gl/maps/h9fpD71Yq6Wcmgkc8"
                                               target="_blank">{{ __('messages.self address') }}</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                                        <div class="unit-left"><span class="icon mdi mdi-email-open text-middle"></span>
                                        </div>
                                        <div class="unit-body"><a href="mailto:info@tridm.uz">info@tridm.uz</a></div>
                                    </div>
                                </li>
                                <li class="unit unit-horizontal unit-middle unit-spacing-xs" id="lang">
                                    <div class="unit-body">
                                        @foreach (Config::get('languages') as $lang => $language)
                                            <a style="margin-right: 10px"
                                               class="{{$lang == app()->getLocale() ? 'active': ''}}"
                                               href="{{ route('lang-switch',['lang' =>  $lang]) }}">
                                                <img
                                                    src="{{ $lang === 'en' ? asset('/images/other/us-flag.gif'): ($lang === 'uz' ? asset('/images/other/uz-flag.gif') : asset('/images/other/rs-flag.gif')) }}"
                                                    height="10" width="15"
                                                    alt=""> {{ __('messages.' . $language )}} </a>
                                        @endforeach
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="rd-navbar-top-panel-right-part">
                            <div class="rd-navbar-top-panel-left-part">
                                <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                                    <div class="unit-left"><span class="icon mdi mdi-login text-middle"></span></div>
                                    <div class="unit-body">
                                        @if(Auth::guest())
                                            <a href="{{ route('main-login-register') }}">{{ __('messages.Login') }}
                                                /{{ __('messages.Register') }}</a>
                                        @else
                                            @if(Auth::user()->role === 'student')
                                                <a href="{{ route('student-dashboard') }}">{{ __('messages.Dashboard') }}</a>
                                            @elseif(Auth::user()->role === 'instructor')
                                                <a href="{{ route('instructor-dashboard') }}">{{ __('messages.Dashboard') }}</a>
                                                @else
                                                <a href="{{ route('dashboard') }}">{{ __('messages.Dashboard') }}</a>
                                            @endif
                                        @endguest
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rd-navbar-menu-wrap clearfix">
                    <!--Navbar Brand-->
                    <div class="rd-navbar-brand"><a class="reveal-inline-block" href="/">
                            <div class="unit unit-xs-middle unit-xl unit-xl-horizontal unit-spacing-xxs">
                                <div class="unit-left"><img width='170' height='170'
                                                            src='{{asset('images/other/институт развития туризма лого.png')}}'
                                                            alt=''/>
                                </div>
                                <div class="unit-body text-xl-left">
                                    <div
                                        class="rd-navbar-brand-title">{{ __('messages.Scientific research institute for the study of the problems of cultural heritage objects and tourism development') }}</div>
                                    <div
                                        class="rd-navbar-brand-slogan">{{ __('messages.under the Ministry of Tourism and cultural heritage of the Republic of Uzbekistan') }}
                                    </div>
                                </div>
                            </div>
                        </a></div>
                    <div class="rd-navbar-nav-wrap">
                        <div class="rd-navbar-mobile-scroll">
                            <div class="rd-navbar-mobile-header-wrap">
                                <!--Navbar Brand Mobile-->
                                <div class="rd-navbar-mobile-brand">
                                    <a href="/">
                                        <img width='138' height='138'
                                             src='{{asset('images/other/институт развития туризма лого.png')}}' alt=''/>
                                    </a>
                                </div>
                            </div>
                            <!-- RD Navbar Nav-->
                            <ul class="rd-navbar-nav">
                                @foreach($dynamicMenus as $parentMenu)
                                    <li class="{{ (request()->is($parentMenu->link)) ? 'active' : '' }}">
                                        <a href="{{ count($parentMenu->childs) > 0 ? '#' : (Str::startsWith($parentMenu->link, '/') ? $parentMenu->link : '/' . $parentMenu->link) }}">{{ $parentMenu->getName() }}</a>
                                        @if(count($parentMenu->childs) > 0)
                                            <ul class="rd-navbar-dropdown">
                                                @foreach($parentMenu->childs as $menu)
                                                    <li class="{{ (request()->is($menu->link)) ? 'active' : '' }}">
                                                        <a href="{{ (Str::startsWith($menu->link, '/') ? $menu->link : '/' . $menu->link)}}">{{ $menu->getName() }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>


                            <!--RD Navbar Mobile Search-->
                            {{--                            <div class="rd-navbar-search-mobile" id="rd-navbar-search-mobile">--}}
                            {{--                                <form class="rd-navbar-search-form search-form-icon-right rd-search"--}}
                            {{--                                      action="search-results.html" method="GET">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <label class="form-label"--}}
                            {{--                                               for="rd-navbar-mobile-search-form-input">{{ __('messages.Search...') }}</label>--}}
                            {{--                                        <input--}}
                            {{--                                            class="rd-navbar-search-form-input form-control form-control-gray-lightest"--}}
                            {{--                                            id="rd-navbar-mobile-search-form-input" type="text" name="s"--}}
                            {{--                                            autocomplete="off"/>--}}
                            {{--                                    </div>--}}
                            {{--                                    <button class="icon fa-search rd-navbar-search-button"--}}
                            {{--                                            type="submit"></button>--}}
                            {{--                                </form>--}}
                            {{--                            </div>--}}
                            <ul class="rd-navbar-nav rd-navbar-search-mobile" id="lang-mobile">
                                <li>
                                    <a href="#">{{ __("messages.". Config::get('languages')[app()->getLocale()]) }}</a>
                                    <ul class="rd-navbar-dropdown">
                                        @foreach (Config::get('languages') as $lang => $language)
                                            @if ($lang != app()->getLocale())
                                                <li class="{{ (request()->is('institute')) ? 'active' : '' }}">
                                                    <a href="{{ route('lang-switch',['lang' =>  $lang]) }}">{{ __('messages.' . $language)}}</a>
                                                </li>
                                            @endif
                                        @endforeach

                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--RD Navbar Search-->
                    {{--                    <div class="rd-navbar-search"><a class="rd-navbar-search-toggle mdi"--}}
                    {{--                                                     data-rd-navbar-toggle=".rd-navbar-search"--}}
                    {{--                                                     href="#"><span></span></a>--}}
                    {{--                        <form class="rd-navbar-search-form search-form-icon-right rd-search"--}}
                    {{--                              action="search-results.html" data-search-live="rd-search-results-live" method="GET">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label class="form-label"--}}
                    {{--                                       for="rd-navbar-search-form-input">{{ __('messages.Search...') }}</label>--}}
                    {{--                                <input class="rd-navbar-search-form-input form-control form-control-gray-lightest"--}}
                    {{--                                       id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off"/>--}}
                    {{--                                <div class="rd-search-results-live" id="rd-search-results-live"></div>--}}
                    {{--                            </div>--}}
                    {{--                        </form>--}}
                    {{--                    </div>--}}


                </div>
            </div>
        </nav>
    </div>
</header>
<style>
    #lang .active {
        font-size: 17px;
        font-weight: bold;
        color: orange;
    }
</style>
