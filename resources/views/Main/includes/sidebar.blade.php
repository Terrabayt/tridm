<div class="offset-top-60">
    <!--Recent posts-->
    <h6 class="text-bold">{{ __('messages.Recent News') }}</h6>
    <div class="text-subline"></div>
    <div class="offset-top-20 text-left">
        @foreach($latestNews as $news)
            @if($loop->index === 3)
                @continue
            @endif
        <div class="offset-top-20">
            <article class="widget-post">
                <h6 class="text-bold text-primary"><a href="{{ route('main-news-show', [ 'news' => $news]) }}">{{ $news->{ 'name_' . app()->getLocale() } }}</a></h6>
                <p class="text-dark">{{ date('d-m-Y', strtotime($news->date)) }}</p>
            </article>
        </div>
        @endforeach
    </div>
</div>
