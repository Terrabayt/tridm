@extends('Main.main-layout')
@section('title',  __('messages.Login') . '/' . __('messages.Register'))

@section('main-content')
    <main class="page-content section-70">
        <!--Section Login Register-->
        <section class="section-98 section-sm-110 section-bottom-66">
            <div class="shell">
                <div class="range range-xs-center section-34">
                    <div class="cell-xs-8 cell-sm-6 cell-md-5">
                        <h2 class="text-bold">{{ __('messages.Login') }}/{{ __('messages.Register') }}</h2>
                        <hr class="divider bg-madison">


                        <div class="offset-sm-top-45 text-center">
                            <!-- Responsive-tabs-->
                            <div class="responsive-tabs responsive-tabs-classic horizontal" data-type="horizontal"
                                 style="width: 100%;">
                                <ul class="resp-tabs-list tabs-1 text-center tabs-group-default"
                                    data-group="tabs-group-default">
                                    <li class="resp-tab-item tabs-group-default resp-tab-active"
                                        aria-controls="tabs-group-default_tab_item-0"
                                        role="tab">{{ trans('messages.Login') }}
                                    </li>
                                    <li class="resp-tab-item tabs-group-default"
                                        aria-controls="tabs-group-default_tab_item-1"
                                        role="tab">{{ trans('messages.Register') }}
                                    </li>
                                </ul>
                                <div class="resp-tabs-container text-sm-left tabs-group-default"
                                     data-group="tabs-group-default">
                                    <div class="resp-tab-content tabs-group-default resp-tab-content-active"
                                         aria-labelledby="tabs-group-default_tab_item-0">
                                        <!-- RD Mailform-->
                                        @if(session()->has('error_login'))
                                            <div class="alert alert-danger">
                                                <ul>
                                                    <li>{{ session()->get('error_login') }}</li>
                                                </ul>
                                            </div>
                                        @endif
                                        <form class="text-left" data-form-output="form-output-global" method="POST"
                                              action="{{ route('main-auth-check') }}">
                                            @csrf
                                            <div class="form-group">
                                                <label class="form-label form-label-outside rd-input-label"
                                                       for="form-login-username">{{ trans('messages.Your email') }}
                                                    :</label>
                                                <input
                                                    class="form-control bg-white form-control-has-validation form-control-last-child"
                                                    id="form-login-username" value="{{ old('email') }}" type="text"
                                                    name="login_email">
                                                <span
                                                    class="form-validation text-danger">{{ $errors->has('login_email') ? $errors->first('login_email') : '' }}</span>
                                            </div>
                                            <div class="form-group offset-top-15">
                                                <label class="form-label form-label-outside rd-input-label"
                                                       for="form-login-password">{{ trans('messages.Password') }}
                                                    :</label>
                                                <input
                                                    class="form-control bg-white form-control-has-validation form-control-last-child"
                                                    id="form-login-password" type="password"
                                                    value="{{ old('login_password') }}" name="login_password">
                                                <span
                                                    class="form-validation text-danger">{{ $errors->has('login_password') ? $errors->first('login_password') : '' }}</span>
                                            </div>
                                            <div class="offset-top-20">
                                                <button class="btn btn-primary reveal-block reveal-lg-inline-block"
                                                        type="submit">{{ trans('messages.Login') }}
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="resp-tab-content tabs-group-default"
                                         aria-labelledby="tabs-group-default_tab_item-1">
                                        <!-- RD Mailform-->
                                        <form class="text-left" method="POST"
                                              action="{{ route('main-auth-register') }}">
                                            @csrf
                                            <div class="form-group">
                                                <label class="form-label form-label-outside rd-input-label"
                                                       for="form-register-username">{{ trans('messages.Full name') }}
                                                    :</label>
                                                <input
                                                    class="form-control bg-white form-control-has-validation form-control-last-child"
                                                    id="form-register-username" type="text"
                                                    value="{{ old('username') }}" name="username">
                                                <span
                                                    class="form-validation text-danger">{{ $errors->has('username') ? $errors->first('username') : '' }}</span>
                                            </div>
                                            <div class="form-group offset-top-15">
                                                <label class="form-label form-label-outside rd-input-label"
                                                       for="form-register-email">{{ trans('messages.Region') }}:</label>
                                                <select class="" name="region">
                                                    @foreach(Config::get('resources.regions') as $region)
                                                        <option
                                                            value="{{ $region }}">{{ trans('messages.' . $region) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group offset-top-15">
                                                <label class="form-label form-label-outside rd-input-label"
                                                       for="form-register-email">{{ trans('messages.Your email') }}
                                                    :</label>
                                                <input
                                                    class="form-control bg-white form-control-has-validation form-control-last-child"
                                                    id="form-register-email" type="text" name="email"><span
                                                    class="form-validation"></span>
                                            </div>
                                            <div class="form-group offset-top-15">
                                                <label class="form-label form-label-outside rd-input-label"
                                                       for="form-register-password">{{ trans('messages.Password') }}
                                                    :</label>
                                                <input
                                                    class="form-control bg-white form-control-has-validation form-control-last-child"
                                                    id="form-register-password" type="password" name="password"><span
                                                    class="form-validation"></span>
                                            </div>
                                            <div class="form-group offset-top-15">
                                                <label class="form-label form-label-outside rd-input-label"
                                                       for="form-register-confirm-password">{{ trans('messages.Confirm Password') }}
                                                    :</label>
                                                <input
                                                    class="form-control bg-white form-control-has-validation form-control-last-child"
                                                    id="form-register-confirm-password" type="password"
                                                    name="password_confirmation"><span
                                                    class="form-validation"></span>
                                            </div>
                                            <div class="offset-top-20">
                                                <button class="btn btn-primary"
                                                        type="submit">{{ trans('messages.Register') }}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
