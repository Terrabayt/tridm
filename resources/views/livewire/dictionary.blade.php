<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <!-- RD Search Form-->
            <div class="form-group">
                <input class="form-search-input form-control" placeholder="{{ __('messages.Search...') }}" type="text"
                       wire:model.debounce.500ms="search">
            </div>
        </div>
    </div>
    <div class=" container">
        <div class="col-md-12">
            @foreach(Config::get('resources.alphabet.'. app()->getLocale()) as $item)
                <button wire:click="changeLetter('{{ $item }}')" style="margin-top: 15px;"
                        class="btn {{ $letter === $item ? 'active' : '' }} btn-primary">{{ $item }}</button>
            @endforeach
            <button wire:click="removeFilter" style="margin-top: 15px;" class="btn btn-float"
                    title="{{ __('messages.Clear filter') }}">X
            </button>
        </div>
    </div>
    <div style="margin-top: 30px;"></div>
    <div class="row">
        @foreach($model as $item)
            <div class="col-md-4">
                <h5><a href="{{ route('main-dictionary-show', ['dictionary' => $item]) }}">{{ $item->getName() }}</a>
                </h5>
            </div>
            <div class="col-md-8">
                <h6>{{ $item->getDesc() }}</h6>
            </div>
            <div class="col-md-12 m-t-10">
                <hr class="hr-dashed" style="margin: 10px 0;">
            </div>
        @endforeach
        @if(!count($model))
            <h5><i class="mdi mdi-close"></i>{{ __('messages.No words found') }}</h5>
        @endif
    </div>
    <div class="text-center">
        {!! $model->links('vendor.pagination.tailwind') !!}
    </div>
</div>

