<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Welcome' => 'Добро пожаловать',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Menu' => 'Меню',
    'Admin panel' => 'Панель администратора',
    'List of menu' => 'Список меню',
    #region Menu

    #endregion
];
