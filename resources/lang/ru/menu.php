<?php

return [
    'Home' => 'Главная',

    'About the institute' => 'О институте',
    'Institute' => 'Институт',
    'Activities' => 'Деятельность',
    'Main goals' => 'Основные задачи',
    'Functions' => 'Функции',
    'Academic Council' => 'Ученый совет',
    'Goal and tasks' => 'Цель и задачи',
    'Rights and obligations' => 'Права и обязанности',
    'The order of the Council' => 'Порядок ученого совета',
    'Governance' => 'Руководство',
    'Structure' => 'Структура',
    'Partners' => 'Партнеры',
    'Regulations' => 'Нормативные документы',
    'Ratings and achievements' => 'Рейтинги и достижения',
    'Benefits' => 'Преимущества',
    'Projects' => 'Проекты',
    'Events' => 'Мероприятия',
    "Tour guide's portfolio" => 'Портфель экскурсовода',
    "Events calendar" => 'Календарь мероприятий',

    "Education" => 'Образование',
    "Quality system" => 'Система качества',
    "Retraining of personnel" => 'Переподготовка кадров',
    "Graduates" => 'Выпускники',
    "Our Courses" => 'Наши курсы',
    "Learning programs" => 'Учебные программы',
    "Training" => 'Повышение квалификации',
    "Online learning" => 'Онлайн обучение',

    "News" => 'Новости',

    "Contacts" => 'Контакты',
];
